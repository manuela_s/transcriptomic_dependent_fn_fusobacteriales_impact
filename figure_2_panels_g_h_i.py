#!/usr/bin/env python3

""" Generate figure 2 panels g-h-i.
Association between Fusobacterium nucleatum load in the Taxonomy cohort and Fusobacteriales relative abundance
in the TCGA-COAD-READ cohort with cell types."""

import pathlib
import typing

import matplotlib.pyplot
import matplotlib.colors
import pandas

import cohort
import constants
import fusobacteria_plot_utils
import fusobacteria_utils

FUSOBACTERIA_DST_DIR = pathlib.Path("figures_and_tables")


def _get_data() -> typing.Tuple[pandas.DataFrame, pandas.Series, typing.List[str]]:
    """
    Prepare data for plots by cohort and cell type.
    :return: tuple with:
        - dataframe, one row per patient with indices 'cohort', 'patient_id', 'technology', 'fusobacteria_binary_class'
        and columns including cell types computed by quanTIseq ('Tumour & Stromal cells', 'T cells CD4', 'T cells CD8',
        'T regs', 'Dendritic cells', 'B cells', 'Macrophages M1', 'Macrophages M2', 'NK cells', 'Neutrophils');
        - series, one row per patient per cell type feature (computed either by MCP-counter or quanTIseq) with indices
        'cohort', 'patient_id', 'technology', 'fusobacteria_binary_class', 'feature' and name 'expression';
        - list, sorted cell type names.
    """
    crc = cohort.get_crc_cohorts()

    # Plot data for figure 2 panels G-H
    plot_data1 = (
        fusobacteria_utils.get_crc_fusobacteria()[["fusobacteria_binary_class"]]
        .join(crc.get_quantiseq_cell_types().drop(columns=["Monocytes"]), how="inner")
        .set_index("fusobacteria_binary_class", append=True)
    )

    # Plot data for figure 2 panel I
    plot_data2 = (
        crc.get_immuno_composition()[
            ["Endothelial_cells", "Fibroblasts", "Neutrophils"]
        ]
        .rename(columns={"Neutrophils": "MCP_Neutrophils"})
        .join(plot_data1.drop(columns=["Tumour & Stromal cells"]), how="inner")
    )
    features_order = plot_data2.columns.to_list()
    plot_data2 = plot_data2.rename_axis(columns="feature").stack().rename("expression")

    return plot_data1, plot_data2, features_order


def _figure_2_panels_g_h_helper(
    plot_data: pandas.DataFrame,
    cohort_name: str,
    fusobacteria_label: str,
    panel_label: str,
) -> None:
    """
    Generate figure 2 panels G-H.
    Plot per-patient cell type composition determined by quanTIseq.
    :param plot_data: dataframe, as returned by _get_data();
    :param cohort_name: name of the cohort to use for labelling figure;
    :param fusobacteria_label: label to use, either Fusobacteriales or Fn for the TCGA-COAD-READ or Taxonomy cohort,
                               respectively;
    :param panel_label: label for panel figure export.
    """
    data = plot_data.copy()

    data_aggr = data.groupby("fusobacteria_binary_class").mean()
    low_fusobacteria = data.query('fusobacteria_binary_class=="low"')
    high_fusobacteria = data.query('fusobacteria_binary_class=="high"')

    # noinspection PyTypeChecker
    fig, axes = matplotlib.pyplot.subplots(
        ncols=3,
        sharey=True,
        figsize=(7, 3),
        gridspec_kw=dict(
            width_ratios=[0.75, 0.1, 0.25],
            top=0.9,
            bottom=0.15,
            left=0.06,
            right=0.8,
            hspace=0.1,
            wspace=0.1,
        ),
    )
    fig.suptitle(cohort_name)

    params = dict(
        stacked=True,
        width=1,
        colormap=matplotlib.colors.ListedColormap(constants.QUANTISEQ.values()),
        legend=None,
    )

    low_fusobacteria.sort_values("Tumour & Stromal cells").plot.bar(
        **params, ax=axes[0]
    )
    axes[0].set_xticks([])
    axes[0].set_xlim(-0.5, low_fusobacteria.shape[0] - 0.5)
    axes[0].set_ylim(0, 100)
    axes[0].set_xlabel(
        f"$\it{fusobacteria_label}$-low\npatients\n(n={low_fusobacteria.shape[0]})"
    )
    axes[0].set_ylabel("Cell type abundance [%]")

    data_aggr.plot.bar(**params, edgecolor="k", linewidth=0.5, ax=axes[1])
    axes[1].set_xlim(-0.5, 1.5)
    axes[1].set_xlabel("Summary")
    axes[1].set_ylabel("")

    high_fusobacteria.sort_values("Tumour & Stromal cells").plot.bar(
        **params, ax=axes[2]
    )
    axes[2].set_xticks([])
    axes[2].set_xlim(-0.5, high_fusobacteria.shape[0] - 0.5)
    axes[2].set_xlabel(
        f"$\it{fusobacteria_label}$-high\npatients\n(n={high_fusobacteria.shape[0]})"
    )
    axes[2].set_ylabel("")

    handles, labels = axes[1].get_legend_handles_labels()
    fig.legend(handles, labels, loc="center right")

    fig.savefig(FUSOBACTERIA_DST_DIR / f"figure_2_panel_{panel_label}.pdf")


# noinspection PyUnusedLocal
def figure_2_panels_g_h_i(
    plot_data1: pandas.DataFrame,
    plot_data2: pandas.Series,
    features_order: typing.List[str],
    cohort_name: str,
    fusobacteria_label: str,
    panel_label: str,
) -> None:
    """
    Generate figure 2 panels G-H-I:
        - panels G-H: per-patient cell type composition determined by quanTIseq by cohort (panel G and H for the
        TCGA-COAD-READ and Taxonomy cohorts, respectively);
        - panel I: comparison of distribution for selected cell types grouped by either Fusobacteriales relative
        abundance (TCGA-COAD-READ cohort) or Fusobacterium nucleatum load (Taxonomy cohort);
    :param plot_data1: dataframe, as returned by _get_data();
    :param plot_data2: series, as returned by _get_data();
    :param features_order: list, as returned by _get_data();
    :param cohort_name: name of the cohort to plot per-patient cell type composition, either 'TCGA-COAD-READ' or
    'taxonomy';
    :param fusobacteria_label: label to use, either Fusobacteriales or Fn for the TCGA-COAD-READ or Taxonomy cohort,
                               respectively;
    :param panel_label: label for panel figure export.
    """
    cohort_label = cohort_name.replace("-", "_").lower()

    _figure_2_panels_g_h_helper(
        plot_data1.query("cohort == @cohort_name"),
        cohort_label,
        fusobacteria_label,
        panel_label,
    )

    fusobacteria_plot_utils.gene_group_vs_fusobacteria_figure_separate_panels(
        plot_data2.to_frame().query("cohort == @cohort_name").expression,
        cohort_label,
        f"figure_2_panel_i_{cohort_label}_cohort",
        features_order,
        FUSOBACTERIA_DST_DIR,
        "Cell types abundance",
    )


def main():
    plot_data1, plot_data2, features_order = _get_data()

    figure_2_panels_g_h_i(plot_data1, plot_data2, features_order, "taxonomy", "Fn", "g")

    figure_2_panels_g_h_i(
        plot_data1, plot_data2, features_order, "TCGA-COAD-READ", "Fusobacteriales", "h"
    )


if __name__ == "__main__":
    matplotlib.rcParams["pdf.fonttype"] = 42
    matplotlib.rcParams["font.size"] = 6
    matplotlib.rcParams["font.family"] = "Arial"

    pathlib.Path(FUSOBACTERIA_DST_DIR).mkdir(parents=True, exist_ok=True)

    main()
