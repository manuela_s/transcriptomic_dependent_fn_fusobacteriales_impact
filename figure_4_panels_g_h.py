#!/usr/bin/env python3

""" Generate figure 4 panels g and h.
Association between between Fusobacterium nucleatum (Fn) load (Taxonomy cohort) and Fusobacteriales relative abundance
(TCGA-COAD-READ cohort) with key inflammtion, pro-inflammation and anti-inflammation markers in CMS1 patients."""

import os
import typing

import pandas

os.environ["OUTDATED_IGNORE"] = "1"

import pathlib

import matplotlib.pyplot

import cohort
import fusobacteria_plot_utils
import fusobacteria_utils


DST_DIR = pathlib.Path("figures_and_tables")

INFLAMMATION_MARKERS = ["NFKB1", "TNF", "IL6", "IL8"]
PRO_INFLAMMATION_MARKERS = ["IL1B", "S100A8", "S100A9"]
ANTI_INFLAMMATION_MARKERS = ["CD163", "SEPP1", "APOE", "MAF"]


def _sub_panel_helper(
    data: pandas.DataFrame,
    features: typing.List[str],
    cohort_label: str,
    group_label: str,
) -> None:
    """
    Helper to generate a sub-panel for a group of features (markers) grouped by either Fusobacterium nucleatum (Fn) load
    or Fusobacteriales relative abundance categorized as low vs. high.
    :param data:
    :param features: list of features/markers to include;
    :param cohort_label: label to be included in figure title;
    :param group_label: label to characterize features/markers group and be included in figure name.
    """
    plot_data = (
        data.copy()
        .rename_axis(columns={None: "feature"})
        .stack()
        .to_frame("expression")
    )
    fusobacteria_plot_utils.gene_group_vs_fusobacteria_figure_separate_panels(
        plot_data.query("feature.isin(@features)").expression,
        cohort_label,
        group_label,
        features,
        DST_DIR,
        f"{group_label} within CMS1",
    )


def _panel_helper(data: pandas.DataFrame, cohort_name: str, panel_name: str) -> None:
    """
    Helper to generate a panel composed of 3 sub-panels, one for each group of markers (key inflammation,
    pro-inflammation and anti-inflammation) displayed in figure 3 panel g and h.
    :param data: dataframe, one row per patient for patients of the 'taxonomy' and 'TCGA-COAD-READ' cohorts. Indices
                 must include 'cohort' and 'fusobacteria_binary_class'. Columns must include gene expression for
                 'INFLAMMATION_MARKERS', 'PRO_INFLAMMATION_MARKERS' and 'ANTI_INFLAMMATION_MARKERS';
    :param cohort_name: label to be included in figure title;
    :param panel_name: label for panel.
    """
    data = data.copy()
    _sub_panel_helper(
        data[INFLAMMATION_MARKERS],
        INFLAMMATION_MARKERS,
        cohort_name,
        f"figure_4_panel_{panel_name}_inflammation_markers",
    )
    _sub_panel_helper(
        data[PRO_INFLAMMATION_MARKERS],
        PRO_INFLAMMATION_MARKERS,
        cohort_name,
        f"figure_4_panel_{panel_name}_pro_inflammation_markers",
    )
    _sub_panel_helper(
        data[ANTI_INFLAMMATION_MARKERS],
        ANTI_INFLAMMATION_MARKERS,
        cohort_name,
        f"figure_4_panel_{panel_name}_anti_inflammation_markers",
    )


def main():
    crc = cohort.get_crc_cohorts()
    data = (
        crc.get_subtypes()[["cms_class"]]
        .dropna()
        .join(
            crc.get_rna()[
                INFLAMMATION_MARKERS
                + PRO_INFLAMMATION_MARKERS
                + ANTI_INFLAMMATION_MARKERS
            ],
            how="inner",
        )
        .join(
            fusobacteria_utils.get_crc_fusobacteria()[["fusobacteria_binary_class"]],
            how="inner",
        )
        .query('cohort.isin(["TCGA-COAD-READ", "taxonomy"]) and cms_class=="CMS1"')
        .set_index(["cms_class", "fusobacteria_binary_class"], append=True)
    )

    _panel_helper(data.query('cohort=="taxonomy"'), "taxonomy", "g")
    _panel_helper(data.query('cohort=="TCGA-COAD-READ"'), "tcga_crc", "h")


if __name__ == "__main__":
    matplotlib.rcParams["pdf.fonttype"] = 42
    matplotlib.rcParams["font.family"] = "Arial"
    matplotlib.rcParams["font.size"] = 6

    pathlib.Path(DST_DIR).mkdir(parents=True, exist_ok=True)

    main()
