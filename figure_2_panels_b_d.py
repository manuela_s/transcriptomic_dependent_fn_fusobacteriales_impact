#!/usr/bin/env python3

""" Generate figure 2 panels b and d.
Per-patient visualisation of Fusobacterium nucleatum load in the Taxonomy cohort and Fusobacteriales relative abundance
in the TCGA-COAD-READ cohort."""

import pathlib

import matplotlib.colors
import matplotlib.pyplot
import matplotlib.ticker
import numpy
import pandas
import seaborn

import constants
import fusobacteria_utils

DST_DIR = pathlib.Path("figures_and_tables")


def figure_2_panels_b_and_d(fusobacteria: pandas.DataFrame, cohort: str) -> None:
    """
    Generate Figure 2 Panels B and D.
    Plot panel with per-patient (waterfall plot, left) and distribution (violin plot with overlaid data-points, right)
    of bacterium prevalence in tumour resections of the Taxonomy (panel B) and TCGA-COAD-READ (panel D). Patients are
    sorted in ascending order by prevalence of either Fusobacterium nucleatum (Taxonomy cohort) or Fusobacteriales at
    the order taxonomic rank (TCGA-COAD-READ cohort). Cut-off of 75th percentile used for patients’ stratification in
    downstream analysis is also indicated (black dotted line).
    :param fusobacteria: dataframe with one row per patient, indiced 'cohort' and 'patient_id' and column 'fusobacteria'
                         with either Fusobacterium nucleatum load (Taxonomy cohort) or Fusobacteriales relative abundance
                         (TCGA-COAD-READ cohort), as returned by `fusobacteria_utils.get_crc_fusobacteria()`;
    :param cohort: name to label figure.
    """
    data = fusobacteria.query("cohort==@cohort")

    if cohort == "taxonomy":
        y_label = "$Fusobacterium$ $nucleautum$ load [$2^{-\\Delta CT}$]"
        y_lim = 10
        figure_label = "b"
    else:
        y_label = "$Fusobacteriales$ relative abundance [%]"
        y_lim = 100
        figure_label = "d"

    fig, axes = matplotlib.pyplot.subplots(
        ncols=2,
        gridspec_kw=dict(
            top=0.985,
            bottom=0.085,
            left=0.105,
            right=0.995,
            wspace=0.04,
            width_ratios=(0.8, 0.2),
        ),
        figsize=(7, 3),
    )
    ax_scatter, ax_dist = (
        axes
    )  # corresponding to left (1) and right (2) hand side in figure 2 panels B and D

    params = dict(y="fusobacteria")

    # Figure 2 panels B and D, right (2)
    ax_dist.set_yscale("symlog", linthresh=0.001)
    ax_dist.set_ylim(-0.5e-4, y_lim)
    ax_dist.yaxis.set_major_locator(
        matplotlib.ticker.SymmetricalLogLocator(
            transform=None, subs=numpy.arange(0, 1.1, 0.1), linthresh=0.001, base=10
        )
    )

    seaborn.violinplot(
        inner="quartile",
        color=".8",
        cut=0,
        data=data.reset_index(),
        ax=ax_dist,
        **params,
    )
    seaborn.swarmplot(
        hue="fusobacteria_binary_class",
        dodge=False,
        data=data.reset_index(),
        s=5,
        color="k",
        linewidth=0,
        alpha=1,
        ax=ax_dist,
        **params,
    )
    ax_dist.set_yticklabels([])
    ax_dist.set_ylabel("")

    # Figure 2 panels B and D, left (1)
    seaborn.scatterplot(
        x="patient_id",
        hue="fusobacteria_binary_class",
        palette=constants.FUSOBACTERIA_BIN_COLORS,
        s=20,
        edgecolor=None,
        data=data.reset_index().sort_values(by="fusobacteria"),
        ax=ax_scatter,
        **params,
    )
    ax_scatter.set_yscale("symlog", linthresh=0.001)
    ax_scatter.set_xlim(-1, data.shape[0] + 1)
    ax_scatter.set_ylim(-0.5e-4, y_lim)
    ax_scatter.set_xticklabels("")
    ax_scatter.yaxis.set_major_locator(
        matplotlib.ticker.SymmetricalLogLocator(
            transform=None, subs=numpy.arange(0, 1.1, 0.1), linthresh=0.001, base=10
        )
    )

    matplotlib.pyplot.axhline(
        data.fusobacteria.quantile(0.75), color="k", linestyle=":"
    )
    matplotlib.pyplot.annotate(
        " binary cut-off: $75^{th}$ percentile",
        (-1, data.fusobacteria.quantile(0.75)),
        verticalalignment="bottom",
        horizontalalignment="left",
    )
    ax_scatter.axhspan(
        ax_scatter.get_ylim()[0],
        data.fusobacteria.quantile(0.75),
        color=constants.FUSOBACTERIA_BIN_COLORS["low"],
        alpha=0.1,
    )
    ax_scatter.axhspan(
        data.fusobacteria.quantile(0.75),
        ax_scatter.get_ylim()[1],
        color=constants.FUSOBACTERIA_BIN_COLORS["high"],
        alpha=0.1,
    )

    ax_scatter.set_xticks([])
    ax_scatter.set_xlabel("Patients (n={})".format(data.shape[0]))
    ax_scatter.set_ylabel(y_label)
    ax_scatter.legend(loc="upper left")

    fig.savefig(DST_DIR / f"figure_2_panel_{figure_label}.pdf")


def main():
    crc_fusobacteria = fusobacteria_utils.get_crc_fusobacteria()

    figure_2_panels_b_and_d(crc_fusobacteria, "taxonomy")
    figure_2_panels_b_and_d(crc_fusobacteria, "TCGA-COAD-READ")


if __name__ == "__main__":
    DST_DIR.mkdir(parents=True, exist_ok=True)

    matplotlib.rcParams["pdf.fonttype"] = 42
    matplotlib.rcParams["font.family"] = "Arial"
    matplotlib.rcParams["font.size"] = 8

    main()
