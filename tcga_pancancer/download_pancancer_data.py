#!/usr/bin/env python3

""" Download data for the TCGA pancancer collection. """

import os
import subprocess
import sys

import requests
import requests.cookies

PATH_PREFIX = os.path.join("tcga_pancancer", "download", "pancancer")


def download_file(
    url: str, local_filename: str = None, cookie_preload: str = None
) -> None:
    """Download file.
    :param url: the url of the file to download;
    :param local_filename: optional name to use for the local file. Defaults to
           using the filename from the url;
    :param cookie_preload: optional URL to use to pre-load cookies before downloading file.
    """
    # Code from https://stackoverflow.com/a/16696317
    if local_filename is None:
        local_filename = url.split("/")[-1]

    jar = requests.cookies.RequestsCookieJar()
    if cookie_preload:
        # Load other URL first, and pass cookies on to the real request.
        r = requests.get(cookie_preload)
        for step in r.history:
            jar.update(step.cookies)

    # NOTE the stream=True parameter below
    with requests.get(url, stream=True, cookies=jar) as r:
        r.raise_for_status()
        with open(os.path.join(PATH_PREFIX, local_filename), "wb") as f:
            for chunk in r.iter_content(chunk_size=8192):
                if chunk:  # filter out keep-alive new chunks
                    f.write(chunk)


def clinical():
    print(
        "Downloading clinical data from TCGA PanCanAtlas: TCGA-CDR-SupplementalTableS1.xlsx"
    )
    download_file(
        "https://api.gdc.cancer.gov/data/1b5f413e-a8d1-4d10-92eb-7c4ae739ed81",
        local_filename="TCGA-CDR-SupplementalTableS1.xlsx",
    )


def mutations():
    print("Downloading mutations from TCGA PanCanAtlas: mc3.v0.2.8.PUBLIC.maf.gz")
    download_file(
        "http://api.gdc.cancer.gov/data/1c8cfe5f-e52d-41ba-94da-f15ea1337efc",
        local_filename="mc3.v0.2.8.PUBLIC.maf.gz",
    )


def mutations_silent_vs_not_silent():
    print(
        "Downloading # silent/non-silent mutations from TCGA PanCanImmune: mutation-load_updated.txt"
    )
    download_file(
        "https://api.gdc.cancer.gov/data/ff3f962c-3573-44ae-a8f4-e5ac0aea64b6",
        local_filename="mutation-load_updated.txt",
    )


def msi():
    print(
        "Downloading MSI status from Bonneville et al., JCO Pres. Onc., 2017, PMID: 29850653: ds_PO.17.00073-1.xlsx"
    )
    download_file(
        "https://ascopubs.org/doi/suppl/10.1200/PO.17.00073/suppl_file/ds_PO.17.00073-1.xlsx",
        cookie_preload="https://ascopubs.org/doi/suppl/10.1200/PO.17.00073",
    )


def cna():
    print(
        "Downloading cna data from TCGA PanCanAtlas: broad.mit.edu_PANCAN_Genome_Wide_SNP_6_whitelisted.seg"
    )
    download_file(
        "http://api.gdc.cancer.gov/data/00a32f7a-c85f-4f86-850d-be53973cbc4d",
        local_filename="broad.mit.edu_PANCAN_Genome_Wide_SNP_6_whitelisted.seg",
    )


def rna():
    print(
        "Downloading transcriptomics data from TCGA PanCanAtlas: EBPlusPlusAdjustPANCAN_IlluminaHiSeq_RNASeqV2.geneExp.tsv"
    )
    download_file(
        "http://api.gdc.cancer.gov/data/3586c0da-64d0-4b74-a449-5ff4d9136611",
        local_filename="EBPlusPlusAdjustPANCAN_IlluminaHiSeq_RNASeqV2.geneExp.tsv",
    )


def rppa():
    print(
        "Downloading proteomics data from TCGA PanCanAtlas: TCGA-RPPA-pancan-clean.txt"
    )
    download_file(
        "http://api.gdc.cancer.gov/data/fcbb373e-28d4-4818-92f3-601ede3da5e1",
        local_filename="TCGA-RPPA-pancan-clean.txt",
    )


def immuno_scores():
    print(
        "Downloading immuno scores from Thorsson et al., Cell, 2018, PMID: 29628290: NIHMS958212-supplement-2.xlsx"
    )
    download_file(
        "https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5982584/bin/NIHMS958212-supplement-2.xlsx"
    )


def download_pancancer_tcga_data():
    """Download TCGA datasets to 'download' directory."""
    clinical()
    mutations()
    mutations_silent_vs_not_silent()
    msi()
    cna()
    rna()
    rppa()
    immuno_scores()


def check_download_checksums() -> None:
    """Check that checksums of downloaded files match expectations."""
    # Capture output for use in jupyter notebook
    completed_process = subprocess.run(
        ["md5sum", "-c", "tcga_pancancer/download_pancancer.md5"],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
    )
    if completed_process.returncode:
        sys.stderr.write(completed_process.stdout.decode())
        exit(completed_process.returncode)
    else:
        sys.stdout.write(completed_process.stdout.decode())
        print("Downloads completed successfully")


def main():
    download_pancancer_tcga_data()
    check_download_checksums()


if __name__ == "__main__":
    os.makedirs(PATH_PREFIX, exist_ok=True)
    main()
