#!/usr/bin/env python3

""" Compute MCP-counter signatures."""

import pathlib
import pandas

import transcriptomics.signatures
import sklearn.preprocessing

INPUT_PATH_PREFIX = pathlib.Path("tcga_pancancer", "processed_data", "pancancer")
OUTPUT_PATH_PREFIX = pathlib.Path("tcga_pancancer", "analysis_data", "pancancer")


def immuno() -> None:
    """Compute MCP-counter cell types composition (Becht et al., Genome Biol., 2016, PMID: 27765066). """
    rna = pandas.read_csv(
        pathlib.Path(INPUT_PATH_PREFIX, "rna.csv"),
        index_col=["bcr_patient_barcode", "sample_type"],
    )
    rna_transformed = pandas.DataFrame(
        sklearn.preprocessing.QuantileTransformer(
            output_distribution="normal", random_state=1
        ).fit_transform(rna),
        index=rna.index,
        columns=rna.columns,
    )
    rna_log2_zscore = pandas.DataFrame(
        sklearn.preprocessing.RobustScaler().fit_transform(rna_transformed),
        index=rna_transformed.index,
        columns=rna_transformed.columns,
    )

    immuno = transcriptomics.signatures.run_mcp_counter_classifier(rna_log2_zscore)
    immuno.to_csv(pathlib.Path(OUTPUT_PATH_PREFIX, "mcp_counter_immune_cells.csv"))


if __name__ == "__main__":
    pathlib.Path(OUTPUT_PATH_PREFIX).mkdir(parents=True, exist_ok=True)
    immuno()
