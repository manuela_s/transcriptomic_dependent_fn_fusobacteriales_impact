#!/usr/bin/env python3

"""Functions and classes to retrieve clinical and molecular data for the TCGA COAD-READ cohort."""

import os

import pandas

import py_utils.cohort

INPUT_PATH_PREFIX = os.path.join("tcga_pancancer", "analysis_data", "crc")
INPUT_PATH_PREFIX_SIGNATURES = os.path.join(
    "tcga_pancancer", "analysis_data", "pancancer"
)


class TcgaCrc:
    def get_clinical(self):
        df = combine_clinical(include_signatures=False)
        df.rename(
            columns={
                "pfi_months": "dfs_months",
                "pfi_event": "dfs_event",
                "tumour_site": "tumour_site_1",
            },
            inplace=True,
        )

        # Clip survival at 10 years (120 months)
        # All patients with DFS/DSS/OS>=120 months had no events (0)
        df.loc[df.dfs_months.ge(120), "dfs_months"] = 120
        df.loc[df.dss_months.ge(120), "dss_months"] = 120
        df.loc[df.os_months.ge(120), "os_months"] = 120

        df["stage"] = pandas.Categorical(
            df.stage.astype("Int64"), [0, 1, 2, 3, 4], ordered=True
        )
        df["T_stage"] = pandas.Categorical(
            df.T_stage.replace({"Tis": "T1"}), ["T1", "T2", "T3", "T4"]
        )
        df["N_stage"] = pandas.Categorical(df.N_stage, ["N0", "N1", "N2"])
        df["M_stage"] = pandas.Categorical(df.M_stage, ["M0", "M1"])
        df["resection_margins"] = pandas.Categorical(
            df.resection_margins.replace({"R1": "R1_or_R2", "R2": "R2_or_R2"}),
            ["R0", "R1_or_R2"],
        )
        df["cancer_type"] = pandas.Categorical(df.cancer_type, ["COAD", "READ"])
        df["sex"] = pandas.Categorical(df.sex, ["male", "female"])
        df["age"] = df.age.astype("Int64")
        df["has_lymphovascular_invasion"] = pandas.Categorical(
            df.has_lymphovascular_invasion, ["no", "yes"]
        )
        df["has_perineural_invasion"] = pandas.Categorical(
            df.has_perineural_invasion, ["no", "yes"]
        )

        df["pooled_cancer_type"] = df.cancer_type.replace(
            {"COAD": "COAD-READ", "READ": "COAD-READ"}
        )
        df["cohort"] = "TCGA-" + df.pooled_cancer_type.astype("str")
        df = df.rename_axis(index={"bcr_patient_barcode": "patient_id"}).reset_index()
        df.set_index(["cohort", "patient_id"], inplace=True)

        return df[
            [
                "stage",
                "T_stage",
                "N_stage",
                "M_stage",
                "resection_margins",
                "cancer_type",
                "tumour_site_1",
                "sex",
                "age",
                "mucinous_status",
                "bmi_kg_over_squared_m",
                "bmi_bin",
                "has_lymphovascular_invasion",
                "has_perineural_invasion",
                "has_vascular_invasion",
                "has_history_colon_polyps",
                "has_history_other_malignancy",
                "os_months",
                "os_event",
                "dss_months",
                "dss_event",
                "dfs_months",
                "dfs_event",
            ]
        ]

    def get_subtypes(self):
        df = pandas.read_csv(
            os.path.join(
                "tcga_pancancer",
                "analysis_data",
                "pancancer",
                "rna_based_molecular_subtypes.csv",
            ),
            index_col=[1],
        ).query('pooled_cancer_type=="COAD-READ"')

        # Use CMS-RF-nearest as 'cms_class', but set to 'NOLBL' labels with multiple assignments (for example, 'CMS1,CMS2')
        df["cms_class"] = pandas.Categorical(
            df["CMS-RF-nearest"], ["CMS1", "CMS2", "CMS3", "CMS4", "NOLBL"]
        ).fillna("NOLBL")
        df["cris_class"] = df.CRIS

        # Merge into dataset CMS/CRIS labels computed in original CMS/CRIS publications
        tcga = clinical()[["cris_classifier", "cms_classifier"]]
        tcga["cms_classifier"] = tcga.cms_classifier.astype("O").fillna("not_computed")
        tcga["cris_classifier"] = tcga.cris_classifier.astype("O").fillna(
            "not_computed"
        )

        j = tcga.join(df[["cms_class", "cris_class"]]).rename(
            columns={
                "cms_class": "computed_cms_class",
                "cris_class": "computed_cris_class",
            }
        )

        j.query(
            'not (cms_classifier=="not_computed" and cris_classifier=="not_computed" and computed_cms_class.isna()'
            "and computed_cris_class.isna())",
            inplace=True,
        )

        # Replace our computed CMS/CRIS labels with CMS/CRIS labels from original publications when available (subset of
        # CRC patients)
        j.loc[:, "cms_class"] = j.cms_classifier.where(
            j.cms_classifier != "not_computed", j.computed_cms_class
        )
        j.loc[:, "cris_class"] = j.cris_classifier.where(
            j.cris_classifier != "not_computed", j.computed_cris_class
        )
        # A small set of patients ('TCGA-AA-3554', 'TCGA-AA-3860', 'TCGA-AG-A00H', 'TCGA-AG-A014', 'TCGA-AG-A023')
        # included CMS labels from original CMS paper, so are included here. However, those patients are not included
        # in the pancancer rna dataset, so could not compute CRIS labels (not included in CRIS paper either).

        j["pooled_cms_class"] = j.cms_class.replace(
            {
                "CMS1": "not_CMS4",
                "CMS2": "not_CMS4",
                "CMS3": "not_CMS4",
                "NOLBL": "not_CMS4",
            }
        )
        j["pooled_cris_class"] = j.cris_class.replace(
            {
                "CRIS-A": "not_CRIS-B",
                "CRIS-C": "not_CRIS-B",
                "CRIS-D": "not_CRIS-B",
                "CRIS-E": "not_CRIS-B",
                "NOLBL": "not_CRIS-B",
            }
        )
        j["pooled_cms_cris_class"] = (
            j.pooled_cms_class.astype(str) + "_" + j.pooled_cris_class.astype(str)
        )
        j["is_mesenchymal"] = j.pooled_cms_cris_class.replace(
            {
                "not_CMS4_not_CRIS-B": "no",
                "CMS4_not_CRIS-B": "yes",
                "not_CMS4_CRIS-B": "yes",
                "CMS4_CRIS-B": "yes",
                "CMS4_nan": "yes",
                "not_CMS4_nan": None,
            }
        )

        j = j.rename_axis(index={"bcr_patient_barcode": "patient_id"}).reset_index()
        j["cohort"] = "TCGA-COAD-READ"
        j.set_index(["cohort", "patient_id"], inplace=True)

        return j[["cms_class", "cris_class", "is_mesenchymal"]]

    def get_cna(self):
        df = (
            pandas.read_csv(
                os.path.join(INPUT_PATH_PREFIX, "cna.csv"),
                index_col=["bcr_patient_barcode"],
            )
            .query("sample_type==1")
            .drop(columns=["sample_type"])
            .dropna(axis=1)
        )
        df.drop(columns=["num_probes"], inplace=True)

        df["cohort"] = "TCGA-COAD-READ"
        df = (
            df.rename_axis(index={"bcr_patient_barcode": "patient_id"})
            .reset_index()
            .set_index(["cohort", "patient_id"])
        )

        return df

    def get_rna(self, standardization: bool = True):
        df = (
            pandas.read_csv(
                os.path.join(INPUT_PATH_PREFIX, "rna.csv"),
                index_col=["bcr_patient_barcode"],
            )
            .query("sample_type==1")
            .drop(columns=["sample_type"])
            .dropna(axis=1)
        )

        df["cohort"] = "TCGA-COAD-READ"
        df = (
            df.rename_axis(index={"bcr_patient_barcode": "patient_id"})
            .reset_index()
            .set_index(["cohort", "patient_id"])
        )

        if standardization:
            df = py_utils.cohort.standardization_helper(df)

        return df

    def get_rppa(self, standardization: bool = True):
        df = (
            pandas.read_csv(
                os.path.join(INPUT_PATH_PREFIX, "rppa.csv"),
                index_col=["bcr_patient_barcode"],
            )
            .query("sample_type==1")
            .drop(columns=["sample_type"])
            .dropna(axis=1)
        )

        df["cohort"] = "TCGA-COAD-READ"
        df = (
            df.rename_axis(index={"bcr_patient_barcode": "patient_id"})
            .reset_index()
            .set_index(["cohort", "patient_id"])
        )

        if standardization:
            df = py_utils.cohort.standardization_helper(df)

        return df

    def get_mutations(self):
        df = (
            pandas.read_csv(
                os.path.join(
                    INPUT_PATH_PREFIX, "mutations_based_on_selected_variants.csv"
                ),
                index_col=["bcr_patient_barcode"],
            )
            .query("sample_type==1")
            .drop(columns=["sample_type"])
            .dropna(axis=1)
        )

        df["cohort"] = "TCGA-COAD-READ"
        df = (
            df.rename_axis(index={"bcr_patient_barcode": "patient_id"})
            .reset_index()
            .set_index(["cohort", "patient_id"])
        )

        return df

    def get_immuno_composition(self):
        df = (
            pandas.read_csv(
                os.path.join(INPUT_PATH_PREFIX, "mcp_counter_immune_cells.csv"),
                index_col=["bcr_patient_barcode"],
            )
            .query("sample_type==1")
            .drop(columns=["sample_type"])
            .dropna(axis=1)
        )

        df["cohort"] = "TCGA-COAD-READ"
        df = (
            df.rename_axis(index={"bcr_patient_barcode": "patient_id"})
            .reset_index()
            .set_index(["cohort", "patient_id"])
        )

        return df

    def get_quantiseq_cell_types(self):
        df = (
            pandas.read_csv(
                os.path.join(INPUT_PATH_PREFIX, "quantiseq_cell_types.csv"),
                index_col=["bcr_patient_barcode"],
            )
            .query("sample_type==1")
            .drop(columns=["sample_type"])
            .dropna(axis=1)
        )

        df["cohort"] = "TCGA-COAD-READ"
        df = (
            df.rename_axis(index={"bcr_patient_barcode": "patient_id"})
            .reset_index()
            .set_index(["cohort", "patient_id"])
        )

        return df

    def get_rna_based_signatures(self):
        df = (
            pandas.read_csv(
                os.path.join(INPUT_PATH_PREFIX, "computed_rna_based_signatures.csv"),
                index_col=["bcr_patient_barcode"],
            )
            .query("sample_type==1")
            .drop(columns=["sample_type"])
            .dropna(axis=1)
        )

        df["cohort"] = "TCGA-COAD-READ"
        df = (
            df.rename_axis(index={"bcr_patient_barcode": "patient_id"})
            .reset_index()
            .set_index(["cohort", "patient_id"])
        )

        return df

    def get_microsatellite_and_mutational_status(self):
        msi = (
            pandas.read_csv(
                os.path.join(INPUT_PATH_PREFIX, "msi.csv"),
                index_col=["bcr_patient_barcode"],
            )
            .query("sample_type==1")
            .drop(columns=["sample_type"])
        )
        msi["microsatellite_status"] = pandas.Categorical(
            msi.msi_status.replace({"MSI-H": "MSI"}), ["MSS", "MSI"]
        )

        mutations = (
            pandas.read_csv(
                os.path.join(
                    INPUT_PATH_PREFIX, "mutations_based_on_selected_variants.csv"
                ),
                index_col=["bcr_patient_barcode"],
            )
            .query("sample_type==1")
            .drop(columns=["sample_type"])
            .dropna(axis=1)
        )
        mutations = mutations[["KRAS", "NRAS", "HRAS", "MRAS", "BRAF", "TP53"]]
        mutations.columns = mutations.columns.str.lower() + "_status"
        mutations = (
            mutations.astype(int)
            .ge(1)
            .apply(
                lambda x: pandas.Categorical(
                    x.map({True: "mutant", False: "wild_type"}), ["wild_type", "mutant"]
                )
            )
        )

        df = msi[["microsatellite_status"]].join(mutations, how="outer")

        df["cohort"] = "TCGA-COAD-READ"
        df = (
            df.rename_axis(index={"bcr_patient_barcode": "patient_id"})
            .reset_index()
            .set_index(["cohort", "patient_id"])
        )

        return df


def combine_clinical(include_signatures=True):
    cli = pandas.concat(
        [
            pandas.read_csv(
                os.path.join(INPUT_PATH_PREFIX, "clinical.csv"),
                index_col=["bcr_patient_barcode"],
            ),
            pandas.read_csv(
                os.path.join(INPUT_PATH_PREFIX, "supp_clinical.csv"),
                index_col=["bcr_patient_barcode"],
            ),
        ],
        axis=1,
        join="inner",
    )

    if include_signatures:
        msi = (
            pandas.read_csv(
                os.path.join(INPUT_PATH_PREFIX, "msi.csv"),
                index_col=["bcr_patient_barcode"],
            )
            .query("sample_type==1")
            .drop(columns=["sample_type"])
        )
        msi["msi_status"] = msi.msi_status.replace({"MSI-H": "MSI"})

        mutations = (
            pandas.read_csv(
                os.path.join(INPUT_PATH_PREFIX, "mutations_silent_vs_non_silent.csv"),
                index_col=["bcr_patient_barcode"],
            )
            .query("sample_type==1")
            .drop(columns=["sample_type"])
        )

        cms = pandas.read_csv(
            os.path.join(INPUT_PATH_PREFIX, "cms_subtyping.csv"),
            index_col=["bcr_patient_barcode"],
        )
        cris = pandas.read_csv(
            os.path.join(INPUT_PATH_PREFIX, "cris_subtyping.csv"),
            index_col=["bcr_patient_barcode"],
        )

        computed_signatures = (
            pandas.read_csv(
                os.path.join(INPUT_PATH_PREFIX, "computed_rna_based_signatures.csv"),
                index_col=["bcr_patient_barcode"],
            )
            .query("sample_type==1")
            .drop(columns=["sample_type"])
        )

        immuno_scores = pandas.read_csv(
            os.path.join(INPUT_PATH_PREFIX, "immuno_scores.csv"),
            index_col=["bcr_patient_barcode"],
        )[
            [
                "immune_subtype",
                "leukocyte_fraction",
                "stromal_fraction",
                "intratumor_heterogeneity",
                "til_regional_fraction",
            ]
        ]

        j = (
            cli.join(msi)
            .join(mutations)
            .join(cms)
            .join(cris)
            .join(computed_signatures)
            .join(immuno_scores)
        )
    else:
        j = cli

    return j


def clinical(include_signatures=True):
    t = combine_clinical(include_signatures)
    t["cancer_type"] = pandas.Categorical(t.cancer_type, ["COAD", "READ"])
    t["sex"] = pandas.Categorical(t.sex, ["male", "female"])
    t["stage"] = pandas.Categorical(t.stage, [1, 2, 3, 4])
    t["T_stage"] = pandas.Categorical(t.T_stage, ["Tis", "T1", "T2", "T3", "T4"])
    t["N_stage"] = pandas.Categorical(t.N_stage, ["N0", "N1", "N2"])
    t["M_stage"] = pandas.Categorical(t.M_stage, ["M0", "M1"])
    t["resection_margins"] = pandas.Categorical(t.resection_margins, ["R0", "R1", "R2"])
    t["mucinous_status"] = pandas.Categorical(
        t.mucinous_status, ["mucinous", "non_mucinous"]
    )
    t["initial_pathologic_dx_year"] = pandas.Categorical(
        t.initial_pathologic_dx_year.astype("Int64")
    )
    t["tumour_site"] = pandas.Categorical(
        t.tumour_site, ["proximal", "distal", "rectal"]
    )
    t["has_lymphovascular_invasion"] = pandas.Categorical(
        t.has_lymphovascular_invasion, ["yes", "no"]
    )
    t["has_vascular_invasion"] = pandas.Categorical(
        t.has_vascular_invasion, ["yes", "no"]
    )
    t["has_perineural_invasion"] = pandas.Categorical(
        t.has_perineural_invasion, ["yes", "no"]
    )
    t["kras_mutation"] = pandas.Categorical(t.kras_mutation, ["mutated", "wild_type"])
    t["braf_mutation"] = pandas.Categorical(t.braf_mutation, ["mutated", "wild_type"])
    t["bmi_bin"] = pandas.Categorical(
        t.bmi_bin,
        ["under-weight", "normal-weight", "over-weight", "obese", "severely-obese"],
    )
    t["microsatellite_instability"] = pandas.Categorical(
        t.microsatellite_instability, ["MSI", "MSS"]
    )

    if include_signatures:
        t["msi_status"] = pandas.Categorical(t.msi_status, ["MSI", "MSS"])
        t["cms_classifier"] = pandas.Categorical(
            t.cms_classifier, ["CMS1", "CMS2", "CMS3", "CMS4", "NOLBL"]
        )
        t["cris_classifier"] = pandas.Categorical(
            t.cris_classifier, ["CRIS-A", "CRIS-B", "CRIS-C", "CRIS-D", "CRIS-E"]
        )
        t["immune_subtype"] = pandas.Categorical(
            t.immune_subtype, ["C1", "C2", "C3", "C4", "C5", "C6"]
        )

    return t
