#!/usr/bin/env python3

"""Fetch gene mappings from mygene."""

import os

import transcriptomics.map_ids_from_ensembl

OUTPUT_PATH_PREFIX = os.path.join("tcga_pancancer", "processed_data", "pancancer")


def fetch_gene_mapping():
    transcriptomics.map_ids_from_ensembl.create_rna_mapping_file(
        os.path.join(OUTPUT_PATH_PREFIX, "rna.csv")
    )


if __name__ == "__main__":
    fetch_gene_mapping()
