#!/usr/bin/env python3

""" Prepare CRC specific datasets for analysis."""

import os

import numpy
import pandas

PANCANCER_INPUT_PATH_PREFIX = os.path.join(
    "tcga_pancancer", "processed_data", "pancancer"
)
INPUT_PATH_PREFIX = os.path.join("tcga_pancancer", "download", "crc")
LOCAL_PATH_PREFIX = os.path.join("tcga_pancancer", "data")
OUTPUT_PATH_PREFIX = os.path.join("tcga_pancancer", "analysis_data", "crc")
PANCANCER_OUTPUT_PATH_PREFIX = os.path.join(
    "tcga_pancancer", "analysis_data", "pancancer"
)


def get_crc_patient_ids() -> pandas.Index:
    """
    Select TCGA-COAD-READ patients.
    :return: index with patient_ids for selected patients.
    """
    patients = (
        pandas.read_csv(
            os.path.join(PANCANCER_INPUT_PATH_PREFIX, "clinical.csv"),
            index_col=["bcr_patient_barcode"],
        )
        .query('cancer_type.isin(["COAD", "READ"])')
        .index.get_level_values("bcr_patient_barcode")
    )

    return patients


def clinical(patients: pandas.Index) -> None:
    print("Preparing clinical data")

    t = pandas.read_csv(
        os.path.join(PANCANCER_INPUT_PATH_PREFIX, "clinical.csv"),
        index_col=["bcr_patient_barcode"],
    ).query("bcr_patient_barcode.isin(@patients)")

    # Drop columns with missing data for all patients
    t.drop(columns=t.columns[t.isna().all(axis=0)], inplace=True)

    t["race"] = t.race.str.lower().str.replace(" ", "_")
    t["sex"] = t.sex.str.lower()

    t["mucinous_status"] = t.histological_type.map(
        {
            "Colon Adenocarcinoma": "non_mucinous",
            "Rectal Adenocarcinoma": "non_mucinous",
            "Colon Mucinous Adenocarcinoma": "mucinous",
            "Rectal Mucinous Adenocarcinoma": "mucinous",
        }
    )
    t.drop(columns=["histological_type"], inplace=True)

    t[
        "treatment_outcome_first_course"
    ] = t.treatment_outcome_first_course.str.lower().str.replace(" |/", "_")

    t = t[
        [
            "cancer_type",
            "age",
            "sex",
            "race",
            "stage",
            "mucinous_status",
            "initial_pathologic_dx_year",
            "treatment_outcome_first_course",
            "os_months",
            "os_event",
            "dss_months",
            "dss_event",
            "dfi_months",
            "dfi_event",
            "pfi_months",
            "pfi_event",
        ]
    ]
    t.to_csv(os.path.join(OUTPUT_PATH_PREFIX, "clinical.csv"))


def supplementary_clinical() -> None:
    print("Preparing supplementary clinical data")

    cli_coad_sup = pandas.read_csv(
        os.path.join(
            LOCAL_PATH_PREFIX, "nationwidechildrens.org_clinical_patient_coad.txt"
        ),
        sep="\t",
        na_values=[
            "[Not Available]",
            "[Unknown]",
            "[Discrepancy]",
            "[Not Evaluated]",
            "[Not Applicable]",
        ],
        skiprows=[1, 2],
        index_col=["bcr_patient_barcode"],
    )
    cli_read_sup = pandas.read_csv(
        os.path.join(
            LOCAL_PATH_PREFIX, "nationwidechildrens.org_clinical_patient_read.txt"
        ),
        sep="\t",
        na_values=[
            "[Not Available]",
            "[Unknown]",
            "[Discrepancy]",
            "[Not Evaluated]",
            "[Not Applicable]",
        ],
        skiprows=[1, 2],
        index_col=["bcr_patient_barcode"],
    )

    t = pandas.concat([cli_coad_sup, cli_read_sup], axis=0)

    # Drop columns with missing values for all patients
    t.drop(columns=t.columns[t.isna().all(axis=0)], inplace=True)

    keep_cols = [
        "ethnicity",
        "tissue_source_site",
        "tumor_tissue_site",
        "anatomic_neoplasm_subdivision",
        "lymphovascular_invasion_indicator",
        "vascular_invasion_indicator",
        "perineural_invasion",
        "ajcc_staging_edition",
        "ajcc_pathologic_tumor_stage",
        "ajcc_tumor_pathologic_pt",
        "ajcc_nodes_pathologic_pn",
        "ajcc_metastasis_pathologic_pm",
        "radiation_treatment_adjuvant",
        "pharmaceutical_tx_adjuvant",
        "history_colon_polyps",
        "history_neoadjuvant_treatment",
        "history_other_malignancy",
        "history_other_malignancy.1",  # harmonize
        "kras_mutation_found",
        "kras_mutation_codon",
        "braf_gene_analysis_result",
        "colon_polyps_at_procurement_indicator",
        "lymph_nodes_examined_count",
        "lymph_nodes_examined_he_count",
        "microsatellite_instability",
        "mismatch_rep_proteins_tested_by_ihc",
        "mismatch_rep_proteins_loss_ihc",
        "weight_kg_at_diagnosis",
        "height_cm_at_diagnosis",
        "cea_level_pretreatment",
        "circumferential_resection_margin_crm",
        "specimen_non_node_tumor_deposits",
        "loci_tested_count",
        "loci_abnormal_count",
        "residual_tumor",
        "lymph_nodes_examined",
        "lymph_nodes_examined_count",
        "lymph_nodes_examined_he_count",
        "lymph_nodes_examined_ihc_count",
        "family_history_colorectal_cancer",
        "treatment_outcome_first_course",
        "days_to_initial_pathologic_diagnosis",
        "icd_10",
        "icd_o_3_histology",
        "icd_o_3_site",
    ]
    t = t.loc[:, keep_cols]

    t.rename(
        columns={
            "anatomic_neoplasm_subdivision": "tumour_site",
            "tissue_source_site": "clinical_site",
            "residual_tumor": "resection_margins",
            "lymphovascular_invasion_indicator": "has_lymphovascular_invasion",
            "vascular_invasion_indicator": "has_vascular_invasion",
            "perineural_invasion": "has_perineural_invasion",
            "history_other_malignancy": "has_history_other_malignancy",
            "history_colon_polyps": "has_history_colon_polyps",
            "ajcc_tumor_pathologic_pt": "T_stage",
            "ajcc_nodes_pathologic_pn": "N_stage",
            "ajcc_metastasis_pathologic_pm": "M_stage",
            "colon_polyps_at_procurement_indicator": "has_colon_polyps_at_procurement",
            "kras_mutation_found": "kras_mutation",
            "braf_gene_analysis_result": "braf_mutation",
        },
        inplace=True,
    )

    # Tumor site mapping from cohort_utilities.PoolTumourSite.m
    t["tumour_site"] = t.tumour_site.map(
        {
            "Sigmoid Colon": "distal",
            "Rectum": "rectal",
            "Ascending Colon": "proximal",
            "Rectosigmoid Junction": "rectal",
            "Transverse Colon": "proximal",
            "Hepatic Flexure": "proximal",
            "Descending Colon": "distal",
            "Splenic Flexure": "proximal",
            "Cecum": "proximal",
        }
    )

    t["ethnicity"] = t.ethnicity.str.lower().str.replace(" ", "_")
    t["has_lymphovascular_invasion"] = t.has_lymphovascular_invasion.str.lower()
    t["has_vascular_invasion"] = t.has_vascular_invasion.str.lower()
    t["has_perineural_invasion"] = t.has_perineural_invasion.str.lower()
    t["radiation_treatment_adjuvant"] = t.radiation_treatment_adjuvant.str.lower()
    t["pharmaceutical_tx_adjuvant"] = t.pharmaceutical_tx_adjuvant.str.lower()
    t["has_history_neoadjuvant_treatment"] = t.history_neoadjuvant_treatment.str.lower()
    t["has_colon_polyps_at_procurement"] = t.has_colon_polyps_at_procurement.str.lower()
    t["has_history_colon_polyps"] = t.has_history_colon_polyps.str.lower()
    t["microsatellite_instability"] = t.microsatellite_instability.map(
        {"YES": "MSI", "NO": "MSS"}
    )
    t["kras_mutation"] = t.kras_mutation.map({"YES": "mutated", "NO": "wild_type"})
    t["braf_mutation"] = t.braf_mutation.map(
        {"Abnormal": "mutated", "Normal": "wild_type"}
    )

    t["T_stage"] = t.T_stage.replace({"T4a": "T4", "T4b": "T4"})

    t["N_stage"] = t.N_stage.replace(
        {"N1b": "N1", "N2b": "N2", "N1a": "N1", "N2a": "N2", "N1c": "N1", "NX": None}
    )

    t["M_stage"] = t.M_stage.replace({"MX": None, "M1a": "M1", "M1b": "M1"})

    t["resection_margins"] = t.resection_margins.replace({"RX": None})

    t["has_history_other_malignancy"] = t.has_history_other_malignancy.map(
        {
            "No": "no",
            "Yes": "yes",
            "Yes, History of Synchronous/Bilateral Malignancy": "yes",
        }
    )

    # Compute BMI (kg/m^2)
    t["bmi_kg_over_squared_m"] = t.weight_kg_at_diagnosis / (
        (t.height_cm_at_diagnosis / 100) ** 2
    )
    # Set record for 'TCGA-NH-A8F7' to NaN (probably weight and height got swapped in the annotation?)
    # j.loc['TCGA-NH-A8F7', ['height_cm_at_diagnosis', 'weight_kg_at_diagnosis', 'bmi_kg_over_squared_m']]
    # height_cm_at_diagnosis       80.3
    # weight_kg_at_diagnosis      175.3
    # bmi_kg_over_squared_m     271.863
    # Name: TCGA-NH-A8F7, dtype: object
    t.loc["TCGA-NH-A8F7", "bmi_kg_over_squared_m"] = None

    t["bmi_bin"] = pandas.cut(
        t.bmi_kg_over_squared_m,
        [
            numpy.floor(t.bmi_kg_over_squared_m.min()),
            18.5,
            25,
            30,
            45,
            numpy.ceil(t.bmi_kg_over_squared_m.max()),
        ],
        labels=[
            "under-weight",
            "normal-weight",
            "over-weight",
            "obese",
            "severely-obese",
        ],
    )

    t.drop(columns=["weight_kg_at_diagnosis", "height_cm_at_diagnosis"], inplace=True)

    t.to_csv(os.path.join(OUTPUT_PATH_PREFIX, "supp_clinical.csv"))


def mutations_based_on_selected_variants(patients: pandas.Index) -> None:
    print("Preparing mutation data based on selected variants")

    pandas.read_csv(
        os.path.join(
            PANCANCER_INPUT_PATH_PREFIX, "mutations_based_on_selected_variants.csv"
        ),
        index_col=["bcr_patient_barcode", "sample_type"],
    ).query("bcr_patient_barcode.isin(@patients)").to_csv(
        os.path.join(OUTPUT_PATH_PREFIX, "mutations_based_on_selected_variants.csv")
    )


def mutations_silent_vs_not_silent(patients: pandas.Index) -> None:
    print("Preparing silent vs. non-silent mutations data")

    pandas.read_csv(
        os.path.join(PANCANCER_INPUT_PATH_PREFIX, "mutations_silent_vs_non_silent.csv"),
        index_col=["bcr_patient_barcode", "sample_type"],
    ).query("bcr_patient_barcode.isin(@patients)").to_csv(
        os.path.join(OUTPUT_PATH_PREFIX, "mutations_silent_vs_non_silent.csv")
    )


def msi(patients: pandas.Index) -> None:
    print(
        "Preparing MSI status from Bonneville et al., JCO Pres. Onc., 2017, PMID: 29850653"
    )

    pandas.read_csv(
        os.path.join(PANCANCER_INPUT_PATH_PREFIX, "msi.csv"),
        index_col=["bcr_patient_barcode", "sample_type"],
    ).query("bcr_patient_barcode.isin(@patients)").to_csv(
        os.path.join(OUTPUT_PATH_PREFIX, "msi.csv")
    )


def cna(patients: pandas.Index) -> None:
    cna = pandas.read_csv(os.path.join(PANCANCER_INPUT_PATH_PREFIX, "cna.csv"))
    cna.set_index(["bcr_patient_barcode", "sample_type", "chromosome"], inplace=True)
    cna.query("bcr_patient_barcode.isin(@patients)").to_csv(
        os.path.join(OUTPUT_PATH_PREFIX, "cna.csv")
    )


def rna(patients: pandas.Index) -> None:
    pandas.read_csv(
        os.path.join(PANCANCER_INPUT_PATH_PREFIX, "rna.csv"),
        index_col=["bcr_patient_barcode", "sample_type"],
    ).query("bcr_patient_barcode.isin(@patients)").to_csv(
        os.path.join(OUTPUT_PATH_PREFIX, "rna.csv")
    )


def rppa(patients: pandas.Index) -> None:
    pandas.read_csv(
        os.path.join(PANCANCER_INPUT_PATH_PREFIX, "rppa.csv"),
        index_col=["bcr_patient_barcode", "sample_type"],
    ).query("bcr_patient_barcode.isin(@patients)").to_csv(
        os.path.join(OUTPUT_PATH_PREFIX, "rppa.csv")
    )


def immuno_scores(patients: pandas.Index) -> None:
    pandas.read_csv(
        os.path.join(PANCANCER_INPUT_PATH_PREFIX, "immuno_scores.csv"),
        index_col=["bcr_patient_barcode"],
    ).query("bcr_patient_barcode.isin(@patients)").to_csv(
        os.path.join(OUTPUT_PATH_PREFIX, "immuno_scores.csv")
    )


def consensus_molecular_subtyping() -> None:
    t = pandas.read_csv(
        os.path.join(INPUT_PATH_PREFIX, "cms_labels_public_all.txt"), delimiter="\t"
    )
    t = t[t.dataset == "tcga"]
    t.rename(
        columns={
            "sample": "bcr_patient_barcode",
            "CMS_final_network_plus_RFclassifier_in_nonconsensus_samples": "cms_classifier",
        },
        inplace=True,
    )

    t.set_index("bcr_patient_barcode", inplace=True)
    t[["cms_classifier"]].to_csv(os.path.join(OUTPUT_PATH_PREFIX, "cms_subtyping.csv"))


def cris_subtyping() -> None:
    t = pandas.read_excel(
        os.path.join(INPUT_PATH_PREFIX, "41467_2017_BFncomms15107_MOESM425_ESM.xlsx"),
        skiprows=2,
        usecols=["Dataset ID", "Sample ID", "CRIS Assignment"],
    )
    t.rename(
        columns={
            "Dataset ID": "dataset",
            "Sample ID": "bcr_patient_barcode",
            "CRIS Assignment": "cris_classifier",
        },
        inplace=True,
    )
    t["cris_classifier"] = pandas.Categorical(
        t.cris_classifier.str.replace("CRIS-?", "CRIS-"),
        ["CRIS-A", "CRIS-B", "CRIS-C", "CRIS-D", "CRIS-E"],
    )
    t = t[t.dataset == "TCGA"]
    t.set_index("bcr_patient_barcode", inplace=True)
    t[["cris_classifier"]].to_csv(
        os.path.join(OUTPUT_PATH_PREFIX, "cris_subtyping.csv")
    )


def computed_rna_signatures(patients: pandas.Index) -> None:
    pandas.read_csv(
        os.path.join(PANCANCER_OUTPUT_PATH_PREFIX, "computed_rna_based_signatures.csv"),
        index_col=["bcr_patient_barcode", "sample_type"],
    ).query("bcr_patient_barcode.isin(@patients)").to_csv(
        os.path.join(OUTPUT_PATH_PREFIX, "computed_rna_based_signatures.csv")
    )


def computed_mcp_signatures(patients: pandas.Index) -> None:
    pandas.read_csv(
        os.path.join(PANCANCER_OUTPUT_PATH_PREFIX, "mcp_counter_immune_cells.csv"),
        index_col=["bcr_patient_barcode", "sample_type"],
    ).query("bcr_patient_barcode.isin(@patients)").to_csv(
        os.path.join(OUTPUT_PATH_PREFIX, "mcp_counter_immune_cells.csv")
    )


def main():
    print(f"Preparing TCGA-COAD-READ data and save in {OUTPUT_PATH_PREFIX}")

    patients = get_crc_patient_ids()

    clinical(patients)
    supplementary_clinical()
    mutations_based_on_selected_variants(patients)
    mutations_silent_vs_not_silent(patients)
    msi(patients)
    cna(patients)
    rna(patients)
    rppa(patients)
    immuno_scores(patients)
    consensus_molecular_subtyping()
    cris_subtyping()
    computed_rna_signatures(patients)
    computed_mcp_signatures(patients)


if __name__ == "__main__":
    os.makedirs(OUTPUT_PATH_PREFIX, exist_ok=True)
    main()
