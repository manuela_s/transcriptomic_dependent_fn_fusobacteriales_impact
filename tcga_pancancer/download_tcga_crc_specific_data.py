#!/usr/bin/env python3

""" Download data specific to the TCGA colorectal (COAD-READ) cancer cohort. """

import getpass
import os
import subprocess
import sys
import urllib.request

import synapseclient
import synapseutils


PATH_PREFIX = os.path.join("tcga_pancancer", "download", "crc")


def synapse_login() -> synapseclient.Synapse:
    """Ask user for synapse login details.
    :return: synapse object.
    """
    while True:
        email_address = input("Synapse email address: ")
        password = getpass.getpass("Synapse password (hidden): ")
        try:
            syn = synapseclient.login(email=email_address, password=password)
        except synapseclient.exceptions.SynapseAuthenticationError as e:
            print(e)
            continue
        else:
            break
    return syn


def cms_subtyping(syn: synapseclient.Synapse) -> None:
    print(
        "Downloading CMS subtyping data from synapse entity syn4978511: cms_labels_public_all.txt"
    )
    synapseutils.syncFromSynapse(
        syn, entity="syn4978511", path=PATH_PREFIX
    )  # Full data for CMS paper at syn2623706


def cris_subtyping() -> None:
    print(
        "Downloading CRIS subtyping from Isella, Nat. Comm.; PMID: 28561063: 41467_2017_BFncomms15107_MOESM425_ESM.xlsx"
    )
    urllib.request.urlretrieve(
        "https://static-content.springer.com/esm/art%3A10.1038%2Fncomms15107/MediaObjects/41467_2017_BFncomms15107_MOESM425_ESM.xlsx",
        os.path.join(PATH_PREFIX, "41467_2017_BFncomms15107_MOESM425_ESM.xlsx"),
    )


def download_tcga_crc_specific_data(syn: synapseclient.Synapse) -> None:
    """Download TCGA datasets to 'download' directory."""
    cms_subtyping(syn)
    cris_subtyping()


def check_crc_specific_download_checksums() -> None:
    """Check that checksums of downloaded files match expectations."""
    # Capture output for use in jupyter notebook
    completed_process = subprocess.run(
        ["md5sum", "-c", "tcga_pancancer/download_crc.md5"],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
    )
    if completed_process.returncode:
        sys.stderr.write(completed_process.stdout.decode())
        exit(completed_process.returncode)
    else:
        sys.stdout.write(completed_process.stdout.decode())
        print("Downloads completed successfully")


def main():
    syn = synapse_login()
    download_tcga_crc_specific_data(syn)
    check_crc_specific_download_checksums()


if __name__ == "__main__":
    os.makedirs(PATH_PREFIX, exist_ok=True)
    main()
