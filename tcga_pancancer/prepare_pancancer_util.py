#!/usr/bin/env python3

""" Prepare TCGA pancancer datasets for analysis."""

import os

import dask.dataframe
import pandas

INPUT_PATH_PREFIX = os.path.join("tcga_pancancer", "download", "pancancer")
LOCAL_PATH_PREFIX = os.path.join("tcga_pancancer", "data")
OUTPUT_PATH_PREFIX = os.path.join("tcga_pancancer", "processed_data", "pancancer")


def clinical() -> None:
    print("Preparing clinical data")
    t = pandas.read_excel(
        os.path.join(INPUT_PATH_PREFIX, "TCGA-CDR-SupplementalTableS1.xlsx"),
        index_col=0,
        na_values=[
            "[Not Available]",
            "[Unknown]",
            "[Discrepancy]",
            "[Not Evaluated]",
            "[Not Applicable]",
        ],
        engine="openpyxl",
    )
    t.columns = t.columns.str.lower()
    t.rename(
        columns={
            "type": "cancer_type",
            "age_at_initial_pathologic_diagnosis": "age",
            "gender": "sex",
            "os": "os_event",
            "dss": "dss_event",
            "dfi": "dfi_event",
            "pfi": "pfi_event",
        },
        inplace=True,
    )

    # Include only non-redacted patients
    t = t.loc[t.redaction != "Redacted"]
    t.drop(columns=["redaction"], inplace=True)

    # Cancer staging info is filled in for some cancer types under 'ajcc_pathologic_tumor_stage', for others under
    # 'clinical_stage', for others is never filled and in some cases is filled for both (and may have inconsistencies)
    # Use:
    # - 'ajcc_pathologic_tumor_stage' when filled in
    # - 'clinical_stage' when 'ajcc_pathologic_tumor_stage' is not filled in
    # - hard-code staging for GBM to 4
    t["stage"] = t.ajcc_pathologic_tumor_stage.where(
        t.ajcc_pathologic_tumor_stage.notna(), t.clinical_stage
    )
    t["stage"] = t.stage.replace(
        {
            "I": "1",
            "I/II NOS": "1_2",
            "III": "3",
            "IIa": "2",
            "IIb": "2",
            "IS": "is",
            "IVa": "4",
            "IVb": "4",
            "Stage 0": "0",
            "Stage I": "1",
            "Stage IA": "1",
            "Stage IA1": "1",
            "Stage IA2": "1",
            "Stage IB": "1",
            "Stage IB1": "1",
            "Stage IB2": "1",
            "Stage IC": "1",
            "Stage II": "2",
            "Stage IIA": "2",
            "Stage IIA1": "2",
            "Stage IIA2": "2",
            "Stage IIB": "2",
            "Stage IIC": "2",
            "Stage III": "3",
            "Stage IIIA": "3",
            "Stage IIIB": "3",
            "Stage IIIC": "3",
            "Stage IIIC1": "3",
            "Stage IIIC2": "3",
            "Stage IS": "is",
            "Stage IV": "4",
            "Stage IVA": "4",
            "Stage IVB": "4",
            "Stage IVC": "4",
            "Stage X": None,
        }
    )
    t.loc[t.cancer_type == "GBM", "stage"] = "4"

    t["race"] = t.race.str.lower().str.replace(" ", "_")
    t["sex"] = t.sex.str.lower()

    # Combine info for residual_tumor which is available under 'residual_tumor' for most cancer types and under
    # 'margin_status' for BRCA. Note SARC has info under both columns (which mostly agree)
    # Map values for BRCA to R0,1,2 categories based on https://www.breastcancer.org/symptoms/diagnosis/margins
    t["residual_tumor"] = t.residual_tumor.where(
        t.residual_tumor.notna(), t.margin_status
    )
    t["residual_tumor"] = t.residual_tumor.replace(
        {"Negative": "R0", "Close": "R1", "Positive": "R2", "RX": None}
    )

    # Map histological grade based on https://www.cancer.gov/about-cancer/diagnosis-staging/prognosis/tumor-grade-fact-sheet
    t["histological_grade"] = t.histological_grade.replace(
        {
            "High Grade": "G4",
            "Low Grade": "G1",
            "GB": "G2",  # assume 'B' indicates '2'
            "GX": None,
        }
    )

    t["menopause_status"] = t.menopause_status.replace(
        {
            "Post (prior bilateral ovariectomy OR >12 mo since LMP with no prior hysterectomy)": "post",
            "Pre (<6 months since LMP AND no prior bilateral ovariectomy AND not on estrogen replacement)": "pre",
            "Peri (6-12 months since last menstrual period)": "peri",
            "Indeterminate (neither Pre or Postmenopausal)": None,
        }
    )
    # Set 'menopause_status' to 'post' for women over 60 (conservative based on https://www.nhs.uk/conditions/menopause/#:~:text=The%20menopause%20is%20a%20natural,before%2040%20years%20of%20age.)
    t["menopause_status2"] = t.menopause_status.where(
        ~((t.age > 60) & (t.sex == "female")), "post"
    )

    # TODO: clean up histological_type and treatment_outcome_first_course
    t["os_months"] = (t["os.time"] / (365 / 12)).round()
    t["dss_months"] = (t["dss.time"] / (365 / 12)).round()
    t["dfi_months"] = (t["dfi.time"] / (365 / 12)).round()
    t["pfi_months"] = (t["pfi.time"] / (365 / 12)).round()

    t = t[
        [
            "bcr_patient_barcode",
            "cancer_type",
            "age",
            "sex",
            "race",
            "stage",
            "residual_tumor",
            "menopause_status",
            "histological_type",
            "histological_grade",
            "initial_pathologic_dx_year",
            "treatment_outcome_first_course",
            "os_months",
            "os_event",
            "dss_months",
            "dss_event",
            "dfi_months",
            "dfi_event",
            "pfi_months",
            "pfi_event",
        ]
    ]

    t.set_index("bcr_patient_barcode", inplace=True)

    t.to_csv(os.path.join(OUTPUT_PATH_PREFIX, "clinical.csv"))


def mutations_based_on_selected_variants() -> None:
    print("Preparing mutation data based on selected variants")

    maf = dask.dataframe.read_table(
        os.path.join(INPUT_PATH_PREFIX, "mc3.v0.2.8.PUBLIC.maf"),
        usecols=[
            "Tumor_Sample_Barcode",
            "Hugo_Symbol",
            "Variant_Classification",
            "Variant_Type",
        ],
    )

    all_samples = maf.Tumor_Sample_Barcode.unique().compute()

    # Calculate the max impact per sample, symbol
    maf_agg = (
        maf.groupby(["Tumor_Sample_Barcode", "Hugo_Symbol", "Variant_Classification"])
        .size()
        .to_frame("n")
        .n.compute()
    )

    # Exclude variants: ["3'Flank", "3'UTR", "5'Flank", "5'UTR", "Intron", "RNA", "Silent", "Nonstop_Mutation",
    # "Translation_Start_Site"
    variants_to_include = [
        "Frame_Shift_Del",
        "Frame_Shift_Ins",
        "In_Frame_Del",
        "In_Frame_Ins",
        "Missense_Mutation",
        "Nonsense_Mutation",
        "Splice_Site",
    ]
    # Unstack, to make the table have one column per symbol, one row per sample/patient.
    maf_wide = (
        maf_agg.unstack("Variant_Classification")[variants_to_include]
        .sum(axis=1)
        .unstack("Hugo_Symbol", fill_value=0)
    )

    # We assume that all samples were present in the original file, but some got removed when we subset to
    # interesting genes. Reindex with the original set of samples to insert nans for samples that were tested, but
    # did not have any mutations.
    maf_wide = maf_wide.reindex(all_samples)
    maf_wide["bcr_patient_barcode"] = maf_wide.index.str[:12]
    maf_wide["sample_type"] = maf_wide.index.str[13:15]
    maf_wide.set_index(["bcr_patient_barcode", "sample_type"], inplace=True)

    maf_wide.to_csv(
        os.path.join(OUTPUT_PATH_PREFIX, "mutations_based_on_selected_variants.csv")
    )


def mutations_silent_vs_not_silent() -> None:
    print("Preparing silent vs. non-silent mutations data")

    # Mutations data downloaded from https://gdc.cancer.gov/about-data/publications/panimmune
    t = pandas.read_csv(
        os.path.join(INPUT_PATH_PREFIX, "mutation-load_updated.txt"), sep="\t"
    )
    t.columns = t.columns.str.lower().str.replace(" ", "_")
    t.rename(
        columns={
            "patient_id": "bcr_patient_barcode",
            "tumor_sample_id": "bcr_sample_barcode",
            "non-silent_per_mb": "non_silent_per_mb",
        },
        inplace=True,
    )
    t["sample_type"] = t.bcr_sample_barcode.str[13:15]

    t.set_index(["bcr_patient_barcode", "sample_type"], inplace=True)

    # Remove unused columns
    t.drop(columns=["cohort", "bcr_sample_barcode"], inplace=True)

    t.to_csv(os.path.join(OUTPUT_PATH_PREFIX, "mutations_silent_vs_non_silent.csv"))


def msi() -> None:
    print(
        "Preparing MSI status from Bonneville et al., JCO Pres. Onc., 2017, PMID: 29850653"
    )

    t = pandas.read_excel(
        os.path.join(INPUT_PATH_PREFIX, "ds_PO.17.00073-1.xlsx"), engine="openpyxl"
    )

    t.columns = t.columns.str.lower().str.replace(" ", "_")
    t.rename(columns={"case_id": "bcr_patient_barcode"}, inplace=True)

    # Subset to patients from the TCGA project
    t = t.loc[t.cancer_type.str.startswith("TCGA")]
    t.loc[:, "stripped_tumor_filename"] = t.tumor_filename.str.extract(
        "(TCGA-[0-9A-Z]{2}-[A-Z0-9]{4}-[0-9A-Z]{3})"
    ).values
    t.loc[:, "sample_type"] = t.stripped_tumor_filename.str[13:15]

    # Drop unused columns
    t.drop(
        columns=[
            "cancer_type",
            "stripped_tumor_filename",
            "tumor_filename",
            "tumor_file_id",
            "tumor_download_date",
            "normal_filename",
            "normal_file_id",
            "normal_download_date",
        ],
        inplace=True,
    )

    # Bin  samples as MSI-H or MSS based on MANTIS score and cut-off of 0.4 (see https://www.ncbi.nlm.nih.gov/pubmed/29850653)
    assert t.mantis_score.isna().sum() == 0, "MANTIS score is missing for some patients"
    t["msi_status"] = (t.mantis_score > 0.4).replace({True: "MSI-H", False: "MSS"})
    t.set_index(["bcr_patient_barcode", "sample_type"], inplace=True)

    t.to_csv(os.path.join(OUTPUT_PATH_PREFIX, "msi.csv"))


def cna() -> None:
    """"Read in TCGA Pancancer curated cna data
    (https://gdc.cancer.gov/about-data/publications/pancanatlas)."""
    print("Preparing CNA data")

    t = pandas.read_csv(
        os.path.join(
            INPUT_PATH_PREFIX, "broad.mit.edu_PANCAN_Genome_Wide_SNP_6_whitelisted.seg"
        ),
        dtype={"Start": int, "End": int, "Num_Probes": int},
        index_col=0,
        sep="\t",
    )

    # Add identifier as per standard TCGA nomenclature (https://docs.gdc.cancer.gov/Encyclopedia/pages/TCGA_Barcode/)
    t["bcr_patient_barcode"] = t.index.get_level_values("Sample").str[0:12]
    t["sample_type"] = t.index.get_level_values("Sample").str[13:15]

    t.columns = t.columns.str.lower()
    t.rename(columns={"segment_mean": "logr"}, inplace=True)
    t["chromosome"] = ("chr" + t.chromosome.astype("str")).replace({"chr23": "chrX"})

    t.reset_index(drop=True, inplace=True)
    t.set_index(["bcr_patient_barcode", "sample_type", "chromosome"], inplace=True)

    t.to_csv(os.path.join(OUTPUT_PATH_PREFIX, "cna.csv"))


def rna() -> None:
    """"Read in TCGA Pancancer curated level 4 transcriptomics data
    (https://gdc.cancer.gov/about-data/publications/pancanatlas)."""
    print("Preparing RNA data")

    t = pandas.read_csv(
        os.path.join(
            INPUT_PATH_PREFIX,
            "EBPlusPlusAdjustPANCAN_IlluminaHiSeq_RNASeqV2.geneExp.tsv",
        ),
        index_col=0,
        sep="\t",
    )

    # Transpose to have samples in rows and genes in columns
    t = t.T

    # Add identifier as per standard TCGA nomenclature (https://docs.gdc.cancer.gov/Encyclopedia/pages/TCGA_Barcode/)
    t.index.rename("id", inplace=True)
    t["bcr_patient_barcode"] = t.index.get_level_values("id").str[0:12]
    t["sample_type"] = t.index.get_level_values("id").str[13:15]

    # Aggregate values by median for patients with multiple measurements from the same 'sample_type'.
    # Patients affected include: 'TCGA-06-0211', 'TCGA-FG-5965', 'TCGA-TQ-A7RK', 'TCGA-DU-6404', 'TCGA-06-0156',
    # 'TCGA-DU-6407', 'TCGA-23-1023', 'TCGA-21-1076', 'TCGA-DD-AACA', 'TCGA-BF-A3DL'
    t = t.groupby(["bcr_patient_barcode", "sample_type"]).median()

    # Clean-up gene names
    t.columns = t.columns.str.replace("\\|[0-9]+", "")
    # Drop genes with names with '?'
    t = t[t.columns[~t.columns.str.startswith("?")]]

    t.to_csv(os.path.join(OUTPUT_PATH_PREFIX, "rna.csv"))


def rppa() -> None:
    """"Read in TCGA Pancancer curated level 4 proteomics data
    (https://gdc.cancer.gov/about-data/publications/pancanatlas)."""
    print("Preparing RPPA data")

    t = pandas.read_csv(
        os.path.join(INPUT_PATH_PREFIX, "TCGA-RPPA-pancan-clean.txt"), sep="\t"
    )

    t.columns = t.columns.str.lower()
    t.rename(columns={"tumortype": "cancer_type"}, inplace=True)

    # Add identifier as per standard TCGA nomenclature (https://docs.gdc.cancer.gov/Encyclopedia/pages/TCGA_Barcode/)
    t["bcr_patient_barcode"] = t.sampleid.str[0:12]
    t["sample_type"] = t.sampleid.str[13:15]

    t.set_index(
        ["cancer_type", "bcr_patient_barcode", "sample_type", "sampleid"], inplace=True
    )

    assert (
        t.groupby(["cancer_type", "bcr_patient_barcode", "sample_type"])
        .size()
        .values.max()
        == 1
    ), "There are more that one measurement per patient per sample type. Handle aggregation"

    t.reset_index(["cancer_type", "sampleid"], drop=True, inplace=True)

    t.to_csv(os.path.join(OUTPUT_PATH_PREFIX, "rppa.csv"))


def immuno_scores() -> None:
    print("Preparing immuno scores from Thorsson et al., Cell, 2018, PMID: 29628290")

    t = pandas.read_excel(
        os.path.join(INPUT_PATH_PREFIX, "NIHMS958212-supplement-2.xlsx"),
        sheet_name="PanImmune_MS",
        engine="openpyxl",
    )

    t.columns = t.columns.str.lower().str.replace(" ", "_")
    t.rename(
        columns={
            "tcga_participant_barcode": "bcr_patient_barcode",
            "tcga_study": "cancer_type",
            "immune_subtype": "immune_subtype",
            "tcga_subtype": "transcriptomic_subtype",
        },
        inplace=True,
    )

    # Remove survival info as collected separately in the clinical file
    t.drop(columns=["cancer_type", "os", "os_time", "pfi", "pfi_time"], inplace=True)

    # Set index
    t.set_index(["bcr_patient_barcode"], inplace=True)

    t.to_csv(os.path.join(OUTPUT_PATH_PREFIX, "immuno_scores.csv"))


def main():
    print(f"Preparing TCGA pancancer data and save in {OUTPUT_PATH_PREFIX}")

    clinical()
    mutations_silent_vs_not_silent()
    mutations_based_on_selected_variants()
    msi()
    cna()
    rna()
    rppa()
    immuno_scores()


if __name__ == "__main__":
    os.makedirs(OUTPUT_PATH_PREFIX, exist_ok=True)
    main()
