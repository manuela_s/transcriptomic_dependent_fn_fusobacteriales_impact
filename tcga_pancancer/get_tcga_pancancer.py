#!/usr/bin/env python3

""" Functions and classes to retrieve clinical and molecular data for the TCGA pancancer cohorts."""

import os

import numpy
import pandas

import py_utils.cohort

INPUT_PATH_PREFIX = os.path.join("tcga_pancancer", "processed_data", "pancancer")
INPUT_PATH_PREFIX_SIGNATURES = os.path.join(
    "tcga_pancancer", "analysis_data", "pancancer"
)
CRC_INPUT_PATH_PREFIX = os.path.join("tcga_pancancer", "analysis_data", "crc")


class TcgaPancancer:
    def get_clinical(self):
        df = combine_clinical(include_signatures=False)
        df.rename(
            columns={"pfi_months": "dfs_months", "pfi_event": "dfs_event"}, inplace=True
        )

        df["cohort"] = "TCGA-" + df.pooled_cancer_type.astype("str")
        df = df.rename_axis(index={"bcr_patient_barcode": "patient_id"}).reset_index()
        df.set_index(["cohort", "patient_id"], inplace=True)

        return df[
            [
                "stage",
                "sex",
                "age",
                "os_months",
                "os_event",
                "dss_months",
                "dss_event",
                "dfs_months",
                "dfs_event",
            ]
        ]

    def get_subtypes(self):
        df = pandas.read_csv(
            os.path.join(
                INPUT_PATH_PREFIX_SIGNATURES, "rna_based_molecular_subtypes.csv"
            ),
            index_col=[1],
        )

        # Use CMS-RF-nearest as 'cms_class', but set to 'NOLBL' labels with multiple assignments (for example, 'CMS1,CMS2')
        df["cms_class"] = pandas.Categorical(
            df["CMS-RF-nearest"], ["CMS1", "CMS2", "CMS3", "CMS4", "NOLBL"]
        ).fillna("NOLBL")
        df["cris_class"] = df.CRIS

        # Merge into dataset CMS/CRIS labels computed in original CMS/CRIS publications
        tcga = clinical()[["pooled_cancer_type", "cris_classifier", "cms_classifier"]]
        tcga["cms_classifier"] = tcga.cms_classifier.astype("O").fillna("not_computed")
        tcga["cris_classifier"] = tcga.cris_classifier.astype("O").fillna(
            "not_computed"
        )

        j = tcga.join(df[["cms_class", "cris_class"]], how="outer").rename(
            columns={
                "cms_class": "computed_cms_class",
                "cris_class": "computed_cris_class",
            }
        )

        j.query(
            'not (cms_classifier=="not_computed" and cris_classifier=="not_computed" and computed_cms_class.isna()'
            "and computed_cris_class.isna())",
            inplace=True,
        )

        # Replace our computed CMS/CRIS labels with CMS/CRIS labels from original publications when available (subset of
        # CRC patients)
        j.loc[:, "cms_class"] = j.cms_classifier.where(
            j.cms_classifier != "not_computed", j.computed_cms_class
        )
        j.loc[:, "cris_class"] = j.cris_classifier.where(
            j.cris_classifier != "not_computed", j.computed_cris_class
        )
        # A small set of patients ('TCGA-AA-3554', 'TCGA-AA-3860', 'TCGA-AG-A00H', 'TCGA-AG-A014', 'TCGA-AG-A023')
        # included CMS labels from original CMS paper, so are included here. However, those patients are not included
        # in the pancancer rna dataset, so could not compute CRIS labels (not included in CRIS paper either).

        j["pooled_cms_class"] = j.cms_class.replace(
            {
                "CMS1": "not_CMS4",
                "CMS2": "not_CMS4",
                "CMS3": "not_CMS4",
                "NOLBL": "not_CMS4",
            }
        )
        j["pooled_cris_class"] = j.cris_class.replace(
            {
                "CRIS-A": "not_CRIS-B",
                "CRIS-C": "not_CRIS-B",
                "CRIS-D": "not_CRIS-B",
                "CRIS-E": "not_CRIS-B",
                "NOLBL": "not_CRIS-B",
            }
        )
        j["pooled_cms_cris_class"] = (
            j.pooled_cms_class.astype(str) + "_" + j.pooled_cris_class.astype(str)
        )
        j["is_mesenchymal"] = j.pooled_cms_cris_class.replace(
            {
                "not_CMS4_not_CRIS-B": "no",
                "CMS4_not_CRIS-B": "yes",
                "not_CMS4_CRIS-B": "yes",
                "CMS4_CRIS-B": "yes",
                "CMS4_nan": "yes",
                "not_CMS4_nan": None,
            }
        )

        j["cohort"] = "TCGA-" + j.pooled_cancer_type.astype("str")
        j = j.rename_axis(index={"bcr_patient_barcode": "patient_id"}).reset_index()
        j.set_index(["cohort", "patient_id"], inplace=True)

        return j[["cms_class", "cris_class", "is_mesenchymal"]]

    def get_cna(self):
        cli = combine_clinical(include_signatures=False)[["pooled_cancer_type"]]

        df = (
            pandas.read_csv(
                os.path.join(INPUT_PATH_PREFIX, "cna.csv"),
                index_col=["bcr_patient_barcode"],
            )
            .query("sample_type==1")
            .drop(columns=["sample_type"])
            .dropna(axis=1)
        )

        df.drop(columns=["num_probes"], inplace=True)

        df = df.join(cli)
        df["cohort"] = "TCGA-" + df.pooled_cancer_type.astype("str")
        df.drop(columns=["pooled_cancer_type"], inplace=True)
        df = df.rename_axis(index={"bcr_patient_barcode": "patient_id"}).reset_index()
        df.set_index(["cohort", "patient_id"], inplace=True)

        return df

    def get_rna(self, standardization: bool = True):
        cli = combine_clinical(include_signatures=False)[["pooled_cancer_type"]]

        df = (
            pandas.read_csv(
                os.path.join(INPUT_PATH_PREFIX, "rna.csv"),
                index_col=["bcr_patient_barcode"],
            )
            .query("sample_type==1")
            .drop(columns=["sample_type"])
            .dropna(axis=1)
        )

        df = df.join(cli)
        df["cohort"] = "TCGA-" + df.pooled_cancer_type.astype("str")
        df.drop(columns=["pooled_cancer_type"], inplace=True)
        df = df.rename_axis(index={"bcr_patient_barcode": "patient_id"}).reset_index()
        df.set_index(["cohort", "patient_id"], inplace=True)

        if standardization:
            df = py_utils.cohort.standardization_helper(df)

        return df

    def get_rppa(self, standardization: bool = True):
        cli = combine_clinical(include_signatures=False)[["pooled_cancer_type"]]

        df = (
            pandas.read_csv(
                os.path.join(INPUT_PATH_PREFIX, "rppa.csv"),
                index_col=["bcr_patient_barcode"],
            )
            .query("sample_type==1")
            .drop(columns=["sample_type"])
            .dropna(axis=1)
        )

        df = df.join(cli)
        df["cohort"] = "TCGA-" + df.pooled_cancer_type.astype("str")
        df.drop(columns=["pooled_cancer_type"], inplace=True)
        df = df.rename_axis(index={"bcr_patient_barcode": "patient_id"}).reset_index()
        df.set_index(["cohort", "patient_id"], inplace=True)

        if standardization:
            df = py_utils.cohort.standardization_helper(df)

        return df

    def get_mutations(self):
        cli = combine_clinical(include_signatures=False)[["pooled_cancer_type"]]

        df = (
            pandas.read_csv(
                os.path.join(
                    INPUT_PATH_PREFIX, "mutations_based_on_selected_variants.csv"
                ),
                index_col=["bcr_patient_barcode"],
            )
            .query("sample_type==1")
            .drop(columns=["sample_type"])
            .dropna(axis=1)
        )

        df = df.join(cli)
        df["cohort"] = "TCGA-" + df.pooled_cancer_type.astype("str")
        df.drop(columns=["pooled_cancer_type"], inplace=True)
        df = df.rename_axis(index={"bcr_patient_barcode": "patient_id"}).reset_index()
        df.set_index(["cohort", "patient_id"], inplace=True)

        return df

    def get_immuno_composition(self):
        df = (
            pandas.read_csv(
                os.path.join(
                    INPUT_PATH_PREFIX_SIGNATURES, "mcp_counter_immune_cells.csv"
                ),
                index_col=["bcr_patient_barcode"],
            )
            .query("sample_type==1")
            .drop(columns=["sample_type"])
            .dropna(axis=1)
        )

        df = df.join(combine_clinical(include_signatures=False)[["pooled_cancer_type"]])
        df["cohort"] = "TCGA-" + df.pooled_cancer_type.astype("str")
        df.drop(columns=["pooled_cancer_type"], inplace=True)
        df = df.rename_axis(index={"bcr_patient_barcode": "patient_id"}).reset_index()
        df.set_index(["cohort", "patient_id"], inplace=True)

        return df

    def get_quantiseq_cell_types(self):
        df = (
            pandas.read_csv(
                os.path.join(INPUT_PATH_PREFIX_SIGNATURES, "quantiseq_cell_types.csv"),
                index_col=["bcr_patient_barcode"],
            )
            .query("sample_type==1")
            .drop(columns=["sample_type"])
            .dropna(axis=1)
        )

        df = df.join(combine_clinical(include_signatures=False)[["pooled_cancer_type"]])
        df["cohort"] = "TCGA-" + df.pooled_cancer_type.astype("str")
        df.drop(columns=["pooled_cancer_type"], inplace=True)
        df = df.rename_axis(index={"bcr_patient_barcode": "patient_id"}).reset_index()
        df.set_index(["cohort", "patient_id"], inplace=True)

        return df

    def get_rna_based_signatures(self):
        df = (
            pandas.read_csv(
                os.path.join(
                    INPUT_PATH_PREFIX_SIGNATURES, "computed_rna_based_signatures.csv"
                ),
                index_col=["bcr_patient_barcode"],
            )
            .query("sample_type==1")
            .drop(columns=["sample_type"])
            .dropna(axis=1)
        )

        df = df.join(combine_clinical(include_signatures=False)[["pooled_cancer_type"]])
        df["cohort"] = "TCGA-" + df.pooled_cancer_type.astype("str")
        df.drop(columns=["pooled_cancer_type"], inplace=True)
        df = df.rename_axis(index={"bcr_patient_barcode": "patient_id"}).reset_index()
        df.set_index(["cohort", "patient_id"], inplace=True)

        return df

    def get_microsatellite_and_mutational_status(self):
        msi = (
            pandas.read_csv(
                os.path.join(INPUT_PATH_PREFIX, "msi.csv"),
                index_col=["bcr_patient_barcode"],
            )
            .query("sample_type==1")
            .drop(columns=["sample_type"])
        )
        msi["microsatellite_status"] = pandas.Categorical(
            msi.msi_status.replace({"MSI-H": "MSI"}), ["MSS", "MSI"]
        )

        mutations = (
            pandas.read_csv(
                os.path.join(
                    INPUT_PATH_PREFIX, "mutations_based_on_selected_variants.csv"
                ),
                index_col=["bcr_patient_barcode"],
            )
            .query("sample_type==1")
            .drop(columns=["sample_type"])
            .dropna(axis=1)
        )
        mutations = mutations[["KRAS", "NRAS", "HRAS", "MRAS"]]
        mutations.columns = mutations.columns.str.lower() + "_status"
        mutations = (
            mutations.astype(int)
            .ge(1)
            .apply(
                lambda x: pandas.Categorical(
                    x.map({True: "mutant", False: "wild_type"}), ["wild_type", "mutant"]
                )
            )
        )

        df = msi[["microsatellite_status"]].join(mutations, how="outer")

        df = df.join(combine_clinical(include_signatures=False)[["pooled_cancer_type"]])
        df["cohort"] = "TCGA-" + df.pooled_cancer_type.astype("str")
        df.drop(columns=["pooled_cancer_type"], inplace=True)
        df = df.rename_axis(index={"bcr_patient_barcode": "patient_id"}).reset_index()
        df.set_index(["cohort", "patient_id"], inplace=True)

        return df


def combine_clinical(include_signatures=True):
    cli = pandas.read_csv(
        os.path.join(INPUT_PATH_PREFIX, "clinical.csv"),
        index_col=["bcr_patient_barcode"],
    )

    # Cancer types
    cli["cancer_solid_or_heme_lymphatic"] = pandas.Categorical(
        numpy.where(
            cli.cancer_type.isin(["LAML", "DLBC", "THYM"]), "heme_or_lymphatic", "solid"
        ),
        ["heme_or_lymphatic", "solid"],
    )
    # Classification based on https://www.ncbi.nlm.nih.gov/pubmed/29625048 and https://www.ncbi.nlm.nih.gov/pubmed/29625050
    cli["cancer_pan_type"] = cli.cancer_type.replace(
        {
            "ACC": "endocrine",
            "BLCA": "genitourinary",
            "BRCA": "gynecologic",
            "CESC": "gynecologic",
            "CHOL": "developmental_gastrointestinal",
            "COAD": "core_gastrointestinal",
            "DLBC": "heme_lymphatic",
            "ESCA": "core_gastrointestinal",
            "GBM": "cns",
            "HNSC": "head_and_neck",
            "KICH": "genitourinary",
            "KIRC": "genitourinary",
            "KIRP": "genitourinary",
            "LAML": "heme_lymphatic",
            "LGG": "cns",
            "LIHC": "developmental_gastrointestinal",
            "LUAD": "thoracic",
            "LUSC": "thoracic",
            "MESO": "thoracic",
            "OV": "gynecologic",
            "PAAD": "developmental_gastrointestinal",
            "PCPG": "endocrine",  # Pheochromocytoma and Paraganglioma
            "PRAD": "genitourinary",
            "READ": "core_gastrointestinal",
            "SARC": "soft_tissue",
            "SKCM": "melanoma",
            "STAD": "core_gastrointestinal",
            "TGCT": "genitourinary",
            "THCA": "endocrine",
            "THYM": "heme_lymphatic",
            "UCEC": "gynecologic",
            "UCS": "gynecologic",
            "UVM": "melanoma",
        }
    )
    cli["cancer_pan_type"] = pandas.Categorical(
        cli.cancer_pan_type,
        [
            "core_gastrointestinal",
            "developmental_gastrointestinal",
            "thoracic",
            "genitourinary",
            "gynecologic",
            "cns",
            "head_and_neck",
            "endocrine",
            "melanoma",
            "heme_lymphatic",
            "soft_tissue",
        ],
    )
    cli["pooled_cancer_type"] = pandas.Categorical(
        cli.cancer_type.replace({"COAD": "COAD-READ", "READ": "COAD-READ"})
    )

    cli["age"] = cli.age.astype("Int64")
    cli["sex"] = pandas.Categorical(cli.sex, ["male", "female"])
    cli["race"] = pandas.Categorical(
        cli.race,
        [
            "white",
            "black_or_african_american",
            "asian",
            "american_indian_or_alaska_native",
            "native_hawaiian_or_other_pacific_islander",
        ],
    )

    cli["stage"] = pandas.Categorical(
        cli.stage.replace({"is": "0", "1_2": "2"}).astype("float").astype("Int64"),
        [0, 1, 2, 3, 4],
    )
    cli["residual_tumor"] = pandas.Categorical(cli.residual_tumor, ["R0", "R1", "R2"])
    cli["menopause_status"] = pandas.Categorical(
        cli.menopause_status, ["pre", "peri", "post"]
    )
    cli["histological_grade"] = pandas.Categorical(
        cli.histological_grade, ["G1", "G2", "G3", "G4"]
    )
    cli["initial_pathologic_dx_year"] = pandas.Categorical(
        cli.initial_pathologic_dx_year.astype("Int64")
    )

    if include_signatures:
        msi = (
            pandas.read_csv(
                os.path.join(INPUT_PATH_PREFIX, "msi.csv"),
                index_col=["bcr_patient_barcode"],
            )
            .query("sample_type==1")
            .drop(columns=["sample_type"])
        )
        mutations = (
            pandas.read_csv(
                os.path.join(INPUT_PATH_PREFIX, "mutations_silent_vs_non_silent.csv"),
                index_col=["bcr_patient_barcode"],
            )
            .query("sample_type==1")
            .drop(columns=["sample_type"])
        )

        cms = pandas.read_csv(
            os.path.join(CRC_INPUT_PATH_PREFIX, "cms_subtyping.csv"),
            index_col=["bcr_patient_barcode"],
        )
        cris = pandas.read_csv(
            os.path.join(CRC_INPUT_PATH_PREFIX, "cris_subtyping.csv"),
            index_col=["bcr_patient_barcode"],
        )

        computed_signatures = (
            pandas.read_csv(
                os.path.join(
                    INPUT_PATH_PREFIX_SIGNATURES, "computed_rna_based_signatures.csv"
                ),
                index_col=["bcr_patient_barcode"],
            )
            .query("sample_type==1")
            .drop(columns=["sample_type"])
        )

        immuno_scores = pandas.read_csv(
            os.path.join(INPUT_PATH_PREFIX, "immuno_scores.csv"),
            index_col=["bcr_patient_barcode"],
        )[
            [
                "immune_subtype",
                "leukocyte_fraction",
                "stromal_fraction",
                "intratumor_heterogeneity",
                "til_regional_fraction",
            ]
        ]

        j = (
            cli.join(msi)
            .join(mutations)
            .join(cms)
            .join(cris)
            .join(computed_signatures)
            .join(immuno_scores)
        )
    else:
        j = cli

    return j


def clinical(include_signatures=True):
    t = combine_clinical(include_signatures)
    t["cancer_type"] = pandas.Categorical(t.cancer_type)

    if include_signatures:
        t["msi_status"] = pandas.Categorical(
            t.msi_status.replace({"MSI-H": "MSI"}), ["MSI", "MSS"]
        )
        t["cms_classifier"] = pandas.Categorical(
            t.cms_classifier, ["CMS1", "CMS2", "CMS3", "CMS4", "NOLBL"]
        )
        t["cris_classifier"] = pandas.Categorical(
            t.cris_classifier, ["CRIS-A", "CRIS-B", "CRIS-C", "CRIS-D", "CRIS-E"]
        )
        t["immune_subtype"] = pandas.Categorical(t.immune_subtype)

    return t
