#!/usr/bin/env python3

""" Load redaction information from clinical file. """

import pathlib
import pandas


def get_redaction_info() -> pandas.Series:
    """Load redaction information from clinical file.
    :return: series, one row per patient, indexed by 'patient_id' with redaction information as values."""
    t = pandas.read_excel(
        pathlib.Path(
            "tcga_pancancer",
            "download",
            "pancancer",
            "TCGA-CDR-SupplementalTableS1.xlsx",
        ),
        index_col=1,
    ).rename_axis(index={"bcr_patient_barcode": "patient_id"})[["Redaction"]]
    t.columns = t.columns.str.lower()
    t["redaction"] = t.redaction.str.lower().fillna("not_redacted")

    return t.redaction
