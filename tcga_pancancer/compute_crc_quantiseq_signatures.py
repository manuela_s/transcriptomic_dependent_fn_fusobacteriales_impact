#!/usr/bin/env python3

""" Compute quanTIseq signatures."""

import pathlib
import pandas

import transcriptomics.signatures

PATH_PREFIX = pathlib.Path("tcga_pancancer", "analysis_data", "crc")


def quantiseq() -> None:
    """
    Compute quanTIseq cell types composition (Finotello et al., Genome Med., 2019, PMID: 31126321).
    """
    rna = pandas.read_csv(
        pathlib.Path(PATH_PREFIX, "rna.csv"),
        index_col=["bcr_patient_barcode", "sample_type"],
    )

    quantiseq = transcriptomics.signatures.run_quantiseq_classifier(rna.dropna(axis=1))
    quantiseq.to_csv(pathlib.Path(PATH_PREFIX, "quantiseq_cell_types.csv"))


if __name__ == "__main__":
    pathlib.Path(PATH_PREFIX).mkdir(parents=True, exist_ok=True)
    quantiseq()
