#!/usr/bin/env python3

""" Prepare transcriptomics-based signature for the TCGA pancancer cohorts."""

import os

import pandas
import sklearn.preprocessing

import transcriptomics.signatures

INPUT_PATH_PREFIX = os.path.join("tcga_pancancer", "processed_data", "pancancer")
OUTPUT_PATH_PREFIX = os.path.join("tcga_pancancer", "analysis_data", "pancancer")


def rna_signatures() -> None:
    rna = pandas.read_csv(
        os.path.join(INPUT_PATH_PREFIX, "rna.csv"),
        index_col=["bcr_patient_barcode", "sample_type"],
    )
    rna_transformed = pandas.DataFrame(
        sklearn.preprocessing.QuantileTransformer(
            output_distribution="normal", random_state=1
        ).fit_transform(rna),
        index=rna.index,
        columns=rna.columns,
    )
    rna_log2_zscore = pandas.DataFrame(
        sklearn.preprocessing.RobustScaler().fit_transform(rna_transformed),
        index=rna_transformed.index,
        columns=rna_transformed.columns,
    )

    signatures = pandas.concat(
        {
            "rna_emt_score": transcriptomics.signatures.run_emt_classifier(
                rna_log2_zscore
            ),
            "rna_tis_score": transcriptomics.signatures.run_tis_score(rna_log2_zscore),
            "rna_ifng_score": transcriptomics.signatures.run_ifng_score(
                rna_log2_zscore
            ),
            "rna_cytolytic_score": transcriptomics.signatures.run_cytolytic_score(
                rna_log2_zscore
            ),
            "rna_hippo_score": transcriptomics.signatures.run_hippo_classifier(
                rna_log2_zscore
            ),
            "rna_hypoxia_score": transcriptomics.signatures.run_hypoxia_classifier(
                rna_log2_zscore
            ),
            "rna_proliferation_score": transcriptomics.signatures.run_proliferation_classifier(
                rna_log2_zscore
            ),
            "rna_dna_damage_score": transcriptomics.signatures.run_dna_damage_classifier(
                rna_log2_zscore
            ),
            "rna_wnt_score": transcriptomics.signatures.run_wnt_classifier(
                rna_log2_zscore
            ),
            "rna_metastasis_score": transcriptomics.signatures.run_metastasis_classifier(
                rna_log2_zscore
            ),
            "rna_stemness_score": transcriptomics.signatures.run_stemness_classifier(
                rna_log2_zscore
            ),
        },
        axis=1,
    )

    signatures.to_csv(
        os.path.join(OUTPUT_PATH_PREFIX, "computed_rna_based_signatures.csv")
    )


if __name__ == "__main__":
    os.makedirs(OUTPUT_PATH_PREFIX, exist_ok=True)

    print(
        f"Preparing transcriptomics-based signature score and save in {OUTPUT_PATH_PREFIX}"
    )

    rna_signatures()
