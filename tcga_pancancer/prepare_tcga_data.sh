#!/usr/bin/env bash

set -e

# Download and prepare TCGA data
python3 tcga_pancancer/download_pancancer_data.py
gunzip tcga_pancancer/download/pancancer/mc3.v0.2.8.PUBLIC.maf.gz
python3 tcga_pancancer/download_tcga_crc_specific_data.py

PYTHONPATH=. python3 tcga_pancancer/prepare_pancancer_util.py
#PYTHONPATH=. python3 tcga_pancancer/fetch_gene_mapping.py
PYTHONPATH=. python3 tcga_pancancer/prepare_pancancer_signatures.py
PYTHONPATH=. python3 tcga_pancancer/compute_pancancer_mcp_signatures.py
PYTHONPATH=. python3 tcga_pancancer/prepare_crc_specific_util.py
PYTHONPATH=. python3 tcga_pancancer/compute_crc_quantiseq_signatures.py
PYTHONPATH=. python3 tcga_pancancer/compute_crc_based_subtyping_signatures.py
