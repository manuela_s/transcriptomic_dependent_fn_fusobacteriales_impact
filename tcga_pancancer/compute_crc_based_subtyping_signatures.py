#!/usr/bin/env python3

""" Compute CMS and CRIS subtyping signatures."""

import os

import pandas
import sklearn.metrics

import tcga_pancancer.get_tcga_crc
import transcriptomics.get_samples_metadata
import transcriptomics.map_ids_from_ensembl
import transcriptomics.signatures

INPUT_CRC_PATH_PREFIX = os.path.join("tcga_pancancer", "download", "crc")
INPUT_PANCANCER_PATH_PREFIX = os.path.join(
    "tcga_pancancer", "processed_data", "pancancer"
)
OUTPUT_PANCANCER_PATH_PREFIX = os.path.join(
    "tcga_pancancer", "analysis_data", "pancancer"
)


def cms_helper(rna: pandas.DataFrame, method: str, class_type: str) -> pandas.Series:
    """
    Prepare input data and run CMS classifier.
    :param rna: dataframe with gene expression. One row per patient and one column per gene;
    :param method: string, either 'RF' (Random Forest) or 'SSP' (Single Sample Predictor);
    :param class_type: string, either 'prediction' or 'nearest';
    :return: series with CMS assignments.
    """
    mapping_fn = os.path.join(INPUT_PANCANCER_PATH_PREFIX, "rna_mapping.csv")
    transformed_rna = pandas.DataFrame(
        sklearn.preprocessing.StandardScaler().fit_transform(rna),
        index=rna.index,
        columns=rna.columns,
    )
    cms = transcriptomics.signatures.remap_genes_and_run_cms_classifier(
        mapping_fn, transformed_rna.dropna(axis=1), method=method, class_type=class_type
    )

    return cms


def run_cms_cris_classifiers(cli: pandas.DataFrame, rna: pandas.DataFrame) -> None:
    """Run CMS (Guinney et al., Nat Med., 2015, PMID: 26457759) and CRIS (Isella et al., Nat. Commun., 2017, PMID:
    28561063) classifiers.
    :param cli: dataframe with clinical data, as returned by `tcga_pancancer.get_tcga_pancancer.clinical()`;
    :param rna: dataframe with gene expression, as returned by `tcga_pancancer.get_tcga_pancancer.rna()`.
    """
    subtypes = pandas.concat(
        {
            "CMS-RF": cli.groupby("pooled_cancer_type").apply(
                lambda x: cms_helper(
                    rna.query("bcr_patient_barcode.isin(@x.index)"),
                    method="RF",
                    class_type="prediction",
                )
            ),
            "CMS-RF-nearest": cli.groupby("pooled_cancer_type").apply(
                lambda x: cms_helper(
                    rna.query("bcr_patient_barcode.isin(@x.index)"),
                    method="RF",
                    class_type="nearest",
                )
            ),
            "CMS-SSP": cli.groupby("pooled_cancer_type").apply(
                lambda x: cms_helper(
                    rna.query("bcr_patient_barcode.isin(@x.index)"),
                    method="SSP",
                    class_type="prediction",
                )
            ),
            "CMS-SSP-nearest": cli.groupby("pooled_cancer_type").apply(
                lambda x: cms_helper(
                    rna.query("bcr_patient_barcode.isin(@x.index)"),
                    method="SSP",
                    class_type="nearest",
                )
            ),
            "CRIS": cli.groupby("pooled_cancer_type").apply(
                lambda x: transcriptomics.signatures.run_cris_classifier(
                    rna.query("bcr_patient_barcode.isin(@x.index)").dropna(axis=1)
                )
            ),
        },
        names="subtype",
        axis=1,
    ).fillna("NOLBL")

    subtypes.to_csv(
        os.path.join(OUTPUT_PANCANCER_PATH_PREFIX, "rna_based_molecular_subtypes.csv")
    )


def main():
    cli = tcga_pancancer.get_tcga_pancancer.clinical()[
        [
            "cancer_solid_or_heme_lymphatic",
            "cancer_pan_type",
            "pooled_cancer_type",
            "cancer_type",
        ]
    ]
    cli.query('cancer_solid_or_heme_lymphatic=="solid"', inplace=True)
    cli.pooled_cancer_type.cat.remove_unused_categories(inplace=True)

    rna = (
        pandas.read_csv(
            os.path.join(INPUT_PANCANCER_PATH_PREFIX, "rna.csv"),
            index_col=["bcr_patient_barcode"],
        )
        .query("sample_type==1")
        .drop(columns=["sample_type"])
        .dropna(axis=1)
    )
    rna.query("index.isin(@cli.index)", inplace=True)

    run_cms_cris_classifiers(cli, rna)


if __name__ == "__main__":
    main()
