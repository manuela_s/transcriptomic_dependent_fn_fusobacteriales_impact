#!/usr/bin/env python3

""" Load mapping between protein names and corresponding gene name. """

import os

import pandas


def map_protein_id_to_gene_id() -> pandas.Series:
    """Load mapping between protein names and corresponding gene name.
    :return: series, one row per protein, indexed by 'protein' and corresponding gene name as values."""
    data = pandas.read_csv(
        os.path.join("tcga_pancancer", "data", "rppa_to_gene_mapping.csv"),
        index_col=[0],
    )
    return (
        data.query("index.notna()")
        .gene.str.split(expand=True)
        .stack()
        .reset_index(1, drop=True)
        .rename("gene")
    )
