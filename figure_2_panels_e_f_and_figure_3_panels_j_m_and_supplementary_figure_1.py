#!/usr/bin/env python3

""" Generate figure 2 panels e-f, figure 3 panels j and m and supplementary figure 1.
Comparison of features expression (signatures/gene/protein) in patients with low vs. high either Fusobacterium nucleatum
load (Taxonomy cohort) or Fusobacteriales relative abundance (TCGA-COAD-READ cohort)."""

import pathlib

import matplotlib.pyplot
import pandas

import cohort
import fusobacteria_plot_utils
import fusobacteria_utils

DST_DIR = pathlib.Path("figures_and_tables")


def figures_by_feature_groups(
    fusobacteria: pandas.DataFrame,
    cohort_instance,
    cohort_name: str,
    dst_dir: pathlib.Path,
) -> None:
    """
    Helper function to generate figures by feature groups per cohort.
    :param fusobacteria: dataframe, one row per patient, indices: 'technology', 'cohort', 'patient_id' and must include
                         column 'fusobacteria_binary_class';
    :param cohort_instance: py_utils.cohort.Cohort-like instance;
    :cohort_name: cohort label;
    :dst_dir: path to save figure to.
    """
    crc_data_w = (
        fusobacteria[["fusobacteria_binary_class"]]
        .join(cohort_instance.get_rna(), how="inner")
        .join(cohort_instance.get_rna_based_signatures())
        .join(cohort_instance.get_immuno_composition())
    )

    if cohort_instance.get_rppa() is not None:
        crc_data_w = crc_data_w.join(cohort_instance.get_rppa())

    crc_data_w.set_index("fusobacteria_binary_class", append=True, inplace=True)
    expression = crc_data_w.rename_axis(columns="feature").stack().rename("expression")

    fusobacteria_plot_utils.gene_group_vs_fusobacteria_figure(
        expression,
        cohort_name,
        "figure_2_panels_e_f",
        [
            "NFKB1",
            "TNF",
            "rna_tis_score",
            "rna_ifng_score",
            "rna_cytolytic_score",
            "IL6",
            "IL8",
            "IL10",
            "IL1B",
            "CCL8",
            "CSF1",
            "ICAM1",
            "MMP1",
            "MMP3",
            "MMP9",
            "NOS2",
            "NLRP3",
        ],
        DST_DIR,
    )

    fusobacteria_plot_utils.gene_group_vs_fusobacteria_figure(
        expression,
        cohort_name,
        "figure_3_panel_j",
        [
            "rna_proliferation_score",
            "rna_wnt_score",
            "rna_metastasis_score",
            "rna_dna_damage_score",
        ],
        dst_dir,
    )

    fusobacteria_plot_utils.gene_group_vs_fusobacteria_figure(
        expression,
        cohort_name,
        "supplementary_figure_1",
        ["COX2", "CSF1R", "CSF2", "CSF2RA", "CSF3", "IL6R"],
        dst_dir,
    )

    if cohort_instance.get_rppa() is not None:
        fusobacteria_plot_utils.gene_group_vs_fusobacteria_figure(
            expression,
            cohort_name,
            "figure_3_panel_m",
            ["atm", "cmyc", "dvl3"],
            dst_dir,
        )


def main():
    fusobacteria = fusobacteria_utils.get_crc_fusobacteria()
    figures_by_feature_groups(
        fusobacteria, cohort.get_qpcr_in_house_cohorts(), "Taxonomy", DST_DIR
    )
    figures_by_feature_groups(
        fusobacteria, cohort.get_tcga_crc_cohort(), "TCGA-COAD-READ", DST_DIR
    )


if __name__ == "__main__":
    matplotlib.rcParams["pdf.fonttype"] = 42
    matplotlib.rcParams["font.size"] = 6
    matplotlib.rcParams["font.family"] = "Arial"
    DST_DIR.mkdir(parents=True, exist_ok=True)

    main()
