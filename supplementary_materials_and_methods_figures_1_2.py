#!/usr/bin/env python3

""" Generate figures 1-2 for Supplementary Materials and Methods.
Patients inclusion criteria and data availability for the TCGA-COAD-READ cohort."""

import pathlib
import string

import graphviz
import matplotlib.pyplot
import upsetplot

import cohort
import fusobacteria_utils
import pathseq_utils
import transcriptomics.get_samples_metadata

FUSOBACTERIA_DST_DIR = pathlib.Path("figures_and_tables")


def supplementary_materials_and_methods_figure_1() -> None:
    """
    Generate flow chart displaying number of samples/patients with available fusobacteriales estimates at different
    steps of the analysis.
    """
    samples_in_mapping = transcriptomics.get_samples_metadata.get_samples_metadata()
    samples_in_metrics = pathseq_utils.get_filter_metrics()

    samples_with_pathseq = samples_in_mapping.join(samples_in_metrics, how="inner")
    samples_with_pathseq_in_tcga_crc = samples_with_pathseq.query(
        'source=="tcga" and organ.isin(["colon", "rectum"])'
    )
    samples_with_pathseq_in_tcga_crc_tumour = samples_with_pathseq_in_tcga_crc.query(
        'tissue=="tumour"'
    )
    samples_with_pathseq_in_tcga_crc_tumour_good_reads = samples_with_pathseq_in_tcga_crc_tumour.query(
        "primary_reads>10e6"
    )

    patients_with_pathseq_in_tcga_crc_tumour_good_reads = samples_with_pathseq_in_tcga_crc_tumour_good_reads.groupby(
        "patient_id"
    ).size()

    patients_to_include = patients_with_pathseq_in_tcga_crc_tumour_good_reads.to_frame().join(
        cohort.get_tcga_crc_cohort().get_clinical(), how="inner"
    )

    dot = graphviz.Source(
        string.Template(
            """
    digraph G {
        graph [ranksep=0];
        
        node [fixedsize=true, height=0.5, width=1, nojustify=true, fontname="arial", fontsize=6]
        
        in_tcga_crc [shape=tripleoctagon, fillcolor="#4292c6", style=filled, label="TCGA-COAD-READ\nN samples\nwith RNASeq=${n_in_tcga_crc}"]
        is_tumour [shape=diamond, height=1, fillcolor="#ffff99", style=filled, label="Is tumour?"]
        in_tcga_crc_non_tumour [shape=box, fillcolor=gainsboro, style=filled, label="no\nN samples=${n_in_tcga_crc_non_tumour}"]
        in_tcga_crc_tumour [shape=box, fillcolor="#6baed6", style=filled, label="yes\nN samples=${n_in_tcga_crc_tumour}"]
        has_good_reads [shape=diamond, height=1, fillcolor="#ffff99", style=filled, label="Number of\nprimary reads\n > 10^6?"]
        in_tcga_crc_tumour_bad_reads [shape=box, fillcolor=gainsboro, style=filled, label="no\nN samples=${n_in_tcga_crc_tumour_bad_reads}"]
        in_tcga_crc_tumour_good_reads [shape=box, fillcolor="#9ecae1", style=filled, label="yes\nN samples=${n_in_tcga_crc_tumour_good_reads}"]
        aggregate_by_patient [shape=invtriangle, height=1, fillcolor="#ffff99", style=filled, label="Aggregate multiple\nsamples per\npatient"]
        patients_in_tcga_crc_tumour_good_reads [shape=box, fillcolor="#c6dbef", style=filled, label="n patients=${n_patients_in_tcga_crc_tumour_good_reads}"]
        is_redacted [shape=diamond, height=1, fillcolor="#ffff99", style=filled, label="Is patient\nredacted\nfrom clinical?"]
        redacted_patients_in_tcga_crc_tumour_good_reads [shape=box, fillcolor="gainsboro", style=filled, label="yes\nN patients=${n_redacted_patients_in_tcga_crc_tumour_good_reads}"]
        non_redacted_patients_in_tcga_crc_tumour_good_reads [shape=doubleoctagon, fillcolor="#deebf7", style=filled, label="no\nN patients=${n_non_redacted_patients_in_tcga_crc_tumour_good_reads}"]

        
        in_tcga_crc -> is_tumour
        is_tumour -> in_tcga_crc_non_tumour;
        is_tumour -> in_tcga_crc_tumour
        in_tcga_crc_tumour -> has_good_reads
        has_good_reads -> in_tcga_crc_tumour_bad_reads
        has_good_reads -> in_tcga_crc_tumour_good_reads
        in_tcga_crc_tumour_good_reads -> aggregate_by_patient
        aggregate_by_patient -> patients_in_tcga_crc_tumour_good_reads
        patients_in_tcga_crc_tumour_good_reads -> is_redacted
        is_redacted -> redacted_patients_in_tcga_crc_tumour_good_reads
        is_redacted -> non_redacted_patients_in_tcga_crc_tumour_good_reads
    }
    """
        ).safe_substitute(
            n_in_tcga_crc=samples_with_pathseq_in_tcga_crc.shape[0],
            n_in_tcga_crc_non_tumour=samples_with_pathseq_in_tcga_crc.shape[0]
            - samples_with_pathseq_in_tcga_crc_tumour.shape[0],
            n_in_tcga_crc_tumour=samples_with_pathseq_in_tcga_crc_tumour.shape[0],
            n_in_tcga_crc_tumour_bad_reads=samples_with_pathseq_in_tcga_crc_tumour.shape[
                0
            ]
            - samples_with_pathseq_in_tcga_crc_tumour_good_reads.shape[0],
            n_in_tcga_crc_tumour_good_reads=samples_with_pathseq_in_tcga_crc_tumour_good_reads.shape[
                0
            ],
            n_patients_in_tcga_crc_tumour_good_reads=patients_with_pathseq_in_tcga_crc_tumour_good_reads.shape[
                0
            ],
            n_redacted_patients_in_tcga_crc_tumour_good_reads=patients_with_pathseq_in_tcga_crc_tumour_good_reads.shape[
                0
            ]
            - patients_to_include.shape[0],
            n_non_redacted_patients_in_tcga_crc_tumour_good_reads=patients_to_include.shape[
                0
            ],
        ),
        filename="supplementary_materials_and_methods_figure_1",
        directory=FUSOBACTERIA_DST_DIR,
    )
    dot.render()


def supplementary_materials_and_methods_figure_2() -> None:
    """
    Generate upset plot displaying data availability for multiple data types in the TCGA-COAD-READ cohort.
    """
    tcga_crc = cohort.get_tcga_crc_cohort()
    fusobacteriales = (
        fusobacteria_utils.get_crc_fusobacteria()
        .loc["rna"]
        .query('cohort=="TCGA-COAD-READ"')
        .index.get_level_values("patient_id")
    )
    clinical = tcga_crc.get_clinical()
    rna = tcga_crc.get_rna()[["CASP3"]]
    subtypes = tcga_crc.get_subtypes()
    in_mutations = tcga_crc.get_mutations().index.get_level_values("patient_id")
    in_cna = tcga_crc.get_cna().index.get_level_values("patient_id").unique()
    in_rppa = tcga_crc.get_rppa().index.get_level_values("patient_id")
    in_microsatellite = (
        tcga_crc.get_microsatellite_and_mutational_status()
        .microsatellite_status.dropna()
        .index.get_level_values("patient_id")
    )
    in_cell_types = tcga_crc.get_quantiseq_cell_types().index.get_level_values(
        "patient_id"
    )

    upset_data = upsetplot.from_contents(
        {
            "Mesenchymal status": subtypes.is_mesenchymal.dropna().index.get_level_values(
                "patient_id"
            ),
            "Colorectal intrinsic subtyping\n(CRIS)": subtypes.cris_class.dropna().index.get_level_values(
                "patient_id"
            ),
            "Consensus molecular subtyping\n(CMS)": subtypes.cms_class.dropna().index.get_level_values(
                "patient_id"
            ),
            "Cell type composition": in_cell_types,
            "Protein expression": in_rppa,
            "Gene expression": rna.index.get_level_values("patient_id"),
            "Copy number aberrations": in_cna,
            "Mutational status": in_mutations,
            "Microsatellite status": in_microsatellite,
            "Disease free survival\n(DFS)": clinical[["dfs_months", "dfs_event"]]
            .dropna()
            .index.get_level_values("patient_id"),
            "Disease specific survival\n(DSS)": clinical[["dss_months", "dss_event"]]
            .dropna()
            .index.get_level_values("patient_id"),
            "Overall survival\n(OS)": clinical[["os_months", "os_event"]]
            .dropna()
            .index.get_level_values("patient_id"),
            "Clinical": clinical.index.get_level_values("patient_id"),
            "$Fusobacteriales$\n(& higher resolution taxonomic ranks)": fusobacteriales,
        }
    )
    upsetplot.UpSet(
        upset_data,
        show_counts=True,
        show_percentages=True,
        sort_by="cardinality",
        sort_categories_by=None,
        min_subset_size=5,
    ).plot()
    fig = matplotlib.pyplot.gcf()

    label_to_color = {
        "Mesenchymal status": "#ff7f00",
        "Colorectal intrinsic subtyping\n(CRIS)": "#ff7f00",
        "Consensus molecular subtyping\n(CMS)": "#ff7f00",
        "Cell type composition": "#ff7f00",
        "Protein expression": "#33a02c",
        "Gene expression": "#33a02c",
        "Copy number aberrations": "#33a02c",
        "Mutational status": "#33a02c",
        "Microsatellite status": "#33a02c",
        "Disease free survival\n(DFS)": "#1f78b4",
        "Disease specific survival\n(DSS)": "#1f78b4",
        "Overall survival\n(OS)": "#1f78b4",
        "Clinical": "#1f78b4",
        "$Fusobacteriales$\n(& higher resolution taxonomic ranks)": "#e31a1c",
    }
    for label in fig.axes[1].get_yticklabels():
        label.set_color(label_to_color[label.get_text()])

    fig.axes[1].set_ylabel("Availability of clinical and molecular datasets")
    fig.axes[2].set_xlabel("Number of patients")
    fig.axes[3].set_ylabel("Intersection size\n[Number of patients (%)]")

    fig.set_figwidth(7)

    fig.savefig(
        FUSOBACTERIA_DST_DIR / "supplementary_materials_and_methods_figure_2.pdf"
    )


if __name__ == "__main__":
    matplotlib.rcParams["figure.dpi"] = 141
    matplotlib.rcParams["pdf.fonttype"] = 42
    matplotlib.rcParams["font.size"] = 6
    matplotlib.rcParams["font.family"] = "Arial"
    pathlib.Path(FUSOBACTERIA_DST_DIR).mkdir(parents=True, exist_ok=True)

    supplementary_materials_and_methods_figure_1()
    supplementary_materials_and_methods_figure_2()
