#!/usr/bin/env python3

import pandas

import py_utils.rpy_helper


def r_matrix_to_df(r_matrix):
    return py_utils.rpy_helper.r_matrix_to_df(r_matrix)


def remap_rna_ids(data, mapping_fn, output_id, target_output_ids=None):
    """Rename column-names in data from one type of rna identifier to another (output_id).
    Drop columns not mapping to any output_id and keeps data column with highest variance if multiple genes map to
    the same output_id.
    :param data: pandas DataFrame with gene IDs as column names and samples as rows;
    :param mapping_fn: filename for csv file that maps data column names to other RNA ids (see map_ids_from_ensembl.py);
    :param output_id: string, output_id type, either 'ensembl_id', 'symbol' or 'entrezgene';
    :param target_output_ids: optional sequence of strings, if given, map data only to output_ids in target_output_ids;
    :return: pandas DataFrame, with same index as 'data' and columns remapped to 'output_id'.
    """
    m = pandas.read_csv(mapping_fn, dtype={'entrezgene': str, 'query': str}). \
        set_index(['query']). \
        sort_values(by='_score')

    # Subset to mappings to rna ids present in the data
    m = m.query('query.isin(@data.columns)')

    if 'notfound' in m.columns:
        m = m.drop(index=m.index[m.notfound == True])

    # Drop entries where query did not match to any output id
    m = m.dropna(subset=[output_id])

    if target_output_ids is not None:
        m = m.query('{}.isin(@target_output_ids)'.format(output_id))

    if target_output_ids is None:
        # Keep highest score when a single query maps to multiple output_id.
        # If target_output_ids is given, keep the duplicates to allow a single input column to be included twice for
        # two different output_ids in the result.
        m = m[~m.index.duplicated(keep='last')]

    # If multiple gene ids map to the same output_id, keep the gene id corresponding to the highest score
    m = m.drop_duplicates(subset=[output_id], keep='last')

    # Drop columns that are missing output_id mapping
    data = data[m.index]

    # Rename columns to be named by output_id
    data.columns = m[output_id]
    data.columns.name = output_id

    return data
