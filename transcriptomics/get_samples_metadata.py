#!/usr/bin/env python3

import os

import pandas


def get_samples_metadata() -> pandas.DataFrame:
    """
    Get metadata for samples included in the TCGA pancancer cohorts.
    :return: dataframe, 1 row per sample with index 'sample_id' and columns:
            - 'batch';
            - 'organ';
            - 'cohort';
            - 'sample_type';
            - 'source';
            - 'patient_id';
            - 'tissue';
            - 'distance';
            - 'technology'.
    """
    t = pandas.read_csv(
        os.path.join("transcriptomics", "dbgap_datasets", "tcga_samples.txt"), sep="\t"
    )
    t.rename(
        columns={
            "experimental_strategy": "technology",
            "analysis.metadata.read_groups.0.sequencing_center": "batch",
            "associated_entities.0.entity_submitter_id": "sample_id",
            "cases.0.submitter_id": "patient_id",
            "cases.0.project.project_id": "cohort",
        },
        inplace=True,
    )
    t["technology"] = t.technology.replace({"RNA-Seq": "rna"})
    t.loc[:, "sample_type"] = "patient"
    t.loc[:, "organ"] = t.cohort.replace(
        {
            "TCGA-COAD": "colon",
            "TCGA-READ": "rectum",
            "TCGA-STAD": "stomach",
            "TCGA-ESCA": "esophagus",
            "TCGA-LIHC": "liver",
            "TCGA-CHOL": "gall bladder",
            "TCGA-LGG": "brain",
            "TCGA-GBM": "brain",
            "TCGA-LUAD": "lung",
            "TCGA-LUSC": "lung",
            "TCGA-MESO": "lung",
            "TCGA-PAAD": "pancreas",
            "TCGA-BLCA": "urinary bladder",
            "TCGA-PRAD": "prostate gland",
            "TCGA-OV": "ovary",
            "TCGA-CESC": "uterine cervix",
            "TCGA-UCEC": "endometrium",
            "TCGA-HNSC": "oral cavity",
            "TCGA-TGCT": "testis",
            "TCGA-UCS": "uterus",
            "TCGA-BRCA": "breast",
            "TCGA-KICH": "kidney",
            "TCGA-KIRC": "kidney",
            "TCGA-KIRP": "kidney",
            "TCGA-ACC": "adrenal gland",
            "TCGA-DLBC": "lymph node",
            "TCGA-LAML": "bone marrow",
            "TCGA-THCA": "thyroid gland",
            "TCGA-THYM": "oral cavity",
            "TCGA-PCPG": "adrenal gland",
            "TCGA-SARC": "bone",
            "TCGA-SKCM": "skin",
            "TCGA-UVM": "eye",
        }
    )

    t.loc[:, "tissue"] = (
        t.sample_id.str[13:15]
        .astype("int")
        .replace(
            {
                1: "tumour",
                2: "recurrence",
                3: "peripheral_blood",
                4: "bone_marrow",
                5: "additional_tumour",
                6: "metastasis",
                7: "additional_metastasis",
                11: "matched",
            }
        )
    )
    t.loc[:, "distance"] = t.tissue
    t.loc[:, "source"] = "tcga"

    t = t[
        [
            "sample_id",
            "batch",
            "organ",
            "cohort",
            "sample_type",
            "source",
            "patient_id",
            "tissue",
            "distance",
            "technology",
        ]
    ]

    t.set_index(["sample_id"], inplace=True)

    return t
