#!/usr/bin/env python3

"""Script to download metadata for TCGA samples and store results.
Downloaded results are already available in the repository. Script only provided for
documenting the process.
"""

import io
import os

import pandas
import requests


def get_file_list() -> pandas.DataFrame:
    fields = [
        "file_name",
        "file_size",
        "md5sum",
        "data_category",
        "data_type",
        "state",
        "experimental_strategy",
        "cases.project.project_id",
        "analysis.metadata.read_groups.read_group_id",
        "analysis.metadata.read_groups.read_group_name",
        "analysis.metadata.read_groups.read_length",
        "analysis.metadata.read_groups.sequencing_center",
        "analysis.metadata.read_groups.read_group_qcs.fastq_name",
        "analysis.metadata.read_groups.is_paired_end",
        "analysis.metadata.read_groups.library_name",
        "cases.case_id",
        "cases.submitter_id",
        "associated_entities.case_id",
        "associated_entities.entity_id",
        "associated_entities.entity_submitter_id",
        "associated_entities.entity_type",
    ]

    fields = ",".join(fields)

    files_endpt = "https://api.gdc.cancer.gov/files"

    filters = {
        "op": "and",
        "content": [
            {
                "op": "in",
                "content": {
                    "field": "cases.project.project_id",
                    "value": [
                        "TCGA-COAD",
                        "TCGA-READ",
                        "TCGA-STAD",
                        "TCGA-ESCA",
                        "TCGA-LIHC",
                        "TCGA-CHOL",
                        "TCGA-PAAD",
                        "TCGA-GBM",
                        "TCGA-LGG",
                        "TCGA-HNSC",
                        "TCGA-LUAD",
                        "TCGA-LUSC",
                        "TCGA-MESO",
                        "TCGA-BLCA",
                        "TCGA-PRAD",
                        "TCGA-TGCT",
                        "TCGA-OV",
                        "TCGA-UCEC",
                        "TCGA-CESC",
                        "TCGA-UCS",
                        "TCGA-BRCA",
                        "TCGA-KICH",
                        "TCGA-KIRC",
                        "TCGA-KIRP",
                        "TCGA-ACC",
                        "TCGA-DLBC",
                        "TCGA-LAML",
                        "TCGA-THCA",
                        "TCGA-THYM",
                        "TCGA-PCPG",
                        "TCGA-SARC",
                        "TCGA-SKCM",
                        "TCGA-UVM",
                    ],
                },
            },
            {
                "op": "in",
                "content": {
                    "field": "files.experimental_strategy",
                    "value": ["RNA-Seq"],
                },
            },
            {"op": "in", "content": {"field": "files.data_format", "value": ["BAM"]}},
        ],
    }

    params = {"filters": filters, "fields": fields, "format": "tsv", "size": "100000"}

    response = requests.post(
        files_endpt, headers={"Content-Type": "application/json"}, json=params
    )
    f = io.StringIO(response.content.decode("utf-8"))
    df = pandas.read_csv(f, sep="\t")
    return df


if __name__ == "__main__":
    data = get_file_list()
    data.to_csv(
        os.path.join("transcriptomics", "dbgap_datasets", "tcga_samples.txt"),
        index=False,
        sep="\t",
    )
