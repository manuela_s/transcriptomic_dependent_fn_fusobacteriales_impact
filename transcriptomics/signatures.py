#!/usr/bin/env python3

import io
import os
import subprocess
import tempfile
import warnings

import pandas
import rpy2.rinterface_lib.sexp
import rpy2.robjects
import rpy2.robjects.packages
import rpy2.robjects.pandas2ri

import transcriptomics.util


def run_mcp_counter_classifier(rna: pandas.DataFrame) -> pandas.DataFrame:
    """
    Run MCP-counter classifier from Becht et al., Genome Biol., 2016, PMID: 27765066.
    Wrapper around the `MCPcounter` R package.
    :param rna: dataframe with normalised transcriptomics data, one row per sample and column names should be gene symbols;
    :return: dataframe, with same index as 'rna' and columns: 'T_cells', 'CD8_T_cells', 'Cytotoxic_lymphocytes',
            'B_lineage', 'NK_cells', 'Monocytic_lineage', 'Myeloid_dendritic_cells', 'Neutrophils', 'Endothelial_cells',
            'Fibroblasts'.
    """
    mcp_genes = pandas.read_csv(
        "https://raw.githubusercontent.com/ebecht/MCPcounter/master/Signatures/genes.txt",
        sep="\t",
    )
    missing = mcp_genes.loc[~mcp_genes["HUGO symbols"].isin(rna.columns)]
    warnings.warn(
        "Missing {} genes from signature:\n{}".format(missing.shape[0], missing)
    )
    rna = rna.loc[:, rna.columns.isin(mcp_genes["HUGO symbols"])]

    assert rna.index.is_unique and rna.columns.is_unique

    MCPcounter = rpy2.robjects.packages.importr("MCPcounter")
    with rpy2.robjects.conversion.localconverter(
        rpy2.robjects.default_converter + rpy2.robjects.pandas2ri.converter
    ):
        rna_r_t = rpy2.robjects.conversion.py2rpy(rna.T)
    mcp = transcriptomics.util.r_matrix_to_df(
        MCPcounter.MCPcounter_estimate(rna_r_t, featuresType="HUGO_symbols")
    ).T
    mcp.index = rna.index
    mcp.columns = mcp.columns.str.replace(" ", "_")
    return mcp


def run_quantiseq_classifier(
    rna: pandas.DataFrame, is_microarray=False
) -> pandas.DataFrame:
    """
    Run quanTIseq pipeline from Finotello et al., Genome Med., 2019, PMID: 31126321.
    Wrapper around docker image from https://icbi.i-med.ac.at/software/quantiseq/doc/.
    :param rna: normalized rna data (not standardized and not logged) with one row per sample (in single index) and one
                column per gene named with symbols;
    :param is_microarray: enabled when using gene expression from microarray instead of RNASeq;
    :return: dataframe, with same index as 'rna' and columns: 'Tumour & Stromal cells', 'T cells CD4', 'T cells CD8',
             'T regs', 'Dendritic cells', 'B cells', 'Monocytes', 'Macrophages M1', 'Macrophages M2', 'NK cells',
             'Neutrophils'.
    """
    input_str = rna.reset_index(drop=True).T.to_csv(sep="\t")

    quantiseq_cmd = " ".join(
        [
            "/opt/quantiseq/quanTIseq.sh",
            "--rmgenes=unassigned",
            "--btotalcells=FALSE",
            "--pipelinestart=decon",
            "--tumor=TRUE",
            "--arrays={}".format(str(is_microarray).upper()),
        ]
    )

    # Run icbi/quantiseq2 docker image with custom entrypoint that reads/writes from stdin/stdout instead of using
    # -v bind mounts, as -v does not work when signatures.py already runs in a privileged docker container.
    # (-v refers to host paths, not paths in the container "docker run" is run from).
    sp = subprocess.run(
        [
            "docker",
            "run",
            "-i",
            "--rm",
            "--entrypoint",
            "/bin/sh",
            "icbi/quantiseq2",
            "-c",
            (
                "mkdir /opt/quantiseq/Input/ && "
                "cat > /opt/quantiseq/Input/inputfile.txt && "
                f"{quantiseq_cmd} && "
                "cat /opt/quantiseq/user_output/quanTIseq_cell_fractions.txt"
            ),
        ],
        input=input_str,
        encoding="utf-8",
        check=True,
        capture_output=True,
    )
    quantiseq = pandas.read_table(io.StringIO(sp.stdout))

    quantiseq.columns = quantiseq.columns.str.replace(".", "_").str.lower()

    assert quantiseq.index.equals(rna.reset_index(drop=True).index)
    quantiseq.index = rna.index

    # Clean-up dataframe and pretty-up cell type names
    quantiseq.drop(columns=["sample"], inplace=True)
    quantiseq = quantiseq.rename_axis(columns="cell_type")
    quantiseq.rename(
        columns={
            "b_cells": "B cells",
            "macrophages_m1": "Macrophages M1",
            "macrophages_m2": "Macrophages M2",
            "monocytes": "Monocytes",
            "neutrophils": "Neutrophils",
            "nk_cells": "NK cells",
            "t_cells_cd4": "T cells CD4",
            "t_cells_cd8": "T cells CD8",
            "tregs": "T regs",
            "dendritic_cells": "Dendritic cells",
            "other": "Tumour & Stromal cells",
        },
        inplace=True,
    )
    col_order = [
        "Tumour & Stromal cells",
        "T cells CD4",
        "T cells CD8",
        "T regs",
        "Dendritic cells",
        "B cells",
        "Monocytes",
        "Macrophages M1",
        "Macrophages M2",
        "NK cells",
        "Neutrophils",
    ]
    quantiseq = quantiseq[col_order]

    # Report results in percentages rather than fractions
    quantiseq = quantiseq.mul(100)

    return quantiseq


def run_proliferation_classifier(rna: pandas.DataFrame) -> pandas.Series:
    """
    Run proliferation classifier.
    :param rna: dataframe with normalised transcriptomics data, one row per sample and column names should be gene symbols;
    :return: series, with same index as 'rna' and 'proliferation_score' as values.
    """
    # List of genes taken from Table M1 from Supplementary Data from Nielsen et al. Clin Cancer Res., 2010 (PMID: 20837693)
    proliferation_genes = [
        "BIRC5",
        "CCNB1",
        "CDC20",
        "NUF2",
        "CEP55",
        "NDC80",
        "MKI67",
        "PTTG1",
        "RRM2",
        "TYMS",
        "UBE2C",
    ]
    missing = set(proliferation_genes).difference(rna.columns)
    if len(missing) > 0:
        warnings.warn(
            "Missing {} genes from signature:\n{}".format(len(missing), missing)
        )

    rna = rna.loc[:, set(proliferation_genes).intersection(rna.columns)]

    return rna.mean(axis=1).rename("proliferation_score")


def run_dna_damage_classifier(rna: pandas.DataFrame) -> pandas.Series:
    """
    Run DNA damage classifier.
    :param rna: dataframe with normalised transcriptomics data, one row per sample and column names should be gene symbols;
    :return: series, with same index as 'rna' and 'dna_damage_score' as values.
    """
    # rna: dataframe with normalized rna data.
    # column names should be gene symbols.

    # List of genes taken from https://www.biorxiv.org/content/10.1101/519603v1.full.pdf
    dna_damage_genes = [
        "PRKDC",
        "NEIL3",
        "FANCD2",
        "BRCA2",
        "EXO1",
        "XRCC2",
        "RFC4",
        "USP1",
        "UBE2T",
        "FAAP24",
    ]
    missing = set(dna_damage_genes).difference(rna.columns)
    if len(missing) > 0:
        warnings.warn(
            "Missing {} genes from signature:\n{}".format(len(missing), missing)
        )

    rna = rna.loc[:, set(dna_damage_genes).intersection(rna.columns)]

    return rna.mean(axis=1).rename("dna_damage_score")


def run_wnt_classifier(rna: pandas.DataFrame) -> pandas.Series:
    """
    Run WNT classifier.
    :param rna: dataframe with normalised transcriptomics data, one row per sample and column names should be gene symbols;
    :return: series, with same index as 'rna' and 'wnt_score' as values.
    """

    # List of genes taken from https://www.gsea-msigdb.org/gsea/msigdb/cards/KEGG_WNT_SIGNALING_PATHWAY
    wnt_genes = [
        "AC023512.1",
        "APC",
        "APC2",
        "AXIN1",
        "AXIN2",
        "BTRC",
        "CACYBP",
        "CAMK2A",
        "CAMK2B",
        "CAMK2D",
        "CAMK2G",
        "CCND1",
        "CCND2",
        "CCND3",
        "CER1",
        "CHD8",
        "CHP1",
        "CHP2",
        "CREBBP",
        "CSNK1A1",
        "CSNK1A1L",
        "CSNK1E",
        "CSNK2A1",
        "CSNK2A2",
        "CSNK2B",
        "CTBP1",
        "CTBP2",
        "CTNNB1",
        "CTNNBIP1",
        "CUL1",
        "CXXC4",
        "DAAM1",
        "DAAM2",
        "DKK1",
        "DKK2",
        "DKK4",
        "DVL1",
        "DVL2",
        "DVL3",
        "EP300",
        "FBXW11",
        "FOSL1",
        "FRAT1",
        "FRAT2",
        "FZD1",
        "FZD10",
        "FZD2",
        "FZD3",
        "FZD4",
        "FZD5",
        "FZD6",
        "FZD7",
        "FZD8",
        "FZD9",
        "GSK3B",
        "JUN",
        "LEF1",
        "LRP5",
        "LRP6",
        "MAP3K7",
        "MAPK10",
        "MAPK8",
        "MAPK9",
        "MMP7",
        "MYC",
        "NFAT5",
        "NFATC1",
        "NFATC2",
        "NFATC3",
        "NFATC4",
        "NKD1",
        "NKD2",
        "NLK",
        "PLCB1",
        "PLCB2",
        "PLCB3",
        "PLCB4",
        "PORCN",
        "PPARD",
        "PPP2CA",
        "PPP2CB",
        "PPP2R1A",
        "PPP2R1B",
        "PPP2R5A",
        "PPP2R5B",
        "PPP2R5C",
        "PPP2R5D",
        "PPP2R5E",
        "PPP3CA",
        "PPP3CB",
        "PPP3CC",
        "PPP3R1",
        "PPP3R2",
        "PRICKLE1",
        "PRICKLE2",
        "PRKACA",
        "PRKACB",
        "PRKACG",
        "PRKCA",
        "PRKCB",
        "PRKCG",
        "PRKX",
        "PSEN1",
        "RAC1",
        "RAC2",
        "RAC3",
        "RBX1",
        "RHOA",
        "ROCK1",
        "ROCK2",
        "RUVBL1",
        "SENP2",
        "SFRP1",
        "SFRP2",
        "SFRP4",
        "SFRP5",
        "SIAH1",
        "SKP1",
        "SMAD2",
        "SMAD3",
        "SMAD4",
        "SOX17",
        "TBL1X",
        "TBL1XR1",
        "TBL1Y",
        "TCF7",
        "TCF7L1",
        "TCF7L2",
        "TP53",
        "VANGL1",
        "VANGL2",
        "WIF1",
        "WNT1",
        "WNT10A",
        "WNT10B",
        "WNT11",
        "WNT16",
        "WNT2",
        "WNT2B",
        "WNT3",
        "WNT3A",
        "WNT4",
        "WNT5A",
        "WNT5B",
        "WNT6",
        "WNT7A",
        "WNT7B",
        "WNT8A",
    ]
    missing = set(wnt_genes).difference(rna.columns)
    if len(missing) > 0:
        warnings.warn(
            "Missing {} genes from signature:\n{}".format(len(missing), missing)
        )

    rna = rna.loc[:, set(wnt_genes).intersection(rna.columns)]

    return rna.mean(axis=1).rename("wnt_score")


def run_metastasis_classifier(rna: pandas.DataFrame) -> pandas.Series:
    """
    Run metastasis classifier.
    :param rna: dataframe with normalised transcriptomics data, one row per sample and column names should be gene symbols;
    :return: series, with same index as 'rna' and 'metastasis_score' as values.
    """
    # List of markers from https://www.nature.com/articles/ng1060z/tables/1 (Ramaswamy et al., Nat. Genet., 2003, PMID: 12469122)
    up_markers = [
        "SNRPF",
        "EIF4EL3",
        "HNRPAB",
        "DHPS",
        "PTTG1",
        "COL1A1",
        "COL1A2",
        "LMNB1",
    ]
    down_markers = [
        "ACTG2",
        "MYLK",
        "MYH11",
        "CNN1",
        "HLA-DPB1",
        "RUNX1",
        "MT3",
        "NR4A1",
        "RBM5",
    ]

    missing_up = set(up_markers).difference(rna.columns)
    if len(missing_up) > 0:
        warnings.warn(
            "Missing {} genes from signature:\n{}".format(len(missing_up), missing_up)
        )
    missing_down = set(down_markers).difference(rna.columns)
    if len(missing_down) > 0:
        warnings.warn(
            "Missing {} genes from signature:\n{}".format(
                len(missing_down), missing_down
            )
        )

    up_score = rna[set(up_markers).difference(set(missing_up))].mean(axis=1)
    down_score = rna[set(down_markers).difference(set(missing_down))].mean(axis=1)

    return (up_score - down_score).rename("metastasis_score")


def run_stemness_classifier(rna: pandas.DataFrame) -> pandas.Series:
    """
    Run stemness classifier.
    :param rna: dataframe with normalised transcriptomics data, one row per sample and column names should be gene symbols;
    :return:  series, with same index as 'rna' and 'stemness_score' as values.
    """
    # CD44 + list of genes taken from https://www.gsea-msigdb.org/gsea/msigdb/cards/GALIE_TUMOR_STEMNESS_GENES
    stemness_genes = ["BMP4", "KRT14", "PDGFRA", "PTCH1", "TGFBR2", "TNC", "CD44"]
    missing = set(stemness_genes).difference(rna.columns)
    if len(missing) > 0:
        warnings.warn(
            "Missing {} genes from signature:\n{}".format(len(missing), missing)
        )

    rna = rna.loc[:, set(stemness_genes).intersection(rna.columns)]

    return rna.mean(axis=1).rename("stemness_score")


def run_hippo_classifier(rna: pandas.DataFrame) -> pandas.Series:
    """
    Run hippo classifier.
    :param rna: dataframe with normalised transcriptomics data, one row per sample and column names should be gene symbols;
    :return:  series, with same index as 'rna' and 'hippo' as values.
    """
    # List of genes taken from supplementary materials from Wang et al. Cell Reports., 2018 (PMID: 30380420)
    hippo_genes = [
        "MYOF",
        "AMOTL2",
        "LATS2",
        "CTGF",
        "CYR61",
        "ANKRD1",
        "ASAP1",
        "AXL",
        "F3",
        "IGFBP3",
        "CRIM1",
        "FJX1",
        "FOXF2",
        "GADD45A",
        "CCDC80",
        "NT5E",
        "DOCK5",
        "PTPN14",
        "ARHGEF17",
        "NUAK2",
        "TGFB2",
        "RBMS3",
    ]
    missing = set(hippo_genes).difference(rna.columns)
    if len(missing) > 0:
        warnings.warn(
            "Missing {} genes from signature:\n{}".format(len(missing), missing)
        )

    rna = rna.loc[:, set(hippo_genes).intersection(rna.columns)]

    return rna.mean(axis=1).rename("hippo_score")


def run_hypoxia_classifier(rna: pandas.DataFrame) -> pandas.Series:
    """
    Run hypoxia classifier.
    :param rna: dataframe with normalised transcriptomics data, one row per sample and column names should be gene symbols;
    :return:  series, with same index as 'rna' and 'hypoxia_score' as values.
    """
    # From https://www.nature.com/articles/s41588-018-0318-2 and references therein
    # See also https://www.biorxiv.org/content/biorxiv/early/2018/10/14/442921.full.pdf
    # Gene symbols from "core signature" sheet in Supplementary materials of Buffa (BJC, 2010, https://www.ncbi.nlm.nih.gov/pubmed/20087356)
    hypoxia_markers = [
        "CDH5",
        "ELTD1",
        "CLEC14A",
        "LDB2",
        "ECSCR",
        "MYCT1",
        "RHOJ",
        "VWF",
        "TIE1",
        "KDR",
        "ESAM",
        "CD93",
        "PTPRB",
        "GPR116",
        "SPARCL1",
        "EMCN",
        "ROBO4",
        "ENG",
        "TEK",
        "S1PR1",
        "A2M",
        "JAM2",
        "MEF2C",
        "COL15A1",
        "PECAM1",
        "CALCRL",
        "CLEC3B",
        "PLVAP",
        "RGS5",
        "LRRC32",
        "EBF1",
        "ADCY4",
        "ACVRL1",
        "GPR124",
        "APLNR",
        "TM4SF18",
        "GNG11",
        "CNRIP1",
        "ZNF423",
        "GIMAP8",
        "PDGFD",
        "ITGA9",
        "EDNRB",
    ]
    missing = set(hypoxia_markers).difference(rna.columns)
    if len(missing) > 0:
        warnings.warn(
            "Missing {} genes from signature:\n{}".format(len(missing), missing)
        )

    rna = rna.loc[:, set(hypoxia_markers).intersection(rna.columns)]

    return rna.mean(axis=1).rename("hypoxia_score")


def run_emt_classifier(rna: pandas.DataFrame) -> pandas.Series:
    """
    Run EMT classifier.
    :param rna: dataframe with normalised transcriptomics data, one row per sample and column names should be gene symbols;
    :return:  series, with same index as 'rna' and 'emt_score' as values.
    """
    # List of genes taken from Kwang Chae et al., Sci Rep., 2018, PMID: 29440769.
    epythelial_markers = ["CDH1", "DSP", "OCLN"]
    mesenchymal_markers = [
        "VIM",
        "CDH2",
        "FOXC2",
        "SNAI1",
        "SNAI2",
        "TWIST1",
        "FN1",
        "ITGB6",
        "MMP2",
        "MMP3",
        "MMP9",
        "SOX10",
        "GCS",
    ]

    missing_epythelial = set(epythelial_markers).difference(rna.columns)
    if len(missing_epythelial) > 0:
        warnings.warn(
            "Missing {} genes from signature:\n{}".format(
                len(missing_epythelial), missing_epythelial
            )
        )
    missing_mesenchymal = set(mesenchymal_markers).difference(rna.columns)
    if len(missing_mesenchymal) > 0:
        warnings.warn(
            "Missing {} genes from signature:\n{}".format(
                len(missing_mesenchymal), missing_mesenchymal
            )
        )

    # Calculations based on https://www.ncbi.nlm.nih.gov/pubmed/29440769, list of markers based on https://www.ncbi.nlm.nih.gov/pubmed/28073171
    epythelial_score = rna[
        set(epythelial_markers).difference(set(missing_epythelial))
    ].mean(axis=1)
    mesenchymal_score = rna[
        set(mesenchymal_markers).difference(set(missing_mesenchymal))
    ].mean(axis=1)

    return (epythelial_score - mesenchymal_score).rename("emt_score")


def run_tis_score(rna: pandas.DataFrame) -> pandas.Series:
    """
    Run tumour inflammation signature (TIS) classifier.
    :param rna: dataframe with normalised transcriptomics data, one row per sample and column names should be gene symbols;
    :return:  series, with same index as 'rna' and 'tis_score' as values.
    """
    # List of genes taken from panel D of Figure 1 from Damotte et al. J. Transl. Med., 2019, PMID: 31684954.
    tis_genes = [
        "CD276",
        "HLA-DQA1",
        "CD274",
        "IDO1",
        "HLA-DRB1",
        "HLA-E",
        "CMKLR1",
        "PDCD1LG2",
        "PSMB10",
        "LAG3",
        "CXCL9",
        "STAT1",
        "CD8A",
        "CCL5",
        "NKG7",
        "TIGIT",
        "CD27",
        "CXCR6",
    ]
    missing = set(tis_genes).difference(rna.columns)
    if len(missing) > 0:
        warnings.warn(
            "Missing {} genes from signature:\n{}".format(len(missing), missing)
        )

    rna = rna.loc[:, set(tis_genes).intersection(rna.columns)]

    return rna.mean(axis=1).rename("tis_score")


def run_ifng_score(rna: pandas.DataFrame) -> pandas.Series:
    """
    Run tumour interferon gamma signature (IFNG) classifier.
    :param rna: dataframe with normalised transcriptomics data, one row per sample and column names should be gene symbols;
    :return:  series, with same index as 'rna' and 'ifng_score' as values.
    """
    # List of genes taken from Higgs et al. Cli. Cancer Res., 2018, PMID: 29716923.
    ifng_genes = ["IFNG", "LAG3", "CXCL9", "CD274"]
    missing = set(ifng_genes).difference(rna.columns)
    if len(missing) > 0:
        warnings.warn(
            "Missing {} genes from signature:\n{}".format(len(missing), missing)
        )

    rna = rna.loc[:, set(ifng_genes).intersection(rna.columns)]

    return rna.mean(axis=1).rename("ifng_score")


def run_cytolytic_score(rna: pandas.DataFrame) -> pandas.Series:
    """
    Run cytolytic classifier.
    :param rna: dataframe with normalised transcriptomics data, one row per sample and column names should be gene symbols;
    :return:  series, with same index as 'rna' and 'cytolytic_score' as values.
    """
    # List of markers from Rooney et al., Cell, 2016, PMID: 25594174.
    rna = rna.loc[:, {"GZMA", "PRF1"}.intersection(rna.columns)]

    return rna.mean(axis=1).rename("cytolytic_score")


def run_cms_classifier(
    data: pandas.DataFrame, method: str = "RF", class_type: str = "prediction"
) -> pandas.Series:
    """
    Run CMS classifier from Guinney et al., Nat Med., 2015, PMID: 26457759.
    Wrapper around the `CMSclassifier` R package.
    :param data: dataframe with normalised transcriptomics data, one row per sample and column names mapped to entrez
                 gene ids;
    :param method: string, either 'RF' (Random Forest) or 'SSP' (Single Sample Predictor);
    :param class_type: string, either 'prediction' or 'nearest';
    :return: dataframe, with same index as 'rna' and 'cms_class' as values.
    """
    CMSclassifier = rpy2.robjects.packages.importr("CMSclassifier")

    # Subset to only include genes used by the classifier (to avoid imputation based on whole set of genes)
    features = list(CMSclassifier.listModelGenes(method))

    missing_features = set(features).difference(data.columns)
    warnings.warn(
        "Missing {} entrezgenes from signature:\n{}".format(
            len(missing_features), missing_features
        )
    )
    data = data.loc[:, data.columns.isin(features)]

    with rpy2.robjects.conversion.localconverter(
        rpy2.robjects.default_converter + rpy2.robjects.pandas2ri.converter
    ):
        cms = CMSclassifier.classifyCMS(data.T, method=method)
        cms = rpy2.robjects.conversion.rpy2py(cms[2])
    cms.index = data.index

    if class_type == "prediction":
        # Use None to represent missing values:
        cms.loc[
            cms[method + ".predictedCMS"] == rpy2.rinterface_lib.sexp.NACharacterType(),
            method + ".predictedCMS",
        ] = None
        cms_res = cms[method + ".predictedCMS"].rename("cms_class")
    elif class_type == "nearest":
        cms_res = cms[method + ".nearestCMS"].rename("cms_class")
    else:
        cms_res = cms

    return cms_res


def remap_genes_and_run_cms_classifier(
    mapping_fn: str,
    data: pandas.DataFrame,
    method: str = "RF",
    class_type: str = "prediction",
) -> pandas.Series:
    """
    Remap transcriptomics data to entrez ids and run CMS classifier.
    :param mapping_fn: filename for csv file that maps data column names to other RNA ids (see map_ids_from_ensembl.py);
    :param data: dataframe with normalised transcriptomics data, one row per sample and gene IDs as column names;
    :param method: string, either 'RF' (Random Forest) or 'SSP' (Single Sample Predictor);
    :param class_type: string, either 'prediction' or 'nearest';
    :return: dataframe, with same index as 'data' and 'cms_class' as values.
    """
    # Find the list of entrezgene features needed for this method:
    CMSclassifier = rpy2.robjects.packages.importr("CMSclassifier")
    features = list(CMSclassifier.listModelGenes(method))

    # Some features may be listed as "1 /// 2". Extract:
    mapping = pandas.Series(features).str.split(" /// ", expand=True)
    mapping.index = features
    mapping = mapping.unstack().reset_index(0, drop=True).dropna()

    data = transcriptomics.util.remap_rna_ids(
        data, mapping_fn, "entrezgene", mapping.values
    )

    # Identify entrezgene with highest variance in 'a /// b' pairs.
    mapping = mapping.to_frame("entrezgene").join(
        data.var().rename("variance"), on="entrezgene", how="inner"
    )
    mapping.sort_values("variance", inplace=True)

    mapping = mapping[~mapping.index.duplicated(keep="last")]
    # Rename columns to match feature names above.
    data = data.rename(
        columns=mapping.reset_index().set_index("entrezgene")["index"].to_dict()
    )

    return run_cms_classifier(data, method=method, class_type=class_type)


def run_cris_classifier(data: pandas.DataFrame, nresmpl: int = 1000) -> pandas.Series:
    """
    Run CRIS classifier from Isella et al., Nat. Commun., 2017, PMID: 28561063.
    Wrapper around the `CRISclassifier` R package.
    :param data: dataframe with normalised transcriptomics data, one row per sample, flattened (not MultiIndex) index
                 and column names should be gene symbols;
    :param nresmpl: number of resamples as defined in the CRISclassifier R package;
    :return: dataframe, with same index as 'data' and 'cris_class' as values.
    """
    assert not isinstance(
        data.index, pandas.MultiIndex
    ), "Flatten multi-index before calling run_cris_classifier"

    data = data.transpose()
    # Drop variables with "no" variance.
    data = data.loc[data.var(axis=1) > 1e-10]

    CRISclassifier = rpy2.robjects.packages.importr("CRISclassifier")
    utils = rpy2.robjects.packages.importr("utils")
    env = rpy2.robjects.Environment()
    utils.data("features", package="CRISclassifier", envir=env)

    # Features dataframe in R has the first 2 columns with the same header causing conversion to drop the first column.
    # Workaround: convert one column at the time
    features = pandas.DataFrame(
        {
            name: rpy2.robjects.pandas2ri.converter.rpy2py(env["features"][i])
            for (i, name) in enumerate(["symbol", "class_name", "class_int"])
        }
    )

    missing_features = set(features.symbol).difference(data.index)
    warnings.warn(
        "Missing {} genes from signature:\n{}".format(
            len(missing_features), missing_features
        )
    )

    common_features = set(features.symbol).intersection(data.index)
    data = data.loc[common_features]

    input_file = tempfile.NamedTemporaryFile(suffix=".txt")
    output_dir = tempfile.TemporaryDirectory()
    data.to_csv(input_file.name, sep="\t")
    CRISclassifier.cris_classifier(
        input_file.name, os.path.join(output_dir.name, "output"), nresmpl, 42
    )
    results = pandas.read_table(
        os.path.join(output_dir.name, "output_prediction_result.xls")
    )
    results.index = data.columns

    # Remove labels with BH.FDR>0.2 as per Isella et al., Nature Communications, 2017
    results.loc[results["BH.FDR"] >= 0.2, "predict.label2"] = None
    return results["predict.label2"].rename("cris_class")
