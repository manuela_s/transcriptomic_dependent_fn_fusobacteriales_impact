#!/usr/bin/env python3

import os
import typing

import mygene
import pandas


def create_gene_mapping(
    rna_ids: typing.Sequence[str], scopes: typing.Sequence[str]
) -> pandas.DataFrame:
    """Create a DataFrame that maps RNA identifiers ('ensemblid', 'entrezegene' or 'symbol') to other gene ids.
    :param rna_ids: sequence of strings, RNA identifiers ('ensemblid', 'entrezegene' or 'symbol');
    :param scopes: sequence of strings, The scopes to search for the rna_ids in;
    :return: pandas DataFrame, one row per queried RNA identifier and columns including 'ensembl_id', 'entrezegene' and
             'symbol'.
    """
    mg = mygene.MyGeneInfo()
    m = mg.querymany(
        rna_ids,
        scopes=scopes,
        fields=["symbol", "entrezgene", "ensembl.gene", "name", "alias", "description"],
        species="human",
        as_dataframe=True,
        dotfield=True,
    )
    m.rename(columns={"ensembl.gene": "ensembl_id"}, inplace=True)

    return m


def create_rna_mapping_file(
    rna_fn: str,
    scopes: typing.Sequence[str] = [
        "ensembl.gene",
        "entrezgene",
        "symbol",
        "alias",
        "name",
        "other_names",
    ],
) -> None:
    """Create a mapping file that maps RNA IDs (ensemblid, entrezegene or symbol) to other gene ids.
    :param rna_fn: filename for csv file with RNA sample data. One row per sample. One column per gene. First
                   column is expected to be the sample identifier;
    :param scopes: sequence of strings, The scopes to search for the rna_ids in. Defaults to all scopes;
    Creates a _mapping.csv file for the rna_fn.
    """
    df = pandas.read_csv(rna_fn, index_col=0, nrows=1)
    m = create_gene_mapping(df.columns, scopes)
    m.to_csv(os.path.splitext(rna_fn)[0] + "_mapping.csv")
