[![pythonv3.8](https://img.shields.io/badge/python-v3.8-blue)](https://www.python.org/)
[![License](https://img.shields.io/badge/License-BSD%202--Clause-blue.svg)](https://opensource.org/licenses/bsd-license.php)
[![ZENODO](https://zenodo.org/badge/DOI/10.5281/zenodo.4019142.svg)](https://doi.org/10.5281/zenodo.4019142)


# Patients with mesenchymal tumours and high *Fusobacteriales* prevalence have worse prognosis in colorectal cancer (CRC)

Datasets and source-code underlying the computational analyses included in the manuscript [*"Patients with mesenchymal
tumours and high *Fusobacteriales* prevalence have worse prognosis in colorectal cancer (CRC)"*](http://dx.doi.org/10.1136/gutjnl-2021-325193)
authored by Manuela Salvucci, Nyree Crawford, Katie Stott, Susan Bullman, Daniel Longley and Jochen H. M. Prehn, *Gut*,
2021.


## Data availability

- *Fusobacterium nucleatum* load for tumour resections determined by quantitative real-time polymerase chain reaction of
  the in-house Taxonomy patients cohort are available from [fusobacterium_nucleatum.csv](taxonomy_cohort/data/fusobacterium_nucleatum.csv).
- Relative abundance of *Fusobacteriales* order and higher resolution taxonomic ranks (family, genus, species), including
  *Fusobacterium nucleatum*, for colon (COAD) and rectal (READ) patients of The Cancer Genome Atlas (TCGA) cohort were
  estimated from RNASeq sequencing using a subtractive method implemented by the [PathSeq pipeline](https://pubmed.ncbi.nlm.nih.gov/29982281/)
  are available from [data/pathseq_estimates/](data/pathseq_estimates/).

## Requirements

Analysis was performed in Ubuntu 20.04 with python 3.8.10 and R 3.6.3.
The analysis code depends on:

- ubuntu packages listed in [apt.txt](binder/apt.txt);
- python packages listed in [requirements.txt](binder/requirements.txt);
- additional R packages not installed from ubuntu listed in [install_r_packages.R](binder/install_r_packages.R);
- [GISTIC](http://portals.broadinstitute.org/cgi-bin/cancer/publications/pub_paper.cgi?mode=view&paper_id=216&p=t)
  ([Mermel *et al.*, Genome Biol., 2011, PMID: 21527027](https://pubmed.ncbi.nlm.nih.gov/21527027/)).
- Docker for running [quanTIseq](https://icbi.i-med.ac.at/software/quantiseq/doc/) ([Finotello *et al.*, Genome Med.,
  2019, PMID: 31126321](https://pubmed.ncbi.nlm.nih.gov/31126321/)).

  
A [Dockerfile](binder/Dockerfile) is available for building a docker container with all requirements.

The container can be built with:

```bash
docker build -f binder/Dockerfile --tag fn_fusobacteriales_gut_paper:1.0 .
```

A pre-built docker image is available in the [Zenodo archive](https://doi.org/10.5281/zenodo.4019142).

A container with the docker image can be run with:

```bash
docker run --privileged=true -v /var/run/docker.sock:/var/run/docker.sock --publish 8888:8888 --tty --interactive fn_fusobacteriales_gut_paper:1.0
```
Note that the container needs to be run in `privileged` mode and with access to docker to run `quanTIseq`.

The container will start a Jupyter notebook with access to code and data files and the commands below can be run in the
terminal.

## Analysis steps

To prepare the data and reproduce  the results of the analysis, please run the following sequentially:

| Script                                                                                                                                  | Description                                                                                                                                                                                                                                                                                                                                                                          |
| :-------------------------------------------------------------------------------------------------------------------------------------- | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| [tcga_pancancer/prepare_tcga_data.sh](tcga_pancancer/prepare_tcga_data.sh)                                                              | Download and prepare clinical and molecular data for TCGA datasets for downstream analysis. A [synapse](https://www.synapse.org/) account is required to download the Consensus Molecular Subtyping labels for the TCGA-COAD-READ dataset and username/password will be prompted for. |
| [tcga_pancancer/fetch_gene_mapping.py](tcga_pancancer/fetch_gene_mapping.py)                                                            | (Optional) fetch mapping between gene identifiers. Defaults to use stored mappings to reproduce analysis presented in the manuscript.                                                                                                                                                 |
| [taxonomy_cohort/prepare_data.py](taxonomy_cohort/prepare_data.py)                                                                      | Compute cell type composition using quanTIseq and transcriptomic-based signatures for patients of the in-house Taxonomy cohort for downstream analysis.                                                                                                                               |
| [extract_additional_mutational_features_for_tcga_coad_read_cohort](extract_additional_mutational_features_for_tcga_coad_read_cohort.py) | Extract additional mutational features (transitions, transversions and conversion changes) for patients of the TCGA-COAD-READ cohort.                                                                                                                                                 |
| [run_cna_analysis.py](run_cna_analysis.py)                                                                                              | Run GISTIC analysis on TCGA pancancer patients.                                                                                                                                                                                                                                       |
| [generate_figures_and_tables_in_manuscript_and_supplements.sh](./generate_figures_and_tables_in_manuscript_and_supplements.sh)        | Generate figures and tables for the main manuscript and supplements.                                                                                                                                                                                                                  |

Complete commands:

```bash
tcga_pancancer/prepare_tcga_data.sh
PYTHONPATH=. python3 taxonomy_cohort/prepare_data.py
python3 extract_additional_mutational_features_for_tcga_coad_read_cohort.py
python3 run_cna_analysis.py
./generate_figures_and_tables_in_manuscript_and_supplements.sh
```


## Citation

Please cite [https://doi.org/10.5281/zenodo.4019142](https://doi.org/10.5281/zenodo.4019142) and the corresponding paper, if using these datasets and/or source code.


## Contact information

- Prof. Jochen H. M. Prehn (JPrehn@rcsi.ie);
- Manuela Salvucci (ms@msalvucci.com).


## Funding

This study was supported by a grant from the Health Research Board and Science Foundation Ireland to JHMP (16/US/3301),
a studentship sponsored by The Northern Ireland Department for the Economy (NI DfE), and by funding from NI DfE (SFI-DEL
14/1A/2582, STL/5715/15). 


## Acknowledgments

The results included here are in part based on data generated by the [TCGA Research Network](https://www.cancer.gov/tcga).
We wish to acknowledge the information technology department at the Royal College of Surgeons in Ireland and the
DJEI/DES/SFI/HEA Irish Centre for High-End Computing for the provision of computational facilities and support.
