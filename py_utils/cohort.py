#!/usr/bin/env python3

"""
Common functionality for dealing with patient cohorts.
"""

import abc
import typing

import pandas
import sklearn.preprocessing


def standardization_helper(data: pandas.DataFrame) -> pandas.DataFrame:
    """Standardize RNA dataframe."""
    return pandas.DataFrame(
        sklearn.preprocessing.RobustScaler().fit_transform(data), index=data.index, columns=data.columns)


class Cohort(abc.ABC):

    @abc.abstractmethod
    def get_clinical(self) -> pandas.DataFrame:
        """Get clinical data."""

    @abc.abstractmethod
    def get_rna(self, standardization: bool = True) -> pandas.DataFrame:
        """Get RNA gene expression."""

    @abc.abstractmethod
    def get_subtypes(self) -> pandas.DataFrame:
        """Get subtype information for the patients in the cohort."""

    @abc.abstractmethod
    def get_immuno_composition(self) -> pandas.DataFrame:
        """Get immuno composition."""


class CombinedCohort(Cohort):

    def __init__(self, cohorts: typing.Sequence[Cohort]):
        self.cohorts = cohorts

    def get_clinical(self):
        cli = pandas.concat([cohort.get_clinical() for cohort in self.cohorts])
        return cli

    def get_cna(self) -> pandas.DataFrame:
        cna = pandas.concat([cohort.get_cna() for cohort in self.cohorts])
        return cna

    def get_rna(self, standardization: bool = True) -> pandas.DataFrame:
        return pandas.concat([cohort.get_rna(standardization=standardization) for cohort in self.cohorts])

    def get_rppa(self, standardization: bool = True) -> pandas.DataFrame:
        return pandas.concat([cohort.get_rppa(standardization=standardization) for cohort in self.cohorts])

    def get_mutations(self) -> pandas.DataFrame:
        data = pandas.concat([cohort.get_mutations() for cohort in self.cohorts])
        return data

    def get_subtypes(self) -> pandas.DataFrame:
        return pandas.concat([cohort.get_subtypes() for cohort in self.cohorts])

    def get_immuno_composition(self) -> pandas.DataFrame:
        return pandas.concat([cohort.get_immuno_composition() for cohort in self.cohorts])

    def get_quantiseq_cell_types(self) -> pandas.DataFrame:
        return pandas.concat([cohort.get_quantiseq_cell_types() for cohort in self.cohorts])

    def get_rna_based_signatures(self) -> pandas.DataFrame:
        return pandas.concat([cohort.get_rna_based_signatures() for cohort in self.cohorts])

    def get_microsatellite_and_mutational_status(self) -> pandas.DataFrame:
        data = pandas.concat([cohort.get_microsatellite_and_mutational_status() for cohort in self.cohorts])
        return data
