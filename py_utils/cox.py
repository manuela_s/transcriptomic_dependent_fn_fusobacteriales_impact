#!/usr/bin/env python3

import abc
import dataclasses
import io
import os
import pathlib
import typing

import lifelines
import matplotlib.axes
import matplotlib.backends.backend_svg
import matplotlib.pyplot
import numpy
import pandas
import pandas.io.formats.style

import py_utils.compare_lr_test
import py_utils.format_pvalue


class HazardRatioCI:
    """
    Class representing the confidence interval (CI) of a hazard ratio (HR) in a Cox regression model.
    """

    def __init__(
        self,
        hr: float,
        lower_ci: float,
        upper_ci: float,
        vmin: float = 0,
        vmax: float = 2,
    ):
        """
        Construct HazardRatioCI.
        :param hr: hazard ratio;
        :param lower_ci: 2.5% confidence value of hazard ratio;
        :param upper_ci: 97.5% confidence value of hazard ratio;
        :param vmin: minimum hazard ratio to show in plot;
        :param vmax: maximum hazard ratio to show in plot.
        """
        self.hr = hr
        self.lower_ci = lower_ci
        self.upper_ci = upper_ci
        self.vmin = vmin
        self.vmax = vmax

    def __repr__(self):
        return "<HazardRatio_CI {:.2f} ({:.2f}-{:.2f})".format(
            self.hr, self.lower_ci, self.upper_ci
        )

    def plot(self, ax: matplotlib.axes.Axes = None) -> None:
        """
        Plot hazard ratio with corresponding 95% confidence interval.
        :param ax: axes to plot into or None to plot in the current axes (gca).
        """
        if ax is None:
            ax = matplotlib.pyplot.gca()
        ax.axvline(1, linestyle=":", color="gray")
        if not (numpy.isnan(self.lower_ci) or numpy.isnan(self.upper_ci)):
            ax.plot([self.lower_ci, self.upper_ci], [0, 0], "k")
        if not numpy.isnan(self.hr):
            ax.plot(self.hr, 0, "ko")
        ax.set_xlim(self.vmin, self.vmax)
        ax.set_ylim(-1, 1)
        ax.set_axis_off()

    def to_svg(self, figsize: typing.Tuple[float, float] = (2, 0.3)) -> str:
        """
        Plot hazard ratio with corresponding 95% confidence interval and return as svg.
        :param figsize: width and height of figure to generate (in inches);
        :return: svg string.
        """
        fig = matplotlib.pyplot.Figure(figsize=figsize, facecolor="none")
        canvas = matplotlib.backends.backend_svg.FigureCanvasSVG(fig)
        ax = fig.add_subplot(111)
        fig.subplots_adjust(bottom=0, left=0, right=1, top=1)
        self.plot(ax)
        svg = io.StringIO()
        canvas.print_svg(svg)

        return svg.getvalue().split("\n", 4)[-1]  # Strip-off xml and doc type tags


class AbstractCoxModels(abc.ABC):
    """Superclass for classes implementing interfaces to Cox regression models."""

    @abc.abstractmethod
    def fit(
        self,
        df: pandas.DataFrame,
        duration_col: str,
        event_col: str,
        drop_na: bool,
        step_size: float,
    ) -> "AbstractCoxModels":
        """
        Fit Cox regression model(s) to the data.
        :param df: dataframe with data to fit to;
        :param duration_col: name of column in `df` that contains the event/censoring time;
        :param event_col: name of the column in `df` that contains the event information (`True` if the subject had an
               event, `False` if no event was observed and the subject was censored);
        :param drop_na: whether to drop rows with NaNs. If `True` drop rows with NaNs, if `False` the fitting will fail
               and raise an exception if NaNs are present;
        :param step_size: initial step size for the fitting algorithm to be passed onto lifelines.CoxPHFitter;
        :return: fitted instance of AbstractCoxModels.
        """

    def get_term_levels(self) -> pandas.DataFrame:
        """Generate a dataframe with term, term_level and level columns for each variable and level in input data."""
        term = []
        term_level = []
        level = []
        for v in self.variables_:
            if pandas.api.types.is_categorical_dtype(self.df_[v]):
                for cat in self.df_[v].cat.categories:
                    term.append(v)
                    term_level.append("{}_{}".format(v, cat))
                    level.append(cat)
            elif pandas.api.types.is_numeric_dtype(self.df_[v]):
                term.append(v)
                term_level.append(v)
                level.append(None)
            else:
                raise Exception("Unable to determine term_level for {}".format(v))

        return pandas.DataFrame(
            {
                "term": pandas.Categorical(term, self.variables_),
                "term_level": pandas.Categorical(term_level, term_level),
                "level": pandas.Categorical(level, pandas.Series(level).dropna()),
            },
            index=term_level,
        )

    def get_reference_level(self, term: str) -> typing.Union[str, None]:
        """Get reference level for a categorical term of the Cox regression model."""
        if pandas.api.types.is_categorical_dtype(self.df_[term]):
            return self.df_[term].cat.categories[0]
        elif pandas.api.types.is_numeric_dtype(self.df_[term]):
            return None
        else:
            raise Exception("Unable to determine reference level for {}".format(term))

    def get_n_per_level(self, term: str, level: typing.Union[str, None]) -> int:
        """Get n count for term/level in the Cox regression model."""
        if pandas.api.types.is_categorical_dtype(self.df_[term]):
            return self.df_[term][self.df_[term] == level].count()
        elif pandas.api.types.is_numeric_dtype(self.df_[term]):
            return self.df_[term].count()
        else:
            raise Exception(
                "Unable to determine n_per_level for {}: {}".format(term, level)
            )

    @property
    @abc.abstractmethod
    def summary_per_model(self) -> pandas.DataFrame:
        """
        Generates a dataframe with per-model summary statistics with index 'model' and columns 'pvalue_lr', 'c_index',
        'n_total' and 'n_events'.
        """

    @property
    @abc.abstractmethod
    def summary_per_term(self) -> pandas.DataFrame:
        """
        Generates a dataframe with per-term summary statistics with indices 'model' and 'term' and columns 'ref_level'
        and 'pvalue_lr'.
        """

    @property
    @abc.abstractmethod
    def summary_per_level(self) -> pandas.DataFrame:
        """
        Generates a dataframe with per-level summary statistics with indices 'model', 'term', 'term_level' and 'level'
        and columns returned by `lifelines.CoxPHFitter.summary` and 'n_total'.
        """

    @abc.abstractmethod
    def get_per_model_for_summary_helper(
        self, per_model: pandas.DataFrame, per_term: pandas.DataFrame
    ) -> pandas.DataFrame:
        """
        Adjust the per-model data summary to allow joining with per-term summaries.
        :param per_model: dataframe with per-model summary;
        :param per_term: dataframe with per-term summary;
        :return: per-model summary, adjusted to allow joining with per-term summaries.
        """

    @property
    def summary(self) -> pandas.DataFrame:
        """
        Combines summaries per-model, per-term and per-level into a single dataframe with a layout suitable for printing.
        """
        per_level = self.summary_per_level.rename(
            columns={
                "exp(coef)": "hr",
                "exp(coef) lower 95%": "hr_lower_ci",
                "exp(coef) upper 95%": "hr_upper_ci",
                "n_total": "per_level_n_total",
            }
        )[["per_level_n_total", "hr", "hr_lower_ci", "hr_upper_ci"]]
        per_level["per_level_n_total"] = per_level.per_level_n_total.astype("Int64")

        per_term = self.summary_per_term.rename(columns={"pvalue_lr": "per_term_p"})
        # Add a level index so per-level data is only joined with a single row without level in per_level
        per_term["level"] = pandas.Categorical(
            [None] * per_term.shape[0],
            per_level.index.get_level_values("level").categories,
        )
        per_term.set_index("level", append=True, inplace=True)

        per_model = self.summary_per_model.rename(
            columns={
                "pvalue_lr": "per_model_p",
                "c_index": "per_model_c_index",
                "n_total": "per_model_n_total",
                "n_events": "per_model_n_events",
            }
        )
        per_model["per_model_n_total"] = per_model.per_model_n_total.astype("Int64")
        per_model["per_model_n_events"] = per_model.per_model_n_events.astype("Int64")
        per_model = self.get_per_model_for_summary_helper(per_model, per_term)

        # Join the different parts together
        j1 = per_level.join(
            per_term.ref_level.reset_index("level", drop=True), how="outer"
        )

        j2 = per_term[["per_term_p"]].join(per_model, how="outer")

        # Convert j1 and j2 indexes back to categorical before joining together
        j1.reset_index(["model", "term"], inplace=True)
        j1["model"] = pandas.Categorical(
            j1.model, per_model.index.get_level_values("model").categories
        )
        j1["term"] = pandas.Categorical(j1.term, self.variables_)
        j1.set_index(["model", "term"], append=True, inplace=True)
        j1.set_index(
            j1.index.reorder_levels(["model", "term", "term_level", "level"]),
            inplace=True,
        )

        j2.reset_index(inplace=True)
        j2["model"] = pandas.Categorical(
            j2.model, per_model.index.get_level_values("model").categories
        )
        j2["term"] = pandas.Categorical(j2.term, self.variables_)
        j2["level"] = pandas.Categorical(
            j2.level, per_term.index.get_level_values("level").categories
        )
        j2.set_index(["model", "term", "level"], inplace=True)

        ret = j1.join(j2, how="outer")

        # Redo index to make all levels categorical, with categories in right order.
        ret.set_index(
            pandas.MultiIndex.from_frame(
                pandas.DataFrame(
                    {
                        "model": pandas.Categorical(
                            ret.index.get_level_values("model"),
                            per_model.index.get_level_values("model").categories,
                        ),
                        "term": pandas.Categorical(
                            ret.index.get_level_values("term"), self.variables_
                        ),
                        "term_level": pandas.Categorical(
                            ret.index.get_level_values("term_level"),
                            ret.index.get_level_values(
                                "term_level"
                            ).categories.set_names(None),
                        ),
                        "ref_level": pandas.Categorical(ret.ref_level),
                        "level": pandas.Categorical(
                            ret.index.get_level_values("level")
                        ),
                    }
                )
            ),
            inplace=True,
        )
        ret.drop(columns=["ref_level"], inplace=True)

        ret.sort_index(na_position="first", inplace=True)

        return ret


class UnivariateCoxPerVariable(AbstractCoxModels):
    """Utility class to fit a univariate Cox regression model for each variable.
    """

    def fit(
        self,
        df: pandas.DataFrame,
        duration_col: str,
        event_col: str,
        drop_na: bool = False,
        step_size: float = None,
    ) -> "UnivariateCoxPerVariable":
        self.variables_ = [c for c in df.columns if c not in {duration_col, event_col}]
        self.df_ = df.copy()
        if drop_na:
            self.df_.dropna(subset=[duration_col, event_col], inplace=True)
        self.cox_ = {}
        for v in self.variables_:
            cph = lifelines.CoxPHFitter()
            col = df[v]
            if drop_na:
                col = col.dropna()
            else:
                if col.isna().any():
                    raise ValueError("Column {} has missing data".format(v))

            sub_df = self.df_.loc[:, [duration_col, event_col]].join(
                pandas.get_dummies(col.to_frame(), drop_first=True), how="inner"
            )

            cph.fit(
                sub_df,
                duration_col=duration_col,
                event_col=event_col,
                step_size=step_size,
            )
            self.cox_[v] = cph
        return self

    @property
    def summary_per_model(self) -> pandas.DataFrame:
        ret = pandas.DataFrame(
            [
                {
                    "pvalue_lr": cph.log_likelihood_ratio_test().p_value,
                    "c_index": cph.concordance_index_,
                    "n_total": cph._n_examples,
                    "n_events": cph.event_observed.sum(),
                }
                for cph in self.cox_.values()
            ]
        )

        ret["model"] = pandas.Categorical(self.variables_, self.variables_)
        ret.set_index("model", inplace=True)

        return ret

    @property
    def summary_per_term(self) -> pandas.DataFrame:
        ret = pandas.DataFrame(
            [
                {
                    "ref_level": self.get_reference_level(term),
                    "pvalue_lr": cph.log_likelihood_ratio_test().p_value,
                }
                for (term, cph) in self.cox_.items()
            ]
        )
        ret["model"] = pandas.Categorical(self.variables_, self.variables_)
        ret["term"] = ret["model"]
        ret.set_index(["model", "term"], inplace=True)

        return ret

    @property
    def summary_per_level(self) -> pandas.DataFrame:
        ret = pandas.concat([cph.summary for cph in self.cox_.values()])
        ret = ret.join(self.get_term_levels())
        ret["model"] = ret["term"]
        ret.set_index(["model", "term", "term_level", "level"], inplace=True)
        ret.loc[:, "n_total"] = ret.index.to_frame().apply(
            lambda x: self.get_n_per_level(x.term, x.level), axis="columns"
        )

        return ret

    def get_per_model_for_summary_helper(
        self, per_model: pandas.DataFrame, per_term: pandas.DataFrame
    ) -> pandas.DataFrame:
        return per_model

    def to_html(
        self,
        filename: typing.Union[str, pathlib.Path, typing.TextIO],
        uuid: typing.Union[str, None] = None,
    ):
        """Export the Cox regression model summary to html.
        :param filename: file to write html to;
        :param uuid: If set, override the uuid of the html table.
        """
        data = self.summary_per_level[
            ["exp(coef)", "exp(coef) lower 95%", "exp(coef) upper 95%", "p"]
        ]
        data.rename(
            columns={
                "exp(coef)": "HR",
                "exp(coef) lower 95%": "2.5% CI",
                "exp(coef) upper 95%": "97.5% CI",
                "p": "P-value",
            },
            inplace=True,
        )
        data.loc[:, "HR (95% CI)"] = data.apply(
            lambda x: HazardRatioCI(
                x["HR"],
                x["2.5% CI"],
                x["97.5% CI"],
                vmin=data["2.5% CI"].min(),
                vmax=data["97.5% CI"].max(),
            ),
            axis=1,
        )
        style = data.style
        style.set_table_styles(
            [
                {"selector": "th.row_heading", "props": [("text-align", "left")]},
                {"selector": "th.col_heading", "props": [("text-align", "center")]},
                {"selector": "td", "props": [("text-align", "right")]},
                {"selector": "tr:nth-child(even)", "props": [("background", "#CCC")]},
                {"selector": "tr:nth-child(odd)", "props": [("background", "#FFF")]},
            ]
        )
        style.format(py_utils.format_pvalue.format_pvalue, subset=["P-value"])
        style.format("{:.2f}", subset=["HR", "2.5% CI", "97.5% CI"])
        style.format(lambda hr: hr.to_svg(), subset=["HR (95% CI)"])

        if uuid is None:
            html = style.render()
        else:
            html = style.render(uuid=uuid)
        if isinstance(filename, (os.PathLike, str)):
            with open(filename, "w") as fh:
                fh.write(html)
        else:
            filename.write(html)

    def plot(
        self,
        columns: typing.Iterable[str] = None,
        ax: matplotlib.axes.Axes = None,
        reorder: bool = False,
    ) -> None:
        """
        Plot hazard ratio with corresponding 95% confidence interval for each univariate Cox model.
        :param columns: if given, include only univariate Cox regression models for the given columns in the original
                        dataframe and in the given order. By default, include univariate Cox regression models for all
                        the columns in the original dataframe;
        :param ax: axes to plot into or None to plot in a new figure;
        :param reorder: if enabled, show univariate Cox regression models sorted by hazard ratio.
        """
        ax = ax or matplotlib.pyplot.figure().add_subplot(111)

        if columns is None:
            summary_per_level = self.summary_per_level
        else:
            summary_per_level = self.summary_per_level.loc[columns]

        if reorder:
            summary_per_level.sort_values(by="exp(coef)", ascending=False, inplace=True)

        xerr = pandas.concat(
            [
                summary_per_level["exp(coef)"]
                - summary_per_level["exp(coef) lower 95%"],
                summary_per_level["exp(coef) upper 95%"]
                - summary_per_level["exp(coef)"],
            ],
            axis=1,
        )
        ax.errorbar(
            summary_per_level["exp(coef)"],
            range(summary_per_level.shape[0]),
            xerr=xerr.T.values,
            fmt="o",
            markersize=5,
            color="k",
        )
        ax.vlines(
            1,
            -0.5,
            summary_per_level.shape[0] - 0.5,
            color="k",
            linestyle=":",
            linewidth=1,
        )

        ax.set_yticks(range(summary_per_level.shape[0]))
        ax.set_yticklabels(summary_per_level.index.get_level_values("term_level"))
        ax.set_ylim(-0.5, summary_per_level.shape[0] - 0.5)
        ax.set_xlabel("HR (95% CI)")
        ax.set_ylabel("Variables")
        ax.set_xscale("log")

        return ax


class MultivariateCox(AbstractCoxModels):
    """Utility class to fit a multivariate Cox regression model. Wrapper class around lifelines class for consistent
     interface with UnivariateCoxPerVariable.
    """

    def fit(
        self,
        df: pandas.DataFrame,
        duration_col: str,
        event_col: str,
        drop_na: bool = False,
        step_size: float = None,
    ) -> "MultivariateCox":
        self.variables_ = [c for c in df.columns if c not in {duration_col, event_col}]
        self.df_ = df.copy()
        self.cox_ = lifelines.CoxPHFitter()

        if drop_na:
            self.df_.dropna(inplace=True)
        else:
            if self.df_.isna().any().any():
                raise ValueError("Missing data in input")

        self.cox_.fit(
            pandas.get_dummies(self.df_, drop_first=True),
            duration_col=duration_col,
            event_col=event_col,
            step_size=step_size,
        )

        self.cox_without_var = {}
        for v in self.variables_:
            self.cox_without_var[v] = lifelines.CoxPHFitter().fit(
                pandas.get_dummies(self.df_.drop(columns=[v]), drop_first=True),
                duration_col=duration_col,
                event_col=event_col,
                step_size=step_size,
            )

        return self

    @property
    def summary_per_model(self) -> pandas.DataFrame:
        ret = pandas.DataFrame(
            {
                "pvalue_lr": self.cox_.log_likelihood_ratio_test().p_value,
                "c_index": self.cox_.concordance_index_,
                "n_total": self.cox_._n_examples,
                "n_events": self.cox_.event_observed.sum(),
            },
            index=pandas.Categorical(["multi"]),
        )
        ret.index.name = "model"

        return ret

    @property
    def summary_per_term(self) -> pandas.DataFrame:
        ret = pandas.DataFrame(
            [
                {
                    "ref_level": self.get_reference_level(term),
                    "pvalue_lr": py_utils.compare_lr_test.compare_lr_test(
                        self.cox_, cph
                    )[1],
                }
                for (term, cph) in self.cox_without_var.items()
            ],
            index=pandas.CategoricalIndex(self.variables_, self.variables_),
        )
        ret.index.names = ["term"]
        ret["model"] = pandas.Categorical(["multi"] * ret.shape[0])
        ret.set_index(["model"], append=True, inplace=True)
        ret.set_index(ret.index.reorder_levels(["model", "term"]), inplace=True)

        return ret

    @property
    def summary_per_level(self) -> pandas.DataFrame:
        ret = self.cox_.summary.join(self.get_term_levels())
        ret["model"] = pandas.Categorical(["multi"] * ret.shape[0])
        ret.set_index(["model", "term", "term_level", "level"], inplace=True)
        ret.loc[:, "n_total"] = ret.index.to_frame().apply(
            lambda x: self.get_n_per_level(x.term, x.level), axis="columns"
        )

        return ret

    def get_per_model_for_summary_helper(
        self, per_model: pandas.DataFrame, per_term: pandas.DataFrame
    ) -> pandas.DataFrame:
        per_model["term"] = pandas.Categorical(
            [None], per_term.index.get_level_values("term").categories
        )
        per_model.set_index("term", append=True, inplace=True)

        return per_model


@dataclasses.dataclass(frozen=True)
class Endpoint:
    """Class representing clinical endpoint (overall survival, disease specific survival or disease free survival) for
    Cox regression models."""

    duration_col: str
    event_col: str
    fancy_name: str


@dataclasses.dataclass(frozen=True)
class MultivariateModel:
    """Class representing a multi-variate Cox regression model and the variables included in the model."""

    variables: typing.Tuple[str]
    fancy_name: str


class CoxSummaryTable:
    """Class to create summary tables from multiple uni- and multi-variate Cox regression models for multiple endpoints."""

    def __init__(self, data: pandas.DataFrame):
        """
        Construct CoxSummaryTable.
        :param data: dataframe with data to fit models to.
        """
        self.data = data
        self.variables = self.data.columns.to_list()
        self.endpoints = []  # List of Endpoint instances
        self.multivariate_models = []  # List of MultivariateModel instances
        self.fitted_univariate_models = {}  # Map Endpoint to UnivariateCoxPerVariable
        self.fitted_multivariate_models = (
            {}
        )  # Map (Endpoint, MultivariateModel) to MultivariateCox

    def add_endpoint(self, duration_col: str, event_col: str, fancy_name: str) -> None:
        """
        Add an endpoint to fit Cox model(s) to.
        :param duration_col: name of column in 'data' passed to the constructor that contains the event/censoring time;
        :param event_col: name of the column in 'data' passed to the constructor that contains the event information
                         (`True` if the subject had an event, `False` if no event was observed and the subject was
                         censored);
        :param fancy_name: name to use for endpoint in output.
        """
        self.variables.remove(duration_col)
        self.variables.remove(event_col)
        self.endpoints.append(Endpoint(duration_col, event_col, fancy_name))

    def add_multivariate_model(self, variables: typing.List, fancy_name: str):
        """
        Add a multivariate Cox regression model.
        :param variables: name of column(s) in 'data' passed to the constructor to include in multivariate model;
        :param fancy_name: name to use for model in output.
        """
        self.multivariate_models.append(MultivariateModel(tuple(variables), fancy_name))

    def fit(self, drop_na: bool = False, step_size: float = None) -> None:
        """Fit all Cox regression model(s) to the data.
        For each added endpoint, fit:
         - a univariate model for each variable included in the `data` passed to the contructor;
         - each of the added multivariate models.
         Every column in the 'data' passed to the contructor is considered a variable if it is not used as
         `duration_col` or `event_col` for any of the endpoints.
        :param drop_na: whether to drop rows with NaNs. If `True` drop rows with NaNs, if `False` the fitting will fail
               and raise an exception if NaNs are present;
        :param step_size: initial step size for the fitting algorithm to be passed onto lifelines.CoxPHFitter;
        """
        for endpoint in self.endpoints:
            self.fitted_univariate_models[endpoint] = UnivariateCoxPerVariable().fit(
                self.data[self.variables + [endpoint.duration_col, endpoint.event_col]],
                endpoint.duration_col,
                endpoint.event_col,
                drop_na=drop_na,
                step_size=step_size,
            )
            for multivariate_model in self.multivariate_models:
                self.fitted_multivariate_models[
                    (endpoint, multivariate_model)
                ] = MultivariateCox().fit(
                    self.data[
                        list(multivariate_model.variables)
                        + [endpoint.duration_col, endpoint.event_col]
                    ],
                    endpoint.duration_col,
                    endpoint.event_col,
                    drop_na=drop_na,
                    step_size=step_size,
                )

    @property
    def summary(self) -> pandas.DataFrame:
        """
        Generate summary dataframe with statistics from all fitted Cox regression models.
        :return: dataframe with indices 'endpoint', 'model_type', 'model', 'term', 'term_level', 'ref_level' and 'level'
                 and columns 'per_level_n_total', 'hr', 'hr_lower_ci', 'hr_upper_ci', 'per_term_p', 'per_model_p',
                 'per_model_c_index', 'per_model_n_total' and 'per_model_n_events'.
        """

        def non_categorical_index(df: pandas.DataFrame) -> pandas.DataFrame:
            """Convert dataframe indices to non-categorical types."""
            return df.set_index(
                pandas.MultiIndex.from_tuples(
                    df.index.to_native_types(na_rep=None), names=df.index.names
                )
            )

        uni = pandas.concat(
            [
                non_categorical_index(cox.summary)
                for cox in self.fitted_univariate_models.values()
            ],
            names=["endpoint", "model_type"],
            keys=[
                (e.fancy_name, "univariate")
                for e in self.fitted_univariate_models.keys()
            ],
        )

        if self.multivariate_models:
            multi = pandas.concat(
                [
                    non_categorical_index(cox.summary)
                    for cox in self.fitted_multivariate_models.values()
                ],
                names=["endpoint", "model_type"],
                keys=[
                    (e.fancy_name, m.fancy_name)
                    for (e, m) in self.fitted_multivariate_models.keys()
                ],
            )
            ret = pandas.concat([uni, multi])
        else:
            ret = uni

        # Convert indexes back to categorical types:
        ret.set_index(
            pandas.MultiIndex.from_frame(
                ret.index.to_frame().apply(
                    lambda x: pandas.Categorical(x, pandas.Series(x).dropna().unique())
                )
            ),
            inplace=True,
        )

        ret.sort_index(level="endpoint", sort_remaining=False, inplace=True)

        return ret

    def to_html(
        self,
        filename: typing.Union[str, pathlib.Path, typing.TextIO],
        uuid: typing.Union[str, None] = None,
    ):
        """Export the Cox regression model summary to html.
        :param filename: file to write html to;
        :param uuid: If set, override the uuid of the html table.
        """
        data = self.summary
        data.loc[:, "HR (95% CI)"] = data.apply(
            lambda x: HazardRatioCI(
                x["hr"],
                x["hr_lower_ci"],
                x["hr_upper_ci"],
                vmin=data["hr_lower_ci"].min(),
                vmax=data["hr_upper_ci"].max(),
            ),
            axis=1,
        )

        data = data[
            [
                "HR (95% CI)",
                "hr",
                "hr_lower_ci",
                "hr_upper_ci",
                "per_term_p",
                "per_model_p",
                "per_model_c_index",
                "per_model_n_total",
            ]
        ]
        data.rename(
            columns={
                "hr": "HR",
                "hr_lower_ci": "2.5% CI",
                "hr_upper_ci": "97.5% CI",
                "per_term_p": "Per-term P-value",
                "per_model_p": "Per-model P-value",
                "per_model_c_index": "C-index",
                "per_model_n_total": "N",
            },
            inplace=True,
        )

        data = data.unstack("endpoint")
        data.columns = data.columns.swaplevel()
        data = data.sort_index(axis=1, level="endpoint", sort_remaining=False)
        data.reset_index(["model", "term_level"], drop=True, inplace=True)

        # Change column indices to not be categorical:
        data.columns = pandas.MultiIndex.from_tuples(data.columns.to_flat_index())

        # Replace NaN with empty strings in indices:
        data = (
            data.assign(
                term=data.index.get_level_values("term")
                .add_categories([""])
                .fillna(""),
                ref_level=data.index.get_level_values("ref_level")
                .add_categories([""])
                .fillna(""),
                level=data.index.get_level_values("level")
                .add_categories([""])
                .fillna(""),
            )
            .reset_index(["term", "ref_level", "level"], drop=True)
            .set_index(["term", "ref_level", "level"], append=True)
        )

        # Prettify:
        data.columns.names = ["", ""]
        data.index.names = [x.replace("_", " ").capitalize() for x in data.index.names]

        style = data.style
        style.set_table_styles(
            [
                {"selector": "th.row_heading", "props": [("text-align", "left")]},
                {"selector": "th.col_heading", "props": [("text-align", "center")]},
                {"selector": "td", "props": [("text-align", "right")]},
                {
                    "selector": "tr:nth-child(even)",
                    "props": [("background", "#F6F6F6")],
                },
                {"selector": "tr:nth-child(odd)", "props": [("background", "#FFF")]},
                {
                    "selector": "th.col_heading",
                    "props": [("width", "5em"), ("white-space", "normal")],
                },
                {
                    "selector": "th.col_heading",
                    "props": [("border-bottom", "2px black solid")],
                },
                {
                    "selector": "th.index_name",
                    "props": [("border-bottom", "2px black solid")],
                },
                {
                    "selector": "*",
                    "props": [("font-size", "6pt"), ("font-family", "Arial")],
                },
                {
                    "selector": "*.col0, *.col8",
                    "props": [
                        ("padding-left", "4px"),
                        ("border-left", "4px white solid"),
                    ],
                },
            ]
        )
        for e in self.endpoints:
            style.format(
                lambda x: ""
                if pandas.isna(x)
                else py_utils.format_pvalue.format_pvalue(x),
                subset=[
                    (e.fancy_name, v) for v in ["Per-term P-value", "Per-model P-value"]
                ],
            )
            style.format(
                lambda x: "" if pandas.isna(x) else "{:.2f}".format(x),
                subset=[
                    (e.fancy_name, v) for v in ["HR", "2.5% CI", "97.5% CI", "C-index"]
                ],
            )
            style.format(
                lambda x: "" if pandas.isna(x) else "{:d}".format(x),
                subset=[(e.fancy_name, "N")],
            )
            style.format(lambda hr: hr.to_svg(), subset=[(e.fancy_name, "HR (95% CI)")])

        if uuid is None:
            html = style.render()
        else:
            html = style.render(uuid=uuid)
        if isinstance(filename, (os.PathLike, str)):
            with open(filename, "w") as fh:
                fh.write(html)
        else:
            filename.write(html)
