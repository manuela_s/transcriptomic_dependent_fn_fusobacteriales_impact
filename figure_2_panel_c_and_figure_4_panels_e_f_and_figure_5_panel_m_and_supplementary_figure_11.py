#!/usr/bin/env python3

""" Generate figure 2 panel c, figure 4 panels e-f, figure 5 panel m and supplementary figure 11.
Association between higher resolution taxonomic ranks (including Fusobacterium nucleatum) with transcriptomics-based
molecular subtyping and clinical outcome."""

import pathlib

import matplotlib
import matplotlib.cm
import matplotlib.colors
import matplotlib.pyplot
import pandas
import seaborn

import cohort
import cox_interaction
import fusobacteria_utils
import py_utils.add_n_in_group_names
import py_utils.cox

DST_DIR = pathlib.Path("figures_and_tables")


def figure_2_panel_c(data: pandas.DataFrame) -> None:
    """
    Generate Figure 2 panel C.
    Plot fraction of Fusobacterium nucleatum (Fn) to total Fusobacteriales relative abundance for patients of the
    TCGA-COAD-READ cohort.
    :param data: dataframe, one row per patient and multi-index columns with relative abundance for taxonomic ranks
           of increasing resolution ('order', 'family', 'genus' and 'species'), as returned by `fusobacteria_utils.get_relative_abundance_at_multiple_resolution_taxonomic_ranks()`.
    """
    # Compute Fn to Fusobacteriales fraction
    fn = data.droplevel(["family", "genus", "level", "label"], axis=1)[
        "Fusobacterium_nucleatum"
    ]
    fusobacteriales = data.droplevel(["family", "genus", "species", "label"], axis=1)[
        "order"
    ]
    plot_data = (fn / fusobacteriales).to_frame("fraction_fn_to_total")

    # Sort patients by Fusobacteriales relative abundance to match patients order in figure 2 panel D-1.
    plot_data = plot_data.join(fusobacteriales).sort_values("order")

    fig, axes = matplotlib.pyplot.subplots(
        ncols=2,
        sharey=True,
        gridspec_kw=dict(
            top=0.985,
            bottom=0.085,
            left=0.105,
            right=0.995,
            wspace=0.04,
            width_ratios=(0.8, 0.2),
        ),
        figsize=(7, 3),
    )
    ax_species, ax_dist = (
        axes
    )  # corresponding to left (1) and right (2) hand side in figure 2 panel C

    # Figure 2 panel C-1
    plot_data.fraction_fn_to_total.plot(
        kind="bar", width=1, color=["k"], edgecolor=None, ax=ax_species
    )
    ax_species.set_xlim(-0.5, plot_data.shape[0] - 0.5)
    ax_species.set_xticklabels([])
    ax_species.xaxis.set_ticks_position("none")
    ax_species.set_xlabel(f"Patients (n={plot_data.shape[0]})")
    ax_species.set_ylim(0, 1.01)
    ax_species.set_ylabel(
        "$Fusobacterium$ $nucleautum$\nfraction of total $Fusobacteriales$"
    )

    # Figure 2 panel C-2
    seaborn.violinplot(
        y="fraction_fn_to_total",
        inner="quartile",
        color=".8",
        cut=0,
        data=plot_data,
        ax=ax_dist,
    )
    seaborn.swarmplot(
        y="fraction_fn_to_total",
        data=plot_data,
        s=5,
        color="k",
        linewidth=0,
        alpha=1,
        ax=ax_dist,
    )
    ax_dist.set_ylabel("")
    ax_dist.set_ylim(0, 1.01)

    fig.savefig(DST_DIR / "figure_2_panel_c.pdf")


def figure_4_panels_e_and_f(
    data: pandas.DataFrame, grouping_var: str, panel_label: str
) -> None:
    """
    Generate figure 4 panels E-F.
    Plot figure panels with elative abundance (to total bacteria) of *Fusobacteriales* at the family, genus and species
    taxonomic ranks grouped by 'grouping_var'.
    :param data: dataframe, as returned by `fusobacteria_utils.get_relative_abundance_at_multiple_resolution_taxonomic_ranks()`, but with additional indices 'cms_class',
                 'cris_class' and 'is_mesenchymal';
    :param grouping_var: name of covariate in index to group by, one of 'cms_class', 'cris_class', 'is_mesenchymal';
    :param grouping_var: name for labelling figure panel.
    """
    data = py_utils.add_n_in_group_names.add_n_in_group_names(data, grouping_var)
    plot_data = data.groupby(grouping_var).mean().T
    plot_data.reset_index(["family", "genus", "species"], drop=True, inplace=True)
    plot_data.set_index(
        pandas.Series(range(plot_data.shape[0]), name="id"), append=True, inplace=True
    )
    plot_data.columns = plot_data.columns.str.replace(" ", "\n")

    plot_data_family = (
        plot_data.query('level=="family"').reset_index(["level", "label"], drop=True).T
    )
    plot_data_genus = (
        plot_data.query('level=="genus"').reset_index(["level", "label"], drop=True).T
    )
    plot_data_species = (
        plot_data.query('level=="species"').reset_index(["level", "label"], drop=True).T
    )

    family_colors = ["#a50f15", "#08306b"]
    species_colors = (
        matplotlib.cm.Reds._resample(7)(range(1, 7)).tolist()
        + [matplotlib.colors.to_rgba("lightgray")]
        + matplotlib.cm.Blues._resample(3)(range(1, 3)).tolist()
        + [matplotlib.colors.to_rgba("darkgray")]
    )

    fig, ax = matplotlib.pyplot.subplots(figsize=(7, 4))
    plot_params = dict(kind="barh", stacked=True, ax=ax, legend=True, edgecolor="k")
    plot_data_family.plot(
        width=-0.45, align="edge", color=family_colors, hatch="/", **plot_params
    )
    plot_data_species.plot(
        width=0.45, align="edge", color=species_colors, **plot_params
    )
    plot_data_genus.plot(
        width=0.3,
        color=["#ef3b2c", "lightgray", "#08519c", "darkgray"],
        hatch="\\",
        **plot_params,
    )
    ax.invert_yaxis()
    ax.set_ylim(bottom=plot_data.shape[1] - 0.5, top=-0.5)
    ax.set_xlabel("Relative abundance [% of total bacteria]")
    ax.set_ylabel(f"{grouping_var.replace('_class', '').upper()} subtyping")

    handles, labels = ax.get_legend_handles_labels()
    legend_data = (
        plot_data[[]]
        .join(
            pandas.Series(
                handles,
                name="handle",
                index=pandas.Series(labels, name="id").astype(int),
            ),
            how="inner",
        )
        .reset_index(["label", "level"])
        .sort_index()
    )
    ax.legend(
        legend_data.handle,
        legend_data.label.str[1:].str.replace(" ", "   "),
        title="Family, Genus, Species",
        bbox_to_anchor=(1.05, 1),
        loc="upper left",
        borderaxespad=0.0,
    )

    matplotlib.pyplot.tight_layout()

    fig.savefig(DST_DIR / f"figure_4_panel_{panel_label}.pdf")


def figure_5_panel_m_and_supplementary_figure_11(
    data: pandas.DataFrame, cli: pandas.DataFrame, survival_type: str, figure_label: str
) -> None:
    """
    Generate Figure 5 panel M and Supplementary Figure 11.
    Plot figure panel with Cox regression models fitted on bacterium relative abundance reported at the order, family,
    genus and species taxonomic ranks. For each taxonomic rank, patients were classified as low or high abundance using
    the corresponding 75th percentile relative abundance as cut-off. Univariate Cox regression models were fitted when
    evaluating association between pathogen prevalence (high vs. low; reference low) at each taxonomic rank and OS in
    the whole unselected patient population (left panel). Cox regression models with an interaction term between
    pathogen prevalence (high vs. low; reference low) and mesenchymal status (mesenchymal, i.e. either CMS4 and/or
    CRIS-B, vs. non-mesenchymal, i. e. neither CMS4 nor CRIS-B) at each taxonomic rank and OS were fitted to evaluate
    differential impact of bacterium on clinical outcome by tumour biology (right panels).
    :param data: dataframe, as returned by `fusobacteria_utils.get_relative_abundance_at_multiple_resolution_taxonomic_ranks()`, but with additional indices 'cms_class',
           'cris_class' and 'is_mesenchymal';
    :param cli: dataframe, as returned by `cohort.get_tcga_crc_cohort().get_clinical()`;
    :param survival_type: name of clinical survival endpoint, one of 'os', 'dss' and 'dfs';
    :param survival_type: name to label figure.
    """
    binarized = data.apply(lambda x: fusobacteria_utils.bin_fusobacteria(x))

    survival_time = "{}_months".format(survival_type)
    survival_event = "{}_event".format(survival_type)
    with_survival = binarized.join(
        cli.reset_index("cohort", drop=True)
        .dropna(subset=[survival_time, survival_event])
        .set_index([survival_time, survival_event], append=True)[[]],
        how="inner",
    )
    with_survival.columns = [
        "_".join(str(y).strip() for y in x) for x in with_survival.columns
    ]

    cox_all = py_utils.cox.UnivariateCoxPerVariable()
    cox_all.fit(
        with_survival.replace({"low": 0, "high": 1}).reset_index(
            [survival_time, survival_event]
        ),
        survival_time,
        survival_event,
    )

    interaction_df = with_survival.reset_index(
        [survival_time, survival_event, "is_mesenchymal"]
    ).dropna()
    interactions = cox_interaction.InteractionCoxModels(
        interaction_df, survival_time, survival_event, "is_mesenchymal"
    )

    fig, axes = matplotlib.pyplot.subplots(
        ncols=3, sharex=True, sharey=True, figsize=(7, 5)
    )
    cox_all.plot(ax=axes[0])
    interactions.plot("no", ax=axes[1])
    interactions.plot("yes", ax=axes[2])

    axes[0].set_xlim(0.1, 10)
    axes[0].set_title(f"Unselected\npatients\n(n={with_survival.shape[0]})")
    axes[1].set_title(
        f'Neither CMS4 nor\nCRIS-B patients\n(n={interaction_df.is_mesenchymal.value_counts()["no"]})'
    )
    axes[2].set_title(
        f'Either CMS4 and/or\nCRIS-B patients\n(n={interaction_df.is_mesenchymal.value_counts()["yes"]})'
    )

    axes[0].invert_yaxis()
    axes[0].set_ylabel("Taxonomic rank")

    axes[0].set_xlabel("")
    axes[2].set_xlabel("")

    axes[0].yaxis.tick_right()
    axes[1].yaxis.tick_right()
    axes[2].yaxis.tick_right()
    axes[0].yaxis.set_tick_params(which="both", labelleft=False, labelright=False)
    axes[1].yaxis.set_tick_params(which="both", labelleft=False, labelright=False)
    axes[2].yaxis.set_tick_params(which="both", labelleft=False, labelright=True)
    axes[2].set_yticklabels(
        data.columns.get_level_values("label").str.replace(" ", "--")
    )

    matplotlib.pyplot.tight_layout()

    fig.savefig(DST_DIR / f"{figure_label}_{survival_type}.pdf")


def main():
    cli = cohort.get_tcga_crc_cohort().get_clinical()
    combined = (
        fusobacteria_utils.get_relative_abundance_at_multiple_resolution_taxonomic_ranks()
    )
    subtypes = cohort.get_tcga_crc_cohort().get_subtypes()
    with_subtypes = combined.join(
        subtypes.reset_index("cohort", drop=True).set_index(
            subtypes.columns.to_list(), append=True
        )
    )

    figure_2_panel_c(combined)

    figure_4_panels_e_and_f(with_subtypes, "cms_class", "e")
    figure_4_panels_e_and_f(with_subtypes, "cris_class", "f")

    figure_5_panel_m_and_supplementary_figure_11(
        with_subtypes, cli, "os", "supplementary_figure_11"
    )
    figure_5_panel_m_and_supplementary_figure_11(
        with_subtypes, cli, "dss", "figure_5_panel_m"
    )
    figure_5_panel_m_and_supplementary_figure_11(
        with_subtypes, cli, "dfs", "figure_5_panel_m"
    )


if __name__ == "__main__":
    pathlib.Path(DST_DIR).mkdir(parents=True, exist_ok=True)

    matplotlib.rcParams["pdf.fonttype"] = 42
    matplotlib.rcParams["font.family"] = "Arial"
    matplotlib.rcParams["font.size"] = 8

    main()
