#!/usr/bin/env python3

""" Generate figure 1 panels c-d.
In vitro experiments in colorectal cell lines."""

import pathlib

import matplotlib.pyplot
import pandas
import seaborn

import constants

DST_DIR = pathlib.Path("figures_and_tables")


def get_data() -> pandas.DataFrame:
    """
    Load experimental data in HCT116 and HT29 colorectal cell lines.
    :return: dataframe,
        - one row per experimental value;
        - indices 'presentation_figure_panel', 'manuscript_figure_panel', 'cell_line', 'assay', 'technique', 'units',
        'treatment', 'replica';
        - column 'value'.
    """
    data = pandas.read_csv(
        pathlib.Path("data", "in_vitro_figure_1.csv"), index_col=list(range(6))
    )
    data = data.rename_axis(columns=["replica"]).stack().to_frame("value")

    return data


def plot_helper(
    plot_data: pandas.DataFrame, ylabel: str, figure_label: str, ylog_scale: bool = True
) -> None:
    """
    Helper function to plot experimental data and save corresponding figure as pdf.
    :param plot_data: dataframe including subset of data to plot;
    :param ylabel: label to use as y-axis label;
    :param figure_label: label to use in naming figure;
    :param ylog_scale: whether expressing y-axis scale as log. Default to 'True';
    """
    fig, ax = matplotlib.pyplot.subplots(
        figsize=(2.5, 2), gridspec_kw=dict(top=0.90, bottom=0.23, left=0.31, right=0.87)
    )
    params = dict(x="fn_moi", y="value", data=plot_data)
    l = seaborn.lineplot(markers=True, color="k", ci=None, **params)
    s = seaborn.scatterplot(
        hue="fn_treatment",
        palette=constants.FUSOBACTERIA_BIN_COLORS.values(),
        s=50,
        alpha=0.8,
        **params,
    )

    ax.set_xscale("symlog", linthresh=1, subs=[2, 3, 4, 5, 6, 7, 8, 9])
    if ylog_scale:
        ax.set_yscale("log")

    title, = (plot_data.cell_line + " - " + plot_data.technique).unique()
    ax.set_title(title)
    ax.set_xlabel("$Fn$ MOI")
    ax.set_ylabel(ylabel)

    ax.legend_.remove()

    fig.savefig(DST_DIR / f"{figure_label}.pdf")


def figure_1_panel_c(data: pandas.DataFrame) -> None:
    """
    Generate figure 1 panel c.
    Plot NFkB data.
    :param data: dataframe including NFkB data to plot.
    """
    plot_data = data.copy().query(
        'manuscript_figure_panel=="figure_1C" and assay=="NFKB_activity"'
    )
    plot_data["fn_moi"] = plot_data.index.get_level_values("treatment").map(
        {"Control": 0, "MOI 10:1": 10, "MOI 100:1": 100, "MOI 1000:1": 1000}
    )
    plot_data["fn_treatment"] = plot_data.index.get_level_values("treatment").map(
        {"Control": "no Fn", "MOI 10:1": "Fn", "MOI 100:1": "Fn", "MOI 1000:1": "Fn"}
    )

    plot_helper(
        plot_data.reset_index(),
        "NF$k$B activity\n[RLU]",
        "figure_1_panel_c",
        ylog_scale=False,
    )


def figure_1_panel_d(data: pandas.DataFrame) -> None:
    """
    Generate figure 1 panel d.
    Plot TNFa expression data.
    :param data: dataframe including TNFa data to plot.
    """
    plot_data = data.copy().query(
        'manuscript_figure_panel=="figure_1D" and assay=="TNFa"'
    )
    plot_data["fn_moi"] = plot_data.index.get_level_values("treatment").map(
        {"Control": 0, "MOI 100:1": 100, "MOI 1000:1": 1000}
    )
    plot_data["fn_treatment"] = plot_data.index.get_level_values("treatment").map(
        {"Control": "no Fn", "MOI 100:1": "Fn", "MOI 1000:1": "Fn"}
    )

    plot_helper(
        plot_data.reset_index(),
        "TNF$a$ mRNA expression\n[fold-change to $B$-tubulin]",
        "figure_1_panel_d",
    )


def main():
    data = get_data()

    figure_1_panel_c(data)
    figure_1_panel_d(data)


if __name__ == "__main__":
    matplotlib.rcParams["figure.dpi"] = 141
    matplotlib.rcParams["pdf.fonttype"] = 42
    matplotlib.rcParams["font.size"] = 6
    matplotlib.rcParams["font.family"] = "Arial"
    DST_DIR.mkdir(parents=True, exist_ok=True)

    main()
