#!/usr/bin/env python3

""" Generate supplementary table 1.
Clinico-pathological and demographic characteristics of the colorectal patients of the Taxonomy and TCGA-COAD-READ cohorts.
"""
import pathlib

import pandas
import tableone

import cohort
import fusobacteria_utils


FUSOBACTERIA_DST_DIR = pathlib.Path("figures_and_tables")


def summarise_patients_clinical_characteristics() -> tableone.TableOne:
    """
    Summarise patients clinical characteristics and save results into csv file.
    :return: TableOne instance, with summarised clinical data.
    """
    crc = cohort.get_crc_cohorts()
    fusobacteria = fusobacteria_utils.get_crc_fusobacteria()
    # Include only patients with fusobacteria estimates
    cli = (
        crc.get_clinical()
        .join(crc.get_subtypes())
        .join(fusobacteria[[]], how="inner")
        .query('cohort.isin(["TCGA-COAD-READ", "taxonomy"])')
        .reset_index("cohort")
    )

    cli["cohort"] = pandas.Categorical(
        cli.cohort.replace({"taxonomy": "In house Taxonomy"}),
        ["In house Taxonomy", "TCGA-COAD-READ"],
    )
    cli["stage"] = cli.stage.map({1: "I", 2: "II", 3: "III", 4: "IV"})
    cli["resection_margins"] = cli.resection_margins.str.replace("_", " ")

    cols = [
        "stage",
        "T_stage",
        "N_stage",
        "M_stage",
        "age",
        "sex",
        "resection_margins",
        "tumour_site_1",
        "has_lymphovascular_invasion",
        "has_perineural_invasion",
        "cms_class",
        "cris_class",
    ]

    pretty_cols = {
        "stage": "Stage",
        "T_stage": "T stage",
        "N_stage": "N stage",
        "M_stage": "M stage",
        "age": "Age",
        "sex": "Sex",
        "resection_margins": "Resection margins",
        "tumour_site_1": "Tumour location",
        "has_lymphovascular_invasion": "Lymphovascular invasion",
        "has_perineural_invasion": "Perineural invasion",
        "cms_class": "CMS subtyping",
        "cris_class": "CRIS subtyping",
    }

    cat_cols = [
        "stage",
        "T_stage",
        "N_stage",
        "M_stage",
        "sex",
        "resection_margins",
        "tumour_site_1",
        "has_lymphovascular_invasion",
        "has_perineural_invasion",
        "cms_class",
        "cris_class",
    ]
    num_cols = ["age"]
    cli["age"] = cli.age.astype(float)  # Table1 fails with type Int64
    table1 = tableone.TableOne(
        cli.reset_index(),
        columns=cols,
        categorical=cat_cols,
        nonnormal=num_cols,
        groupby=["cohort"],
        pval=True,
        decimals=0,
        rename=pretty_cols,
    )
    table1.tableone.columns = table1.tableone.columns.get_level_values(1)
    table1.tableone.rename(
        columns={"Missing": "Missing [n]", "P-Value": "P-value"}, inplace=True
    )
    table1.tableone.rename_axis(index=["Variable", "Level"], inplace=True)
    table1.tableone.reset_index(inplace=True)

    isdupe = table1.tableone.duplicated(subset="Variable")
    table1.tableone["Variable"] = table1.tableone["Variable"].where(~isdupe, "")

    table1.to_csv(FUSOBACTERIA_DST_DIR / "supplementary_table_1.csv", index=False)

    return table1


if __name__ == "__main__":
    FUSOBACTERIA_DST_DIR.mkdir(parents=True, exist_ok=True)

    sup_table1 = summarise_patients_clinical_characteristics()
