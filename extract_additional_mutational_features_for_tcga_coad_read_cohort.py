#!/usr/bin/env python3

import pathlib

import dask.dataframe
import rpy2.robjects.packages
import rpy2.robjects.pandas2ri

import cohort
import fusobacteria_utils

maftools = rpy2.robjects.packages.importr('maftools')

DST_DIR = pathlib.Path('processed_data', 'tcga_coad_read_mutational_features')
TCGA_COAD_READ_MAF_FILE = 'tcga_coad_read_mc3_v0_2_8_public.maf'


def subset_mutations_maf_file_to_primary_coad_read_tumours(tcga_coad_read_maf_file: str) -> None:
    """
    Subset and export TCGA pancancer MAF file to include only patients with colon (COAD) or recatla (COAD) primary tumours.
    :param tcga_coad_read_maf_file: filename to name subset MAF file as.
    """
    cli = cohort.get_tcga_crc_cohort().get_clinical()
    patients = cli.index.get_level_values('patient_id')

    pancancer_maf = dask.dataframe.read_table(
        pathlib.Path('tcga_pancancer', 'download', 'pancancer', 'mc3.v0.2.8.PUBLIC.maf'),
        dtype={'Tumor_Sample_Barcode': 'category'})
    pancancer_maf['patient_id'] = pancancer_maf.Tumor_Sample_Barcode.str[:12].astype('category')
    pancancer_maf['sample_type'] = pancancer_maf.Tumor_Sample_Barcode.str[13:15].astype('category')
    tcga_coad_read_maf = pancancer_maf.query('patient_id.isin(@patients) and sample_type=="01"',
                                             local_dict={'patients': patients})
    tcga_coad_read_maf.to_csv(str(DST_DIR / tcga_coad_read_maf_file), sep='\t', index=False, single_file=True)


def extract_key_mutational_features(tcga_coad_read_maf_file: str) -> None:
    """
    Extract key mutational features from MAF file and export results to csv.
    Wrapper around R maftools package.
    :param tcga_coad_read_maf_file: MAF filename to read from.
    """
    # Load Fusobacteriales relative abundance data for the TCGA-COAD-READ cohort primary tumour patients.
    fusobacteria = fusobacteria_utils.get_crc_fusobacteria().query('cohort=="TCGA-COAD-READ"'). \
        reset_index(['technology'], drop=True)[['fusobacteria_binary_class']]

    # Load MAF file with maftools
    tcga_coad_read_maftools = maftools.read_maf(maf=str(DST_DIR / TCGA_COAD_READ_MAF_FILE))

    # Extract key mutational features
    with rpy2.robjects.conversion.localconverter(rpy2.robjects.default_converter + rpy2.robjects.pandas2ri.converter):
        titv = maftools.titv(maf=tcga_coad_read_maftools, plot=False, useSyn=True)
        titv_fraction_contribution = titv.rx('fraction.contribution')
        titv_raw_counts = titv.rx('raw.counts')
        titv_fractions = titv.rx('TiTv.fractions')
        titv_dict = dict(titv.items())

    results = titv_dict['TiTv.fractions'].set_index('Tumor_Sample_Barcode').join(
        titv_dict['fraction.contribution'].set_index('Tumor_Sample_Barcode'))
    results['patient_id'] = results.index.get_level_values('Tumor_Sample_Barcode').str[:12]
    results = fusobacteria.join(results.set_index('patient_id'), how='inner')
    results = results.set_index('fusobacteria_binary_class', append=True). \
        rename_axis(columns='feature').stack().to_frame('expression')

    results.to_csv(DST_DIR / 'tcga_coad_read_mutation_features.csv')


def main():
    subset_mutations_maf_file_to_primary_coad_read_tumours(TCGA_COAD_READ_MAF_FILE)
    extract_key_mutational_features(TCGA_COAD_READ_MAF_FILE)


if __name__ == '__main__':
    pathlib.Path(DST_DIR).mkdir(parents=True, exist_ok=True)

    main()
