#!/usr/bin/env python3

""" Generate figure 5, supplementary figures 9-10 and supplementary table 7.
Survival analysis for patients of the Taxonomy and TCGA-COAD-READ cohorts grouped by either Fusobacterium nucleatumFusobacteriales or  and/or mesenchymal status.
"""

import pathlib
import typing
import warnings

import lifelines
import matplotlib.pyplot
import pandas

import cohort
import constants
import fusobacteria_utils
import py_utils.compare_lr_test
import py_utils.format_pvalue
import py_utils.kaplan_meier

DST_DIR = pathlib.Path("figures_and_tables")


def _make_km_helper(
    data: pandas.DataFrame, grouping_var: str, end_point: str, **kwargs
):
    """
    Helper to make a single Kaplan-Meier (KM) plot.
    :param data: dataframe with one row per patient and columns for survival and 'fusobacteria_binary_class';
    :param grouping_var: column name in data to group by in KM plot;
    :param end_point: prefix for survival column names, ('os', 'dss' or 'dfs').
    """
    months_col = f"{end_point}_months"
    event_col = f"{end_point}_event"
    sub_data = data[[months_col, event_col, grouping_var]].dropna()

    py_utils.kaplan_meier.plot(
        sub_data[months_col],
        sub_data[event_col],
        sub_data[grouping_var],
        ylabel=end_point.upper(),
        bbox_to_anchor=(0, 0.8, 1, 0.1),
        fontsize="small",
        censor_marker_size=3,
        **kwargs,
    )


def _make_km_figure(
    data: pandas.DataFrame, end_point_types: typing.List[str], figure_label: str
):
    """
    Make a figure with multiple Kaplan-Meier plots.
    One row of subplots per survival end-point and 4 columns:
        - unselected patients grouped by either Fusobacteriales or Fn;
        - unselected patients grouped by mesenchymal status;
        - non-mesenchymal patients grouped by either Fusobacteriales or Fn;
        - mesenchymal patients grouped by either Fusobacteriales or Fn;
    :param data: dataframe, one row per patient, must include columns:
        - 'fusobacteria_binary_class': 'high', 'low';
        - 'is_mesenchymal': 'yes', 'no'
        - survival columns: ('os_months', 'os_event'), ('dss_months', 'dss_event'), ('dfs_months', 'dfs_event')
    :param end_point_types: clinical survival end-point, from 'os', 'dss' and/or 'dfs';
    :param name: label for figure.
    """
    end_point_n = len(end_point_types)
    if end_point_n == 3:
        gridspec_kw = dict(
            top=0.925, bottom=0.07, left=0.055, right=0.985, hspace=0.55, wspace=0.55
        )
    else:
        gridspec_kw = dict(
            top=0.87, bottom=0.095, left=0.07, right=0.975, hspace=0.68, wspace=0.5
        )

    fig, axes = matplotlib.pyplot.subplots(
        nrows=len(end_point_types),
        ncols=4,
        squeeze=False,
        figsize=(7, 1.5 * end_point_n + 1),
        gridspec_kw=gridspec_kw,
    )

    for row, ep in enumerate(end_point_types):
        _make_km_helper(
            data,
            "fusobacteria_binary_class",
            ep,
            colors=constants.FUSOBACTERIA_BIN_COLORS.values(),
            title="Unselected patients\nby $Fusobacteriales$",
            ax=axes[row][0],
        )
        _make_km_helper(
            data,
            "is_mesenchymal",
            ep,
            colors=constants.MESENCHYMAL_BIN_COLORS.values(),
            title="Unselected patients\nby mesenchymal status",
            ax=axes[row][1],
        )
        _make_km_helper(
            data.query('is_mesenchymal=="no"'),
            "fusobacteria_binary_class",
            ep,
            colors=constants.FUSOBACTERIA_BIN_COLORS.values(),
            title="Non-mesenchymal patients\nby $Fusobacteriales$",
            ax=axes[row][2],
            include_only_logrank_p_stats=True,
        )
        _make_km_helper(
            data.query('is_mesenchymal=="yes"'),
            "fusobacteria_binary_class",
            ep,
            colors=constants.FUSOBACTERIA_BIN_COLORS.values(),
            title="Mesenchymal patients\nby $Fusobacteriales$",
            ax=axes[row][3],
            include_only_logrank_p_stats=True,
        )

    fig.subplots_adjust(
        top=0.9, bottom=0.05, left=0.05, right=0.975, hspace=0.346, wspace=0.282
    )
    fig.savefig(DST_DIR / f"{figure_label}.pdf")


def _compute_interaction_cox_stats_by_clinical_endpoint_and_covariates(
    survival_data: pandas.DataFrame,
    survival_time: str,
    survival_event: str,
    cols: typing.List[str],
) -> pandas.DataFrame:
    """
    Compute Cox interaction statistics by clinical endpoint and covariates.
    :param survival_data: dataframe, one row per patient, must include columns:
        - 'fusobacteria_binary_class': 'high', 'low';
        - 'is_mesenchymal': 'yes', 'no';
    :param survival_time: name of column in survival_data with survival time;
    :param survival_event: name of column in survival_data with survival events;
    :param cols: additional columns in survival_data to use as covariates;
    :return: dataframe with summary interaction stats.
    """
    # Fit Cox model with interaction
    cp = lifelines.CoxPHFitter()
    cp.fit(
        survival_data,
        survival_time,
        survival_event,
        formula=" + ".join(["fusobacteria_binary_class * is_mesenchymal"] + cols),
    )

    # Fit a reduced Cox model (to use to calculate the interaction p-value)
    cp_reduced = lifelines.CoxPHFitter()
    cp_reduced.fit(
        survival_data,
        survival_time,
        survival_event,
        formula=" + ".join(["fusobacteria_binary_class + is_mesenchymal"] + cols),
    )
    interaction_pvalue = py_utils.format_pvalue.format_pvalue(
        py_utils.compare_lr_test.compare_lr_test(cp, cp_reduced)[1]
    )

    # Fit Cox model setting up the formula to return the HR for high vs. low within a mesenchymal group
    cp_hr = lifelines.CoxPHFitter()
    cp_hr.fit(
        survival_data,
        survival_time,
        survival_event,
        formula=" + ".join(["fusobacteria_binary_class : is_mesenchymal"] + cols),
    )

    # Store stats per model
    per_model_stats = pandas.DataFrame(
        {
            "interaction_pvalue": interaction_pvalue,
            "likelihood_ratio_test_pvalue": py_utils.format_pvalue.format_pvalue(
                cp.log_likelihood_ratio_test().p_value
            ),
            "total_n": survival_data.shape[0],
        },
        index=[""],
    )

    # Store stats per level (subset of interest)
    per_level_stats = cp_hr.summary[
        ["exp(coef)", "exp(coef) lower 95%", "exp(coef) upper 95%", "p"]
    ].query('covariate.str.startswith("fusobacteria_binary_class")')
    per_level_stats.reset_index(inplace=True)
    per_level_stats["covariate"] = per_level_stats.covariate.map(
        {
            "fusobacteria_binary_class[T.high]:is_mesenchymal[no]": "High vs. low Fusobacteriales in non-mesenchymal tumours",
            "fusobacteria_binary_class[T.high]:is_mesenchymal[yes]": "High vs. low Fusobacteriales in mesenchymal tumours",
        }
    )
    per_level_stats.set_index("covariate", inplace=True)
    per_level_stats[
        ["exp(coef)", "exp(coef) lower 95%", "exp(coef) upper 95%"]
    ] = per_level_stats[
        ["exp(coef)", "exp(coef) lower 95%", "exp(coef) upper 95%"]
    ].round(
        2
    )
    per_level_stats["p"] = per_level_stats.p.apply(py_utils.format_pvalue.format_pvalue)

    # Combine results
    stats = pandas.concat([per_model_stats, per_level_stats])

    stats.rename(
        columns={
            "interaction_pvalue": "Interaction\nP-value",
            "likelihood_ratio_test_pvalue": "Likelihood\nratio test\nP-value",
            "total_n": "Total N",
            "exp(coef)": "HR",
            "exp(coef) lower 95%": "2.5% CI",
            "exp(coef) upper 95%": "97.5% CI",
            "p": "P-value",
        },
        inplace=True,
    )

    return stats


def _compute_unadjusted_and_adjusted_interaction_cox_stats_by_survival_type(
    data: pandas.DataFrame, survival_type: str
) -> pandas.DataFrame:
    """
    Compute Cox interaction statistics by clinical endpoint and different sets of covariates.
    :param data: dataframe, one row per patient, must include columns:
        - 'fusobacteria_binary_class': 'high', 'low';
        - 'is_mesenchymal': 'yes', 'no';
        - ('os_months', 'os_event') or ('dss_months', 'dss_event') or ('dfs_months', 'dfs_event');
        - covariate columns 'stage', 'cancer_type', 'sex', 'age', 'has_history_colon_polyps',
          'has_history_other_malignancy';
    :param survival_type: type of clinical survival end-point from 'os', 'dss' or 'dfs';
    :return: dataframe with summary interaction stats for different models (different sets of covariates).
    """
    survival_time = f"{survival_type}_months"
    survival_event = f"{survival_type}_event"

    cols1 = [
        "stage",
        "cancer_type",
        "sex",
        "age",
    ]  # 'resection_margins', 'has_lymphovascular_invasion'
    cols2 = ["has_history_colon_polyps", "has_history_other_malignancy"]

    data = data.copy()
    data.loc[:, "age"] = data.age.astype(float)

    main_data = data.dropna(
        subset=[
            "fusobacteria_binary_class",
            "is_mesenchymal",
            survival_time,
            survival_event,
        ]
    )
    sup_data = data.dropna(
        subset=[
            "fusobacteria_binary_class",
            "is_mesenchymal",
            survival_time,
            survival_event,
        ]
        + cols1
        + cols2
    )
    stats = pandas.concat(
        {
            "Unadjusted (Fig. 5 main manuscript)": _compute_interaction_cox_stats_by_clinical_endpoint_and_covariates(
                main_data, survival_time, survival_event, cols=[]
            ),
            "Unadjusted": _compute_interaction_cox_stats_by_clinical_endpoint_and_covariates(
                sup_data, survival_time, survival_event, cols=[]
            ),
            "Adjusted by clinical, pathological and demographic covariates": _compute_interaction_cox_stats_by_clinical_endpoint_and_covariates(
                sup_data, survival_time, survival_event, cols=cols1
            ),
            "Adjusted by comorbidities and clinical, pathological and demographic covariates": _compute_interaction_cox_stats_by_clinical_endpoint_and_covariates(
                sup_data, survival_time, survival_event, cols=cols1 + cols2
            ),
        },
        names=["Model"],
    )

    return stats


def _compute_and_export_unadjusted_interaction_cox_stats_for_taxonomy_and_tcga_coad_read_cohorts(
    crc_data: pandas.DataFrame
) -> None:
    """
    Compute and export unadjusted interaction Cox stats for each available clinical survival end-point from Taxonomy and
    TCGA-COAD-READ cohorts.
    :param crc_data: dataframe, one row per patient, index 'cohort' and must include columns:
    - 'fusobacteria_binary_class': 'high', 'low';
    - 'is_mesenchymal': 'yes', 'no';
    - ('os_months', 'os_event') or ('dss_months', 'dss_event') or ('dfs_months', 'dfs_event').
    """
    stats = {}
    for cohort_name, survival_type in [
        ("taxonomy", "os"),
        ("taxonomy", "dfs"),
        ("TCGA-COAD-READ", "os"),
        ("TCGA-COAD-READ", "dss"),
        ("TCGA-COAD-READ", "dfs"),
    ]:
        survival_time = f"{survival_type}_months"
        survival_event = f"{survival_type}_event"
        sub_data = crc_data.query("cohort==@cohort_name").dropna(
            subset=[
                "fusobacteria_binary_class",
                "is_mesenchymal",
                survival_time,
                survival_event,
            ]
        )
        stats[
            (cohort_name, survival_type)
        ] = _compute_interaction_cox_stats_by_clinical_endpoint_and_covariates(
            sub_data, survival_time, survival_event, cols=[]
        )

    stats = pandas.concat(stats, names=["Cohort", "Clinical endpoint", "Level"])

    # Apply style and export to excel spreadsheet
    stats.style.set_properties(
        **{"text-align": "left", "font-size": "8pt", "font-family": "times_new_roman"}
    ).set_properties(
        subset=["Total N", "HR", "2.5% CI", "97.5% CI"], **{"text-align": "right"}
    ).to_excel(
        DST_DIR
        / "unadjusted_interaction_cox_stats_for_taxonomy_and_tcga_coad_read_cohorts.xlsx"
    )


def _compute_and_export_unadjusted_and_adjusted_interaction_cox_stats_for_tcga_coad_read_cohort(
    data
):
    """
    Compute and export unadjusted and adjusted interaction Cox stats for each available clinical survival end-point from
    the TCGA-COAD-READ cohort.
    :param data: dataframe, one row per patient, index 'cohort' and must include columns:
    - 'fusobacteria_binary_class': 'high', 'low';
    - 'is_mesenchymal': 'yes', 'no';
    - ('os_months', 'os_event') or ('dss_months', 'dss_event') or ('dfs_months', 'dfs_event');
    - covariate columns 'stage', 'cancer_type', 'sex', 'age', 'has_history_colon_polyps',
     'has_history_other_malignancy'.
    """
    stats = pandas.concat(
        {
            "Overall Survival (OS)": _compute_unadjusted_and_adjusted_interaction_cox_stats_by_survival_type(
                data, "os"
            ),
            "Disease Specific Survival (DSS)": _compute_unadjusted_and_adjusted_interaction_cox_stats_by_survival_type(
                data, "dss"
            ),
            "Disease Free Survival (DFS)": _compute_unadjusted_and_adjusted_interaction_cox_stats_by_survival_type(
                data, "dfs"
            ),
        },
        names=["Clinical endpoint"],
    )

    # Apply style and export to excel spreadsheet
    with warnings.catch_warnings():
        warnings.filterwarnings(
            "ignore",
            message="Title is more than 31 characters. Some applications may not be able to read the file",
        )

        writer = pandas.ExcelWriter(f"{DST_DIR}/supplementary_table_7.xlsx")
        for clinical_endpoint in stats.index.get_level_values("Clinical endpoint"):
            stats.query("`Clinical endpoint`==@clinical_endpoint").style.set_properties(
                **{
                    "text-align": "left",
                    "font-size": "8pt",
                    "font-family": "times_new_roman",
                }
            ).set_properties(
                subset=["Total N", "HR", "2.5% CI", "97.5% CI"],
                **{"text-align": "right"},
            ).to_excel(
                writer, sheet_name=clinical_endpoint
            )
        writer.close()


def main():
    fusobacteria = fusobacteria_utils.get_crc_fusobacteria()

    crc = cohort.get_crc_cohorts()
    crc_data = fusobacteria.join(crc.get_clinical().join(crc.get_subtypes()))

    _make_km_figure(
        crc_data.query('cohort=="taxonomy"'),
        ["os", "dfs"],
        "figure_5_panels_a_b_c_d_and_supplementary_figure_9",
    )
    _make_km_figure(
        crc_data.query('cohort=="TCGA-COAD-READ"'),
        ["os", "dss", "dfs"],
        "figure_5_panels_e_f_g_h_i_j_k_l_and_supplementary_figure_10",
    )

    _compute_and_export_unadjusted_interaction_cox_stats_for_taxonomy_and_tcga_coad_read_cohorts(
        crc_data
    )
    _compute_and_export_unadjusted_and_adjusted_interaction_cox_stats_for_tcga_coad_read_cohort(
        crc_data.query('cohort=="TCGA-COAD-READ"')
    )


if __name__ == "__main__":
    matplotlib.rcParams["pdf.fonttype"] = 42
    matplotlib.rcParams["font.size"] = 6
    matplotlib.rcParams["font.family"] = "Arial"
    DST_DIR.mkdir(parents=True, exist_ok=True)

    main()
