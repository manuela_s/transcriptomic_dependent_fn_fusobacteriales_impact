# Code book for the Taxonomy cohort fusobacterium_nucleatum.csv

- **patient_id:** unique identifier for each patient;

- **fusobacterium_nucleatum:** patient-level measurements of *Fusobacterium nucleatum* load from resected tumour tissue 
                               by quantitative polymerase chain reaction (qPCR).

See "Supplementary Materials and Methods" for more details.
