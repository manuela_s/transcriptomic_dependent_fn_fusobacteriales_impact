# Code book for the Taxonomy cohort rna.csv

- **patient_id:** unique identifier for each patient;

- columns include processed transcriptomics data (Almac Xcel array, Almac Diagnostics, Craigavon, UK; GSE103479) as
  described in [**Allen *et al.*, JCO Precision Oncology 2018, PMID: 30088816](https://pubmed.ncbi.nlm.nih.gov/30088816/)
  and [**McCorry *et al.*, Journal of Pathology, 2018, PMID: 30105762](https://pubmed.ncbi.nlm.nih.gov/30105762/).

See "Supplementary Materials and Methods" for more details.
