# Code book for the Taxonomy cohort clinical.csv

- **patient_id:** unique identifier for each patient;

- **stage:** colorectal cancer disease stage defined according to the 
  [tumour/node/metastasis (TNM) system](https://cancerstaging.org/references-tools/quickreferences/Documents/ColonMedium.pdf)
  by the American Joint Committee on Cancer (AJCC), either
    - 2: T1/T2, N1/N2, M0 or T3/T4, N0, M0;
    - 3: any T, N1/N2, M0;

- **T_stage:** tumour extent staging according to the [TNM system](https://cancerstaging.org/references-tools/Pages/What-is-Cancer-Staging.aspx) by AJCC, either
    - 'T1';
    - 'T2';
    - 'T3';
    - 'T4';

- **N_stage:** lymph nodes staging according to the [TNM system](https://cancerstaging.org/references-tools/Pages/What-is-Cancer-Staging.aspx) by AJCC
    - 'N0';
    - 'N1'; 
    - 'N2';

- **M_stage:** metastasis staging according to the [TNM system](https://cancerstaging.org/references-tools/Pages/What-is-Cancer-Staging.aspx) by AJCC
    - 'M0';
    - 'Mx';

- **age:** patient age at diagnosis, in years;

- **sex:** patient sex, either
    - 'female';
    - 'male';

- **resection_margins:** status of residual tumor, either
    - 'R0': tumor-free surgical margins;
    - 'R1_or_R2': residual tumor present in surgical margins;

- **tumour_size:** maximum tumour diameter, in cm;

- **cancer_type:** type of cancer, either
    - 'COAD': colon cancer;
    - 'READ': rectal cancer;
    - empty: no information available;
    
- **tumour_site_1:** location of primary tumor according to [Henry KA et al., J Cancer Epidemiol., 2014](http://www.ncbi.nlm.nih.gov/pubmed/25165475), either
    - 'distal': mapped from 'descending colon', 'sigmoid colon', 'distal transverse', 'left colon';  
    - 'proximal': mapped from 'ascending colon', 'caecum', 'caecum/ascending colon', 'right colon', 'hepatic flexure',
      'transverse colon', 'ileocaecal valve';
    - 'rectal', mapped from 'sigmoid rectum';
    - empty: no information available;
  
- **tumour_site_2:** location of primary tumor according to [Tejpar S et al., JAMA Oncol., 2017](https://www.ncbi.nlm.nih.gov/pubmed/27722750), either
    - 'left_sided': mapped from 'descending colon', 'sigmoid colon', 'sigmoid rectum', 'left colon';
    - 'right_sided': mapped from 'ascending colon', 'caecum', 'caecum/ascending colon', 'right colon', 'hepatic flexure',
      'transverse colon', 'ileocaecal valve';
    - 'undetermined_origin': mapped from 'distal transverse';
    - empty: no information available;

- **differentiation:** observed degree of tumor differentiation, either
    - 'poor': poor differentiation was observed;
    - 'moderate': moderate differentiation was observed;
    - 'well': greater differentiation was observed;
    - empty: no information available;

- **nodal_count:** total number of lymph nodes excised. Integer or empty if information was not available;

- **positive_nodes**: number of positive nodes observed. Integer or empty if information was not available;

- **has_adjuvant_chemotherapy:** whether chemotherapy treatment was administered to the patient, either
    - 'yes': patient was treated with chemotherapy;
    - 'no': patient was not treated with chemotherapy;
    - empty: no information available;

- **has_lymphovascular_invasion:** whether there was lymphatic invasion, either
    - 'yes': lymphovascular invasion was observed;
    - 'no': lymphovascular invasion was not observed;
    - empty: no information available;

- **has_perineural_invasion:** whether there was perineural invasion, either
    - 'yes': perineural invasion was observed;
    - 'no': perineural invasion was not observed;
    - empty: no information available;

- **has_extramural_venous_invasion:** whether there was extramural venous invasion, either
    - 'yes': extramural venous invasion was observed;
    - 'no': extramural venous invasion was not observed;
    - empty: no information available;

- **kras_status:** KRAS mutational status, either
    - 'wild_type', if no point mutation in BRAF was detected;
    - 'mutant': if at least a point mutation was detected in codon 12, 13 and 61;
    - empty, if information was not available;

- **braf_status:** BRAF mutational status, either
    - 'wild_type', if no point mutation in BRAF was detected;
    - 'mutant': if [amino acid substitution in position 600 from valine V to
      glutamic acid E was detected (V600E)](https://cancer.sanger.ac.uk/cosmic/mutation/overview?id=476);

- **tp53_status:** p53 mutational status, either
    - 'wild_type', if no point mutation in BRAF was detected;
    - 'mutant': if at least a point mutation was detected in codon 12, 13 and 61;
    - empty, if information was not available;

- **os_months:** time interval elapsed between surgical removal of the tumor (either primary or recurrent, as specified in
  the 'sample_type' field) and death from any cause, loss to follow-up or study end-date, in months;

- **os_event:** event,
    - 'yes': patient died from any cause before being lost to follow-up or study end-date;
    - 'no': patient was alive before being lost to follow-up or study end-date;
    - empty: no information available;

- **dfs_months:** time interval elapsed between surgical removal of the tumor (either primary or recurrent, as specified in
  the 'sample_type' field) and tumor relapse (either local and/or metastasis), loss to follow-up or study end-date, in
  months;
- 
- **dfs_event:** event,
    - 'yes': patient had a documented relapse before being lost to follow-up or study end-date;
    - 'no': patient did not relapse before being lost to follow-up or study end-date;
    - empty: no information available.
