# Code book for the Taxonomy cohort rna_based_molecular_subtypes.csv

- **patient_id:** unique identifier for each patient;

- columns include molecular classification for patients' tumour samples according to the Consensus Molecular Subtype
  (CMS, [Guinney *et al.*, Nat. Med., 2015, PMID: 26457759)](https://www.ncbi.nlm.nih.gov/pubmed/26457759) and the
  Cancer Intrinsic Subtype (CRIS, [Isella *et al.*, Nat. Comm., 2017, PMID: 28561063](https://www.ncbi.nlm.nih.gov/pubmed/28561063),
  as described in [McCorry *et al.* (Journal of Pathology, 2018, PMID: 30105762)](https://pubmed.ncbi.nlm.nih.gov/30105762/).

See "Supplementary Materials and Methods" for more details.
