# Code book for the Taxonomy cohort mcp_counter_immune_cells.csv

- **patient_id:** unique identifier for each patient;

- columns include cell types scores derived with the Microenvironment Cell Populations (MCP)-counter classifier [(Becht
  et al., Genome Biol. 2016, PMID: 27765066)](https://pubmed.ncbi.nlm.nih.gov/27765066/) as described in [McCorry *et al.*
  (Journal of Pathology, 2018, PMID: 30105762)](https://pubmed.ncbi.nlm.nih.gov/30105762/)
  followed by robust scaling (`sklearn.preprocessing.RobustScaler`).

See "Supplementary Materials and Methods" for more details.
