#!/usr/bin/env python3

""" Prepare RNA derived cell type profiling and signatures for analysis."""

import pathlib

import pandas
import sklearn.preprocessing

import transcriptomics.signatures


def quantiseq_cell_types_profiling() -> None:
    """
    Compute quanTIseq cell types composition (Finotello et al., Genome Med., 2019, PMID: 31126321).
    """
    rna = pandas.read_csv(
        pathlib.Path("taxonomy_cohort", "data", "rna.csv"), index_col=0
    )

    quantiseq = transcriptomics.signatures.run_quantiseq_classifier(
        rna, is_microarray=True
    )

    quantiseq.to_csv(
        pathlib.Path("taxonomy_cohort", "processed_data", "quantiseq_cell_types.csv")
    )


def prepare_rna_based_signatures() -> None:
    """Prepare transcriptomics-based signatures."""
    rna = pandas.read_csv(
        pathlib.Path("taxonomy_cohort", "data", "rna.csv"), index_col=0
    )
    rna_log2_zscore = pandas.DataFrame(
        sklearn.preprocessing.RobustScaler().fit_transform(rna),
        index=rna.index,
        columns=rna.columns,
    )

    signatures = pandas.concat(
        {
            "rna_emt_score": transcriptomics.signatures.run_emt_classifier(
                rna_log2_zscore
            ),
            "rna_tis_score": transcriptomics.signatures.run_tis_score(rna_log2_zscore),
            "rna_ifng_score": transcriptomics.signatures.run_ifng_score(
                rna_log2_zscore
            ),
            "rna_cytolytic_score": transcriptomics.signatures.run_cytolytic_score(
                rna_log2_zscore
            ),
            "rna_hippo_score": transcriptomics.signatures.run_hippo_classifier(
                rna_log2_zscore
            ),
            "rna_hypoxia_score": transcriptomics.signatures.run_hypoxia_classifier(
                rna_log2_zscore
            ),
            "rna_proliferation_score": transcriptomics.signatures.run_proliferation_classifier(
                rna_log2_zscore
            ),
            "rna_dna_damage_score": transcriptomics.signatures.run_dna_damage_classifier(
                rna_log2_zscore
            ),
            "rna_wnt_score": transcriptomics.signatures.run_wnt_classifier(
                rna_log2_zscore
            ),
            "rna_metastasis_score": transcriptomics.signatures.run_metastasis_classifier(
                rna_log2_zscore
            ),
            "rna_stemness_score": transcriptomics.signatures.run_stemness_classifier(
                rna_log2_zscore
            ),
        },
        axis=1,
    )

    signatures.to_csv(
        pathlib.Path(
            "taxonomy_cohort", "processed_data", "computed_rna_based_signatures.csv"
        )
    )


def main():
    quantiseq_cell_types_profiling()
    prepare_rna_based_signatures()


if __name__ == "__main__":
    pathlib.Path("taxonomy_cohort", "processed_data").mkdir(parents=True, exist_ok=True)

    main()
