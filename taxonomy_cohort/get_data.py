#!/usr/bin/env python3

"""Functions and classes to retrieve clinical and molecular data for the Taxonomy cohort."""

import pathlib

import pandas

import py_utils.cohort


class Taxonomy:
    def get_clinical(self) -> pandas.DataFrame:
        df = pandas.read_csv(
            pathlib.Path("taxonomy_cohort", "data", "clinical.csv"),
            dtype={
                "stage": pandas.CategoricalDtype(categories=[0, 1, 2, 3, 4]),
                "T_stage": pandas.CategoricalDtype(categories=["T1", "T2", "T3", "T4"]),
                "N_stage": pandas.CategoricalDtype(categories=["N0", "N1", "N2"]),
                "differentiation": pandas.CategoricalDtype(
                    categories=["poor", "moderate", "well"]
                ),
                "resection_margins": pandas.CategoricalDtype(
                    categories=["R0", "R1_or_R2"]
                ),
                "sex": pandas.CategoricalDtype(categories=["male", "female"]),
                "age": "Int64",
                "cancer_type": pandas.CategoricalDtype(categories=["COAD", "READ"]),
                "tumour_site_1": pandas.CategoricalDtype(
                    categories=["proximal", "distal", "rectal"]
                ),
                "tumour_site_2": pandas.CategoricalDtype(
                    categories=["left_sided", "right_sided", "undetermined_origin"]
                ),
                "has_adjuvant_chemotherapy": pandas.CategoricalDtype(
                    categories=["no", "yes"]
                ),
                "has_lymphovascular_invasion": pandas.CategoricalDtype(
                    categories=["no", "yes"]
                ),
                "has_perineural_invasion": pandas.CategoricalDtype(
                    categories=["no", "yes"]
                ),
                "os_months": float,
                "dfs_months": float,
            },
        )

        # All patients in the Taxonomy cohort are stage II or III, so set M_stage to M0
        df["M_stage"] = "M0"
        df["cohort"] = "taxonomy"
        df.set_index(["cohort", "patient_id"], inplace=True)

        df = df[
            [
                "stage",
                "T_stage",
                "N_stage",
                "M_stage",
                "differentiation",
                "resection_margins",
                "sex",
                "age",
                "cancer_type",
                "tumour_site_1",
                "tumour_site_2",
                "has_lymphovascular_invasion",
                "has_perineural_invasion",
                "os_months",
                "os_event",
                "dfs_months",
                "dfs_event",
            ]
        ]

        return df

    def get_subtypes(self) -> pandas.DataFrame:
        df = pandas.read_csv(
            pathlib.Path("taxonomy_cohort", "data", "rna_based_molecular_subtypes.csv")
        )

        df["cms_class"] = pandas.Categorical(
            df["CMS-RF-nearest"], ["CMS1", "CMS2", "CMS3", "CMS4", "NOLBL"]
        ).fillna("NOLBL")
        df["cris_class"] = pandas.Categorical(
            df.CRIS, ["CRIS-A", "CRIS-B", "CRIS-C", "CRIS-D", "CRIS-E", "NOLBL"]
        ).fillna("NOLBL")
        df["pooled_cms_class"] = df.cms_class.replace(
            {
                "CMS1": "not_CMS4",
                "CMS2": "not_CMS4",
                "CMS3": "not_CMS4",
                "NOLBL": "not_CMS4",
            }
        )
        df["pooled_cris_class"] = df.cris_class.replace(
            {
                "CRIS-A": "not_CRIS-B",
                "CRIS-C": "not_CRIS-B",
                "CRIS-D": "not_CRIS-B",
                "CRIS-E": "not_CRIS-B",
                "NOLBL": "not_CRIS-B",
            }
        )
        df["pooled_cms_cris_class"] = df.pooled_cms_class + "_" + df.pooled_cris_class
        df["is_mesenchymal"] = df.pooled_cms_cris_class.replace(
            {
                "not_CMS4_not_CRIS-B": "no",
                "CMS4_not_CRIS-B": "yes",
                "not_CMS4_CRIS-B": "yes",
                "CMS4_CRIS-B": "yes",
            }
        )

        df["cohort"] = "taxonomy"
        df.set_index(["cohort", "patient_id"], inplace=True)

        return df[["cms_class", "cris_class", "is_mesenchymal"]]

    def get_cna(self):
        pass

    def get_rna(self, standardization: bool = True) -> pandas.DataFrame:
        df = pandas.read_csv(pathlib.Path("taxonomy_cohort", "data", "rna.csv"))

        df["cohort"] = "taxonomy"
        df.set_index(["cohort", "patient_id"], inplace=True)

        if standardization:
            df = py_utils.cohort.standardization_helper(df)

        return df

    def get_rppa(self):
        pass

    def get_mutations(self):
        pass

    def get_immuno_composition(self) -> None:
        df = pandas.read_csv(
            pathlib.Path("taxonomy_cohort", "data", "mcp_counter_immune_cells.csv")
        )

        df["cohort"] = "taxonomy"
        df.set_index(["cohort", "patient_id"], inplace=True)

        return df

    def get_quantiseq_cell_types(self) -> pandas.DataFrame:
        df = pandas.read_csv(
            pathlib.Path(
                "taxonomy_cohort", "processed_data", "quantiseq_cell_types.csv"
            )
        )

        df["cohort"] = "taxonomy"
        df.set_index(["cohort", "patient_id"], inplace=True)

        return df

    def get_rna_based_signatures(self) -> pandas.DataFrame:
        df = pandas.read_csv(
            pathlib.Path(
                "taxonomy_cohort", "processed_data", "computed_rna_based_signatures.csv"
            )
        )

        df["cohort"] = "taxonomy"
        df.set_index(["cohort", "patient_id"], inplace=True)

        return df

    def get_microsatellite_and_mutational_status(self) -> pandas.DataFrame:
        df = pandas.read_csv(
            pathlib.Path("taxonomy_cohort", "data", "clinical.csv"),
            dtype={
                "kras_status": pandas.CategoricalDtype(
                    categories=["wild_type", "mutant"]
                ),
                "braf_status": pandas.CategoricalDtype(
                    categories=["wild_type", "mutant"]
                ),
                "tp53_status": pandas.CategoricalDtype(
                    categories=["wild_type", "mutant"]
                ),
            },
        )

        df["cohort"] = "taxonomy"
        df.set_index(["cohort", "patient_id"], inplace=True)

        return df[["kras_status", "braf_status", "tp53_status"]]


def fusobacterium_nucleatum() -> pandas.DataFrame:
    data = pandas.read_csv(
        pathlib.Path("taxonomy_cohort", "data", "fusobacterium_nucleatum.csv")
    )

    data["technology"] = "qpcr"
    data["cohort"] = "taxonomy"
    data.set_index(["technology", "cohort", "patient_id"], inplace=True)

    return data
