#!/usr/bin/env python3

"""Factory functions for cohort classes to get clinical and molecular data for cohorts."""

import py_utils.cohort
import taxonomy_cohort
import tcga_pancancer.get_tcga_crc
import tcga_pancancer.get_tcga_pancancer


def get_qpcr_in_house_cohorts():
    return taxonomy_cohort.get_data.Taxonomy()


def get_tcga_crc_cohort():
    return tcga_pancancer.TcgaCrc()


def get_crc_cohorts():
    return py_utils.cohort.CombinedCohort(
        [taxonomy_cohort.get_data.Taxonomy(), tcga_pancancer.TcgaCrc()]
    )


def get_pancancer_cohorts():
    return py_utils.cohort.CombinedCohort([tcga_pancancer.TcgaPancancer()])
