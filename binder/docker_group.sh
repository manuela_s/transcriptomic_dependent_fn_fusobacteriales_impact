#!/bin/bash -x

# Based on https://github.com/jenkinsci/docker/issues/196#issuecomment-179486312

# Create a group inside the container that matches the group of the docker socket (from the host)


DOCKER_SOCKET=/var/run/docker.sock
DOCKER_GROUP=hostdocker

if [ -S ${DOCKER_SOCKET} ]; then
    DOCKER_GID=$(stat -c '%g' ${DOCKER_SOCKET})
    groupadd -for -g ${DOCKER_GID} ${DOCKER_GROUP}
    usermod -aG ${DOCKER_GROUP} ${USER}
fi
