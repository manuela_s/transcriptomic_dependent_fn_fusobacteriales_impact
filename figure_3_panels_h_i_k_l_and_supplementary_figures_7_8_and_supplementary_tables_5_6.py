#!/usr/bin/env python3

""" Generate figure 3 panels h, i, j, k, supplementary figures 7-8 and supplementary tables 5-6.
Association between Fusobacteriales relative abundance and molecular data from transcriptomics and proteomics in patients
of the TCGA-COAD-READ cohort."""

import os
import pathlib
import typing
import warnings

os.environ["OUTDATED_IGNORE"] = "1"

import gseapy
import matplotlib
import matplotlib.cm
import matplotlib.colors
import matplotlib.pyplot
import numpy
import pandas
import pingouin
import scipy
import scipy.cluster
import seaborn
import sklearn.pipeline
import sklearn.preprocessing

import cohort
import constants
import fusobacteria_utils
import py_utils.format_pvalue
import tcga_pancancer.get_rppa_metadata


DST_DIR = pathlib.Path("figures_and_tables")


def _feature_transform(df: pandas.DataFrame) -> pandas.DataFrame:
    """
    Transform dataframe by applying mean centering followed by power transformation.
    :param df: dataframe to _feature_transform;
    :return: transformed dataframe.
    """
    # https://github.com/scikit-learn/scikit-learn/issues/14959#issuecomment-602090088
    preprocessor = sklearn.pipeline.make_pipeline(
        sklearn.preprocessing.StandardScaler(with_std=False),
        sklearn.preprocessing.PowerTransformer(standardize=True, method="yeo-johnson"),
    )
    return pandas.DataFrame(
        preprocessor.fit_transform(df), index=df.index, columns=df.columns
    )


def _identify_significant_continuous_features(
    layer_data: pandas.DataFrame, fusobacteria: pandas.DataFrame
) -> pandas.DataFrame:
    """
    Identify statistically significant features by Fusobacteriales relative abundance (continuous).
    :param layer_data: dataframe, one row per patient and features to test as columns;
    :param fusobacteria: dataframe, one row per patient, must include 'fusobacteria_binary_class' column;
    :return: dataframe, one row per feature tested:
            - indices: feature' and 'test';
            - columns: 'X', 'method', 'tail', 'n', 'r', 'CI95%', 'r2', 'adj_r2', 'z', 'p-unc', 'p-corr', 'p-adjust' and
                       'power'.
    """
    return pingouin.pairwise_corr(
        layer_data.join(fusobacteria.fusobacteria),
        ["fusobacteria"],
        method="spearman",
        padjust="fdr_bh",
    ).set_index(["Y"])


def _get_gene_sets(
    gmt_file: pathlib.Path, background: set
) -> typing.Dict[str, typing.Set[str]]:
    """Reads in gene sets and filter genes for background.
    :param gmt_file:
    :param background: set of all gene symbols that were measured
    """
    gene_sets = dict()
    with open(gmt_file) as f:
        for line in f:
            parts = line.strip().split(sep="\t")
            gene_sets[parts[0]] = set(parts[1:]).intersection(background)

    return gene_sets


def _plot_heatmap(data: pandas.DataFrame, name: str) -> None:
    """
    Generate figure 3 panels H and K.
    Plot heatmap with annotation for Fusobacteriales relative abundance (low vs. high).
    :param data: dataframe, one row per patient and features as columns. Index must include 'fusobacteria_binary_class';
    :param name: panel label to name figure.
    """

    def grouping_sort_table(df: pandas.DataFrame) -> pandas.DataFrame:
        """
        Sort the rows of the dataframe by similarity.
        :param df: dataframe to sort;
        :return: sorted dataframe.
        """
        Z = scipy.cluster.hierarchy.linkage(
            df, method="complete", metric="euclidean", optimal_ordering=True
        )
        return df.iloc[scipy.cluster.hierarchy.leaves_list(Z)]

    data = data.sort_index(level="fusobacteria_binary_class")
    data = (
        data.groupby("fusobacteria_binary_class")
        .apply(grouping_sort_table)
        .reset_index(0, drop=True)
    )

    patients_bin_colors = matplotlib.colors.ListedColormap(
        constants.FUSOBACTERIA_BIN_COLORS.values()
    )(data.index.get_level_values("fusobacteria_binary_class").codes)

    g = seaborn.clustermap(
        data.T,
        xticklabels=False,
        yticklabels=False,
        z_score=None,
        col_cluster=False,
        col_colors=patients_bin_colors,
        cmap="RdBu_r",
        vmin=-3,
        vmax=3,
        cbar_kws=dict(label="Normalized scaled feature"),
    )

    g.savefig(DST_DIR / f"figure_3_panel_{name}.pdf")


def _enrichment_for_gene_set(
    gene_set_name: str, pathway_genes: typing.List[str], background: typing.Set[str]
) -> pandas.DataFrame:
    """
    Run enrichment analysis for one gene set.
    :param gene_set_name: name of gene set. Must match names of gmt files downloaded from
                          https://maayanlab.cloud/Enrichr/#stats.
    :param pathway_genes: list of genes to include in enrichment analysis;
    :param background: set of genes to use as background in enrichment analysis.
    :return: dataframe, one row per term and columns 'term', 'overlap', 'p_value', 'adjusted_p_value', 'genes',
             'changed_genes', 'pathway_size' and 'changed_ratio'.
    """
    df = gseapy.enrichr(
        list(pathway_genes),
        gene_sets=_get_gene_sets(
            pathlib.Path("data", "gene_sets", f"{gene_set_name}.gmt"), background
        ),
        outdir=None,
        no_plot=True,
    ).results
    df.drop(columns=["Gene_set"], inplace=True)
    df["gene_set"] = gene_set_name
    df.columns = df.columns.str.lower().str.replace(" |-", "_")
    df = df.join(
        df.overlap.str.split("/", expand=True)
        .rename(columns={0: "changed_genes", 1: "pathway_size"})
        .astype(int)
    )
    df["changed_ratio"] = df.changed_genes / df.pathway_size

    return df.set_index("gene_set")


def _plot_enrichment(
    data: pandas.DataFrame, vmin: float, vmax: float, name: str
) -> None:
    """
    Generate figure 3 panels I and L and supplementary figures 7-8.
    Plot enrichment results.
    :param data: dataframe, one row per enriched term and columns must include 'trimmed_term', 'changed_ratio',
                 'gene_set', 'changed_genes' and ajusted_p_value';
    :param vmin: lower bound clipping value for colormap;
    :param vmax: upper bound clipping value for colormap;
    :param name: label to name figure.
    """
    with warnings.catch_warnings():
        # Fixed gridspec causes matplotlib tight_layout to issue warnings.
        warnings.simplefilter("ignore", UserWarning)
        g = seaborn.relplot(
            x="trimmed_term",
            y="changed_ratio",
            col="gene_set",
            size="changed_genes",
            hue="adjusted_p_value",
            hue_norm=matplotlib.colors.LogNorm(vmin=vmin, vmax=vmax),
            edgecolor="k",
            linewidth=0.5,
            legend="brief",
            palette="Blues_r",
            height=7 / 2.54,
            aspect=0.545,
            facet_kws=dict(
                gridspec_kws=dict(
                    left=0.3, bottom=0.75, top=0.925, right=0.995, wspace=0.05
                ),
                sharex=False,
            ),
            data=data,
        )
    g.set(ylim=[0, 1.1], yticks=numpy.arange(0, 1.1, 0.25))
    g.set_xticklabels(rotation=90, horizontalalignment="right", fontsize=5)
    g.set_titles("{col_name}")
    g.set_axis_labels(x_var="", y_var="Fraction of\naltered genes\nwithin a pathway")

    g._legend._set_loc(2)
    g._legend.set_bbox_to_anchor([0, 1])

    g.savefig(DST_DIR / f"{name}.pdf")


def _enrichment_analysis_per_molecular_layer(
    pathway_genes: typing.List[str],
    background: typing.Set[str],
    vmin: float,
    vmax: float,
    main_name: str,
    supplement_name: str,
) -> None:
    """
    Generate figure 3 panels I and L and supplementary figures 7-8.
    Compute and plot enrichment.
    :param pathway_genes: list of genes to include in enrichment analysis;
    :param background: set of genes to use as background in enrichment analysis;
    :param vmin: lower bound clipping value for colormap;
    :param vmax: upper bound clipping value for colormap;
    :param main_name: label to name panel figure in main manuscript;
    :param supplement_name: label to name supplementary figure.
    """
    results = pandas.concat(
        [
            _enrichment_for_gene_set("BioPlanet_2019", pathway_genes, background),
            _enrichment_for_gene_set("BioCarta_2016", pathway_genes, background),
            _enrichment_for_gene_set("NCI-Nature_2016", pathway_genes, background),
            _enrichment_for_gene_set("MSigDB_Hallmark_2020", pathway_genes, background),
        ]
    )
    results["trimmed_term"] = (
        results.term.str.replace(" \\(GO:.*\\)", "")
        .str.replace(".*regulation of ", "")
        .str.replace(" Homo sapiens .*", "")
        .str.replace(" WP.*", "")
        .str.replace("Validated targets of ", "")
        .str.replace("Validated ", "")
    )
    results["neg_log10_adjusted_pvalue"] = -numpy.log10(results.adjusted_p_value)
    results.query("adjusted_p_value < 0.05", inplace=True)

    # Select top 8 enriched terms per gene set for plotting
    pathway_plot_data = (
        results.query("adjusted_p_value < 0.05")
        .groupby("gene_set")
        .apply(lambda x: x.nlargest(8, "changed_ratio"))
        .reset_index(level=1, drop=True)
    )
    _plot_enrichment(
        pathway_plot_data.query(
            'gene_set.isin(["BioPlanet_2019", "MSigDB_Hallmark_2020"])'
        ),
        vmin=vmin,
        vmax=vmax,
        name=main_name,
    )
    _plot_enrichment(
        pathway_plot_data.query('gene_set.isin(["BioCarta_2016", "NCI-Nature_2016"])'),
        vmin=vmin,
        vmax=vmax,
        name=supplement_name,
    )


def supplementary_table_5(stats: pandas.DataFrame) -> None:
    """
    Generate supplementary table 5.
    :param stats: dataframe, one row per significant feature and columns include 'method', 'tail', 'n', 'r',
                             'CI95%', 'p-unc' and 'p-corr'.
    """
    stats_for_export = (
        stats[["method", "tail", "n", "r", "CI95%", "p-unc", "p-corr"]]
        .copy()
        .rename_axis(index={"Y": "Gene"})
    )
    stats_for_export.columns = stats_for_export.columns.str.capitalize()
    stats_for_export.rename(
        columns={
            "Ci95%": "95% CI",
            "P-unc": "Unadjusted P-value",
            "P-corr": "FDR-BH adjusted P-value",
        },
        inplace=True,
    )

    stats_for_export["Method"] = stats_for_export.Method.replace(
        "spearman", "spearman correlation"
    )
    stats_for_export["R"] = stats_for_export.R.round(2)
    stats_for_export["Unadjusted P-value"] = stats_for_export[
        "Unadjusted P-value"
    ].apply(py_utils.format_pvalue.format_pvalue)
    stats_for_export["FDR-BH adjusted P-value"] = stats_for_export[
        "FDR-BH adjusted P-value"
    ].apply(py_utils.format_pvalue.format_pvalue)

    stats_for_export.to_csv(DST_DIR / "supplementary_table_5.csv")


def figure_3_panels_h_i_and_supplementary_figure_7_and_supplementary_table_5(
    fusobacteria: pandas.DataFrame
) -> None:
    """
    Generate figure 3 panels H-I, supplementary figure 7 and export supplementary table 5.
    Figure 3 panels H-I include a heatmap (H) displaying expression of genes differentially expressed when comparing
    Fusobacteriales-high vs. low patients and corresponding pathway enrichment analysis (I) with the BioPlanet (version
    2019) and MSigDB Hallmark (version 2020) for patients of the TCGA-COAD-READ cohort.
    Supplementary figure 7 includes pathway enrichment analysis with the BioCarta (version 2016) and NCI-Nature
    (version 2016) pathway databases for genes differentially expressed by Fusobacteriales relative abundance in
    patients of the TCGA-COAD-READ cohort.
    Supplementary table 5 include statistical results testing the association between gene expression and
    Fusobacteriales relative abundance in the TCGA-COAD-READ patients.
    :param fusobacteria: dataframe, one row per patient as returned by `fusobacteria_utils.get_crc_fusobacteria()`.
    """
    tcga_rna = cohort.get_tcga_crc_cohort().get_rna(standardization=False)
    # Include 5000 most variant genes in downstream analysis
    tcga_rna = tcga_rna[
        tcga_rna.var().sort_values(ascending=False).head(5000).index.to_list()
    ]
    tcga_rna = _feature_transform(tcga_rna)

    stats = _identify_significant_continuous_features(tcga_rna, fusobacteria)
    significant_stats = stats.query("`p-corr` < 0.05")

    supplementary_table_5(significant_stats)

    plot_data = (
        tcga_rna[significant_stats.index]
        .join(fusobacteria.fusobacteria_binary_class, how="inner")
        .set_index("fusobacteria_binary_class", append=True)
    )

    _plot_heatmap(plot_data, "h")
    _enrichment_analysis_per_molecular_layer(
        pathway_genes=plot_data.columns.to_list(),
        background=set(tcga_rna.columns),
        vmin=1e-19,
        vmax=1,
        main_name="figure_3_panel_i",
        supplement_name="supplementary_figure_7",
    )


def supplementary_table_6(stats: pandas.DataFrame) -> None:
    """
    Generate supplementary table 6.
    :param stats: dataframe, one row per significant feature and columns include 'method', 'tail', 'n', 'r', 'CI95%',
                  'p-unc' and 'p-corr'.
    """
    stats_for_export = (
        stats[["method", "tail", "n", "r", "CI95%", "p-unc", "p-corr"]]
        .copy()
        .rename_axis(index={"Y": "Protein"})
    )
    stats_for_export.columns = stats_for_export.columns.str.capitalize()
    stats_for_export.rename(
        columns={
            "Ci95%": "95% CI",
            "P-unc": "Unadjusted P-value",
            "P-corr": "FDR-BH adjusted P-value",
        },
        inplace=True,
    )

    stats_for_export["Method"] = stats_for_export.Method.replace(
        "spearman", "spearman correlation"
    )
    stats_for_export["R"] = stats_for_export.R.round(2)
    stats_for_export["Unadjusted P-value"] = stats_for_export[
        "Unadjusted P-value"
    ].apply(py_utils.format_pvalue.format_pvalue)
    stats_for_export["FDR-BH adjusted P-value"] = stats_for_export[
        "FDR-BH adjusted P-value"
    ].apply(py_utils.format_pvalue.format_pvalue)
    stats_for_export.reset_index(inplace=True)
    stats_for_export["Protein"] = stats_for_export.Protein.map(
        {
            "atm": "ATM",
            "cmyc": "cMYC",
            "caspase7cleavedd198": "Caspase7 (cleaved 198)",
            "chk1": "Chk1",
            "claudin7": "Claudin7",
            "cycline1": "Cycline1",
            "dvl3": "DVL3",
            "gab2": "Gab2",
            "igfbp2": "Igfbp2",
            "mek1_ps217s221": "Mek1 (s217-s221)",
            "pkcalpha": "Pkc alpha",
            "pkcdelta_ps664": "Pkc delta (s644)",
            "rb_ps807s811": "RB (s807-s811)",
            "yap_ps127": "Yap (s127)",
            "yb1_ps102": "Yb1 (s102)",
            "gapdh": "GAPDH",
            "raptor": "Raptor",
            "rictor": "Rictor",
        }
    )
    stats_for_export.set_index("Protein", inplace=True)

    stats_for_export.to_csv(DST_DIR / "supplementary_table_6.csv")


def figure_3_panels_k_l_and_supplementary_figure_8_and_supplementary_table_6(
    fusobacteria: pandas.DataFrame
) -> None:
    """
    Generate figure 3 panels K-L, supplementary figure 8 and export supplementary table 6.
    Figure 3 panels K-L include a heatmap (K) displaying expression of proteins differentially expressed when comparing
    Fusobacteriales-high vs. low patients and corresponding pathway enrichment analysis (L) with the BioPlanet (version
    2019) and MSigDB Hallmark (version 2020) for patients of the TCGA-COAD-READ cohort.
    Supplementary figure 8 includes pathway enrichment analysis with the BioCarta (version 2016) and NCI-Nature
    (version 2016) pathway databases for genes differentially expressed by Fusobacteriales relative abundance in
    patients of the TCGA-COAD-READ cohort.
    Supplementary table 6 include statistical results testing the association between protein expression and
    Fusobacteriales relative abundance in the TCGA-COAD-READ patients.
    :param fusobacteria: dataframe, one row per patient as returned by `fusobacteria_utils.get_crc_fusobacteria()`.
    """
    tcga_rppa = _feature_transform(
        cohort.get_tcga_crc_cohort().get_rppa(standardization=False)
    )

    stats = _identify_significant_continuous_features(tcga_rppa, fusobacteria)
    significant_stats = stats.query("`p-corr` < 0.05")

    supplementary_table_6(significant_stats)

    plot_data = (
        tcga_rppa[significant_stats.index]
        .join(fusobacteria.fusobacteria_binary_class, how="inner")
        .set_index("fusobacteria_binary_class", append=True)
    )

    _plot_heatmap(plot_data, "k")
    _enrichment_analysis_per_molecular_layer(
        pathway_genes=tcga_pancancer.get_rppa_metadata.map_protein_id_to_gene_id()
        .loc[plot_data.columns]
        .to_list(),
        background=set(
            tcga_pancancer.get_rppa_metadata.map_protein_id_to_gene_id()
            .to_frame()
            .query("protein.isin(@tcga_rppa.columns)")
            .gene
        ),
        vmin=1e-5,
        vmax=1,
        main_name="figure_3_panel_l",
        supplement_name="supplementary_figure_8",
    )


def main():
    fusobacteria = fusobacteria_utils.get_crc_fusobacteria()

    figure_3_panels_h_i_and_supplementary_figure_7_and_supplementary_table_5(
        fusobacteria
    )  # gene expression layer
    figure_3_panels_k_l_and_supplementary_figure_8_and_supplementary_table_6(
        fusobacteria
    )  # protein expression layer


if __name__ == "__main__":
    DST_DIR.mkdir(parents=True, exist_ok=True)
    matplotlib.rcParams["pdf.fonttype"] = 42
    matplotlib.rcParams["font.size"] = 6
    matplotlib.rcParams["font.family"] = "Arial"
    matplotlib.rcParams["figure.dpi"] = 141

    main()
