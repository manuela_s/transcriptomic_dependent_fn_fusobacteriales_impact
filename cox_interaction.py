#!/usr/bin/env python3

"""Functions to fit and plot interaction Cox regression models."""

import collections
import typing

import lifelines
import matplotlib.axes
import pandas

import py_utils.add_n_in_group_names
import py_utils.compare_lr_test
import py_utils.cox


class InteractionCoxModels:
    """
    Helper class, modelled after cox.UnivariateCoxPerVariable and cox.MultivariateCox, to help generate and compare
    Cox interaction models, between a feature 'interaction_col' and a covariate feature.
    """

    def __init__(
        self,
        df: pandas.DataFrame,
        duration_col: str,
        event_col: str,
        interaction_col: str,
    ):
        """
        Contruct InteractionCoxModels.
        :param df: dataframe, one row per observation and columns for duration, event, interaction term and every
                   covariate to fit Cox models for;
        :param duration_col: name of the column in 'df' that holds the duration information;
        :param event_col: name of the column in 'df' that holds the event information (True for event, False for cesnored);
        :param interaction_col: name of column with feature to include in the interaction term for each covariate model.
        """
        self.interaction_col_ = interaction_col
        self.interaction_col_values_ = (
            df[interaction_col].astype("category").cat.categories.to_list()
        )
        assert (
            len(self.interaction_col_values_) == 2
        ), "InteractionCoxModels class requires an interaction column with two values"
        self.variables_ = [
            c for c in df.columns if c not in {duration_col, event_col, interaction_col}
        ]
        self.cox_ = collections.defaultdict(
            dict
        )  # Map variable names to a dict mapping operators to Cox models
        for v in self.variables_:
            for operator in ["*", "+", ":"]:
                # Set penalizer to avoid convergence issues when running with genes with insufficient events in groups.
                cph = lifelines.CoxPHFitter(penalizer=1e-4)
                cph.fit(
                    df,
                    duration_col=duration_col,
                    event_col=event_col,
                    formula=f"{v} {operator} {interaction_col}",
                )
                self.cox_[v][operator] = cph

    def summary(self, interaction_col_value: str) -> pandas.DataFrame:
        """
        Generate a dataframe with summary statistics for all the fitted Cox models.
        :param interaction_col_value: level value for interaction column to get interaction coefficients for;
        :return: dataframe, one row per each fitted model with summary statistics and columns as
                 from lifelines.CoxPHFitter.summary. Coefficients are for the interaction between the
                 'interaction_col_value' level of 'interaction_col' and the 'high' level of the covariate.
        """
        return pandas.concat(
            {
                v: self.cox_[v][":"].summary.loc[
                    f"{v}[T.high]:{self.interaction_col_}[{interaction_col_value}]"
                ]
                for v in self.variables_
            },
            axis=1,
        ).T

    def model_stats(self) -> pandas.DataFrame:
        """
        Generate a dataframe with per-model summary statistics for all the fitted Cox models.
        :return: dataframe, one row per each fitted model and columns including total number of observations ('n'),
                 interaction p-value ('interaction_pvalue'), loglikelihood ratio p-value ('lrt_pvalue') and interaction
                 hazard ratio ('interaction_hr').
        """
        return pandas.concat(
            [
                pandas.Series(
                    {v: self.cox_[v]["*"]._n_examples for v in self.variables_},
                    name="n",
                ),
                pandas.Series(
                    {
                        v: py_utils.compare_lr_test.compare_lr_test(
                            self.cox_[v]["*"], self.cox_[v]["+"]
                        )[1]
                        for v in self.variables_
                    },
                    name="interaction_pvalue",
                ),
                pandas.Series(
                    {
                        v: self.cox_[v][":"].log_likelihood_ratio_test().p_value
                        for v in self.variables_
                    },
                    name="lrt_pvalue",
                ),
                pandas.Series(
                    {
                        v: self.cox_[v]["*"].hazard_ratios_.loc[
                            f"{v}[T.high]:{self.interaction_col_}[T.{self.interaction_col_values_[1]}]"
                        ]
                        for v in self.variables_
                    },
                    name="interaction_hr",
                ),
            ],
            axis=1,
        )

    def plot(
        self,
        interaction_col_value: str,
        ax: matplotlib.axes.Axes,
        columns: typing.List[str] = None,
    ) -> None:
        """
        Plot interaction hazard ratio with corresponding 95% confidence interval for each fitted Cox model.
        :param interaction_col_value: level of interaction_col to plot coefficients for;
        :param ax: axes to plot into;
        :param columns: if given, include only interaction Cox regression models for the given columns in the original
                        dataframe and in the given order. By default, include interaction Cox regression models for all
                        the columns in the original dataframe.
        """

        summary = self.summary(interaction_col_value).join(self.model_stats())

        def annotate_significance(s: pandas.Series) -> str:
            if s.interaction_pvalue < 0.05:
                return f"{s.name} **"
            elif s.interaction_pvalue <= 0.1:
                return f"{s.name} *"
            else:
                return str(s.name)

        summary["label"] = summary.apply(annotate_significance, axis=1)

        if columns:
            summary = summary.loc[columns]

        xerr = pandas.concat(
            [
                summary["exp(coef)"] - summary["exp(coef) lower 95%"],
                summary["exp(coef) upper 95%"] - summary["exp(coef)"],
            ],
            axis=1,
        )
        ax.errorbar(
            summary["exp(coef)"],
            range(summary.shape[0]),
            xerr=xerr.T.values,
            fmt="o",
            markersize=5,
            color="k",
        )
        ax.vlines(
            1, -0.5, summary.shape[0] - 0.5, color="k", linestyle=":", linewidth=1
        )

        ax.set_xlabel("HR (95% CI)")
        ax.set_yticks(range(summary.shape[0]))
        ax.set_yticklabels(summary.label)
