#!/usr/bin/env python3

"""
Functions to prepare and plot data comparing expression of feature(s) in patients with low vs. high either Fusobacterium
nucleatum load (Taxonomy cohort) or Fusobacteriales relative abundance (TCGA-COAD-READ cohort)."""

import os

os.environ["OUTDATED_IGNORE"] = "1"

import pathlib
import textwrap
import typing
import warnings

import matplotlib.lines
import matplotlib.pyplot
import pandas

import pingouin
import seaborn

import constants
import py_utils.format_pvalue


def prepare_plot_data(expression: pandas.Series, gene_names: typing.List[str]):
    """Prepare data for plot comparing expression in patients with low vs. high either Fusobacterium nucleatum load
    (Taxonomy cohort) or Fusobacteriales relative abundance (TCGA-COAD-READ cohort).
    :param expression: series with one row per patient/feature, indices/columns must include 'fusobacteria_binary_class'
                       and 'feature' with values in 'expression';
    :param gene_names: list of features to plot.
    """
    plot_data = expression.to_frame().query("feature.isin(@gene_names)").reset_index()
    plot_data["feature"] = pandas.Categorical(plot_data.feature, gene_names)
    plot_data.feature.cat.remove_unused_categories(inplace=True)
    stats = (
        plot_data.groupby("feature")
        .apply(pingouin.kruskal, dv="expression", between="fusobacteria_binary_class")
        .reset_index([None])
    )
    stats.loc[:, "n"] = plot_data.groupby("feature").size()

    stats["feature_pretty_name"] = stats.apply(
        lambda x: textwrap.fill(
            "{} ({})".format(
                x.name.replace("_", " "),
                py_utils.format_pvalue.format_pvalue(x["p-unc"], pvalue_prefix="P"),
            ),
            12,
        ),
        axis=1,
    )
    plot_data = plot_data.join(stats.feature_pretty_name, on="feature")

    return plot_data


def gene_group_vs_fusobacteria_figure(
    expression: pandas.Series,
    figure_title: str,
    group_name: str,
    feature_names: typing.List[str],
    dst_dir: pathlib.Path,
):
    """Make figure with gene expression compared for high/low fusobacteria
    :param expression: series with one row per patient/feature, indices/columns must include 'fusobacteria_binary_class'
                       and 'feature' with values in 'expression';

    :param figure_title: label for cohort, used in figure title and filename;
    :param group_name: part of label for figure filename;
    :param feature_names: list of features to plot;
    :param dst_dir: destination dir to save figure to.
    """
    plot_data = prepare_plot_data(expression, feature_names)
    plot_data.sort_values(by="feature", inplace=True)

    fig_width = 0.7 + len(feature_names) * 2 + 0.3
    fig, ax = matplotlib.pyplot.subplots(
        figsize=(fig_width, 4),
        gridspec_kw=dict(
            top=0.9, bottom=0.15, left=0.7 / fig_width, right=1 - 0.3 / fig_width
        ),
    )
    ax = seaborn.violinplot(
        x="feature_pretty_name",
        y="expression",
        hue="fusobacteria_binary_class",
        hue_order=list(constants.FUSOBACTERIA_BIN_COLORS.keys()),
        palette=constants.FUSOBACTERIA_BIN_COLORS,
        split=True,
        legend=False,
        inner="quartile",
        data=plot_data,
    )
    for l in ax.findobj(matplotlib.lines.Line2D):
        if l.axes is None:
            continue
        l.set_zorder(3)
        l.set_linewidth(3)
    ax.set_xlabel("Molecular marker")
    ax.set_ylabel("Standardized expression")
    ax.set_title(group_name)
    ax.legend().remove()
    ax.set_ylim(-3, 3)

    fig.suptitle(f"{figure_title} (n={plot_data.patient_id.nunique()})", fontsize=12)

    fig.savefig(dst_dir / f"{group_name}_{figure_title.lower().replace('-', '_')}.pdf")


def gene_group_vs_fusobacteria_figure_separate_panels(
    expression: pandas.Series,
    figure_title: str,
    figure_filename: str,
    feature_names: typing.List[str],
    dst_dir: pathlib.Path,
    overall_y_label: str,
):
    """Make figure with separate axes for each panel comparing expression in patients with low vs. high either
    Fusobacterium nucleatum load (Taxonomy cohort) or Fusobacteriales relative abundance (TCGA-COAD-READ cohort).
    :param expression: series with one row per patient/feature, indices/columns must include 'fusobacteria_binary_class'
                       and 'feature' with values in 'expression';
    :param figure_title: label for cohort, used in figure title;
    :param figure_filename: label for figure filename;
    :param feature_names: list of features to plot;
    :param dst_dir: destination dir to save figure to;
    :param overall_y_label: overall y label for figure.
    """
    plot_data = prepare_plot_data(expression, feature_names)

    def plot_helper(data, color):
        ax = matplotlib.pyplot.gca()
        ax = seaborn.violinplot(
            x="feature_pretty_name",
            y="expression",
            hue="fusobacteria_binary_class",
            hue_order=list(constants.FUSOBACTERIA_BIN_COLORS.keys()),
            palette=constants.FUSOBACTERIA_BIN_COLORS,
            split=True,
            legend=False,
            inner="quartile",
            data=data,
        )
        for l in ax.findobj(matplotlib.lines.Line2D):
            if l.axes is None:
                continue
            l.set_zorder(3)
            l.set_linewidth(0.25)
        ax.set_xlabel("")
        ax.set_ylabel("")
        ax.legend().remove()

    with warnings.catch_warnings():
        # Fixed gridspec causes matplotlib tight_layout to issue warnings.
        warnings.simplefilter("ignore", UserWarning)
        g = seaborn.FacetGrid(
            col="feature_pretty_name",
            sharex=False,
            sharey=False,
            data=plot_data,
            height=2,
            aspect=0.3,
            gridspec_kws=dict(
                top=0.867, bottom=0.22, left=0.069, right=0.984, hspace=0.2, wspace=1.0
            ),
        )
        g.map_dataframe(plot_helper)

    g.set_titles("")
    g.axes[0, 0].set_ylabel(overall_y_label)
    g.fig.suptitle(f"{figure_title} (n={plot_data.patient_id.nunique()})", fontsize=8)

    g.fig.savefig(dst_dir / f'{figure_filename.lower().replace(" ", "_")}.pdf')
