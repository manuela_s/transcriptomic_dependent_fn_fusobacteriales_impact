#!/usr/bin/env python3

""" Generate figure 4 panels a, b, c and d.
Association between Fusobacterium nucleatum (Fn) load (Taxonomy cohort) and Fusobacteriales relative abundance
(TCGA-COAD-READ cohort) with transcriptomics-based Consensus Molecular Subtyping (CMS) and ColoRectal Intrinsic
Subtyping (CRIS)."""

import pathlib

import matplotlib
import matplotlib.lines
import matplotlib.pyplot
import matplotlib.ticker
import pandas
import seaborn

import cohort
import constants
import fusobacteria_utils
import py_utils.outlier_scale
import py_utils.rounding

DST_DIR = pathlib.Path("figures_and_tables")


# noinspection PyUnusedLocal
def _panel_helper(data: pandas.DataFrame, color, subtyping: str) -> None:
    """
    Plot panel with a boxplot with overlaid swarmplot.
    If outliers are detected, use a mixed linear/log scale for y-axis.
    :param data: dataframe, one row per patient and must include a column 'expression' with numerical values to plot;
    :param color: unused;
    :param subtyping: subtyping type, either 'cms' or 'cris'.
    """
    ax = matplotlib.pyplot.gca()

    subtyping_var = f"{subtyping}_class"
    subtyping_order = getattr(constants, f"{subtyping.upper()}_ORDER")
    subtyping_colors = getattr(constants, f"{subtyping.upper()}_COLORS")

    # Check for outliers (data-points more extreme than 1.5 times the inter-quartile range)
    q1, q3 = data.expression.quantile([0.25, 0.75])
    upper = 1.5 * (q3 - q1) + q3
    highest_non_outlier = data.expression[data.expression < upper].max()
    threshold = py_utils.rounding.round_up_to_n_significant_digits(
        highest_non_outlier, 2
    )
    if data.expression.max() > threshold * 1.5:
        # Use logarithmic scale for outliers
        ax.axhline(threshold, color="k", linestyle=":")
        ax.set_yscale("symlog", linthresh=threshold, linscale=3)
        ax.yaxis.set_major_locator(
            py_utils.outlier_scale.OutlierLogLocator(linthresh=threshold, base=10)
        )
        ax.yaxis.set_minor_locator(
            py_utils.outlier_scale.OutlierLogLocator(
                linthresh=threshold, base=10, subs=range(1, 9)
            )
        )
        ax.yaxis.set_major_formatter(matplotlib.ticker.StrMethodFormatter("{x:g}"))

    # Make boxplot with overlaying swarmplot color-coded by subtyping
    params = dict(
        x=subtyping_var,
        y="expression",
        order=subtyping_order,
        hue=subtyping_var,
        hue_order=subtyping_order,
        palette=subtyping_colors,
        data=data.reset_index(),
    )
    b = seaborn.boxplot(
        dodge=False, width=0.9, saturation=1, showfliers=False, **params
    )
    s = seaborn.swarmplot(size=2, **params)

    # Set transparency on boxplot fill color
    for patch in b.artists:
        r, g, b, a = patch.get_facecolor()
        patch.set_facecolor((r, g, b, 0.3))

    # Modify quartile line objects to be more visible
    for line in s.findobj(matplotlib.lines.Line2D):
        if line.axes is None:
            continue
        line.set_zorder(3)

    # Set x tick label with n-counts per subtype
    labels = (
        data.groupby(subtyping_var)
        .size()
        .to_frame("n")
        .apply(lambda x: f"{x.name}\n(n={x.n})", axis=1)
    )
    ax.set_xticklabels(labels[subtyping_order].values)


def panels_by_subtyping(
    data: pandas.DataFrame, subtyping: str, panel_labels: str
) -> None:
    """
    Plot Fusobacterium nucleatum load (Taxonomy cohort) and Fusobacteriales relative abundance (TCGA-COAD-READ) by
    subtyping.
    :param data: dataframe, one row per patient and must include the columns 'expression' and subtyping information.
    :param subtyping: subtyping type, either 'cms' or 'cris';
    :param panel_labels: panel labels to name figure.
    """
    seaborn.set(style="ticks", rc={"grid.linewidth": 0.1})
    seaborn.set_context("paper", font_scale=0.9)
    g = seaborn.FacetGrid(
        col="cohort",
        col_order=["taxonomy", "TCGA-COAD-READ"],
        sharex=False,
        sharey=False,
        data=data.reset_index(),
    )

    g.map_dataframe(_panel_helper, subtyping=subtyping)

    g.set_titles("{col_name}")
    g.set_axis_labels(
        x_var=f"{subtyping.upper()} subtype",
        y_var="Fusobacterium nucleatum (Fn) load or\n$Fusobacteriales$ relative abundance",
    )

    matplotlib.pyplot.subplots_adjust(
        left=0.1, bottom=0.15, right=0.95, top=0.95, wspace=0.3, hspace=0.25
    )
    matplotlib.pyplot.tight_layout()

    g.fig.savefig(DST_DIR / f"figure_4_panels_{panel_labels}.pdf")


def main():
    fusobacteria = fusobacteria_utils.get_crc_fusobacteria()
    crc = cohort.get_crc_cohorts()
    data = (
        fusobacteria.join(crc.get_clinical().join(crc.get_subtypes()))[
            ["cms_class", "cris_class", "fusobacteria"]
        ]
        .rename(columns={"fusobacteria": "expression"})
        .query('cohort.isin(["taxonomy", "TCGA-COAD-READ"])')
    )

    panels_by_subtyping(data, "cms", "a_c")
    panels_by_subtyping(data, "cris", "b_d")


if __name__ == "__main__":
    matplotlib.rcParams["pdf.fonttype"] = 42
    matplotlib.rcParams["font.size"] = 6
    pathlib.Path(DST_DIR).mkdir(parents=True, exist_ok=True)

    main()
