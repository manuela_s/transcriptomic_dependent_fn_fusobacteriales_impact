#!/usr/bin/env python3

""" Generate figure 6, supplementary figures 12, 13, and 14 and supplementary table 8.
Exploration of mechanism underlying differential impact of Fusobacterium nucleatum (load, Taxonomy cohort) and
Fusobacteriales (relative abundance, TCGA-COAD-READ cohort) in mesenchymal versus non-mesenchymal tumours."""

import pathlib
import re
import typing
import warnings

import adjustText
import matplotlib.cm
import matplotlib.colors
import matplotlib.patches
import matplotlib.pyplot
import pandas
import seaborn
import statsmodels.formula.api

import cohort
import constants
import cox_interaction
import fusobacteria_utils
import py_utils.compare_lr_test
import py_utils.cox
import py_utils.format_pvalue


FUSOBACTERIA_SUBTYPING_DST_DIR = pathlib.Path("figures_and_tables")

# review to get the data from https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6723901/
GENE_CLASSES = {
    "NOTCH": ["NOTCH1", "NOTCH2"],
    "EMT": [
        "rna_emt_score",
        "SNAI1",
        "SNAI2",
        "TWIST1",
        "TWIST2",
        "TWISTNB",
        "ZEB1",
        "ZEB2",
    ],
    "WNT": ["rna_wnt_score", "BMP2", "BMP5", "FRZB", "POSTN", "WNT5A"]
    + ["FABP1", "ID1", "ID3"],
    "Secretion, migration and metastasis": [
        "rna_metastasis_score",
        "TFF1",
        "TFF2",
        "SPINK4",
    ],
    "Cell cycle": ["KIAA0101", "STMN1"],
    "DNA damage": ["rna_dna_damage_score"],
    "Cell types": [
        "LGR5",
        "ASCL2",
        "OLFM4",
        "MKI67",
        "PCNA",
        "GUCA2B",
        "SLC26A3",
        "TFF3",
        "SPINK1",
        "REG4",
        "AGR2",
    ],
    "Fibroblast": ["COL3A1", "DCN", "THY1"],
    "Endothelial": ["ENG", "PECAM1"],
    "Immuno": ["CD24", "CD38", "CD68", "HLA-DRA"],
    "T cells chemotactism and activation": [
        "CXCL9",
        "CXCL10",
        "CXCL16",
        "IFNG",
        "IL15",
    ],
    "Activated T cells": [
        "LTB",
        "TNFRSF4",
        "TNFRSF18",
        "TRBC2",
        "CD3E",
        "CD7",
    ],  # From https://pubmed.ncbi.nlm.nih.gov/31217164/#&gid=article-figures&pid=figure-3-uid-2
    "Activated NK cells": [
        "CTSW",
        "HOPX",
        "GNLY",
        "GZMB",
        "GZMA",
        "CCL5",
        "IL32",
    ],  # From https://pubmed.ncbi.nlm.nih.gov/31217164/#&gid=article-figures&pid=figure-3-uid-2
    "B cells chemotactism and TLS": ["CXCL13", "CCL19", "CCL21", "CCR7", "CXCR5"],
    "Inflammation signatures": [
        "rna_tis_score",
        "rna_ifng_score",
        "rna_cytolytic_score",
    ],
    "Inflammation": [
        "NFKB1",
        "TNF",
        "LTA",
        "IL6",
        "IL6R",
        "IL8",
        "IL10",
        "IL13",
        "IL1B",
        "CD40LG",
        "CD70",
        "FASLG",
        "TNFSF8",
        "TNFRSF9",
        "TNFSF10",
        "TNFSF13B",
        "IRF1",
        "IRF2",
        "IRF3",
        "IRF4",
        "IRF5",
        "IRF6",
        "IRF7",
        "IRF8",
        "IRF9",
    ],
    "Pro inflammation": ["IL1B", "S100A8", "S100A9"],
    "Anti inflammation": ["CD163", "SEPP1", "APOE", "MAF"],
    "Metallo proteins": ["MMP1", "MMP3", "MMP9"],
    "Immunosuppression": ["TGFB1", "TGFB3", "LGALS1", "IL10"],
    "Inflammasome": ["NLRP3", "CASP1", "GSDMD"],
    "Angiogenesis": ["VEGFB", "VEGFC", "PDGFC"],
    "Complement system": [
        "C1QA",
        "C1QB",
        "C1QC",
        "C1R",
        "C1S",
        "C3AR1",
        "C5AR1",
        "C7",
        "CFD",
        "CFI",
        "CFH",
    ],
    "Macrophage chemotactism and activation": [
        "CXCL1",
        "CXCL2",
        "CXCL3",
        "CXCL5",
        "CXCL6",
        "CXCR1",
        "CXCR2",
        "CSF1",
        "CSF1R",
        "CSF2",
        "CSF2RA",
        "CSF3",
        "IL8",
    ],  # Missing  'CXCL7'
    "Antigen presentation": [
        "HLA-A",
        "HLA-B",
        "HLA-C",
        "HLA-E",
        "HLA-F",
        "HLA-G",
        "B2M",
    ],
    "Other pathways": [
        "rna_proliferation_score",
        "rna_hippo_score",
        "rna_hypoxia_score",
        "rna_stemness_score",
        "CASP1",
        "CASP3",
        "CASP8",
        "CASP9",
        "CFLAR",
        "XIAP",
        "DIABLO",
        "COX2",
        "NOS2",
        "NLRP3",
        "CCL8",
        "CXL8",
    ],
}


def stats_helper(s: pandas.Series, grouping_var: str) -> pandas.Series:
    """
    Compute statistics for a gene or signature.
    :param s: series, gene expression or signature score to run statistics for;
    :param grouping_var: name of an index in s to group by;
    :return: series with statistical results.
    """
    stats_data = (
        s.to_frame("gene")
        .assign(
            fusobacteria_binary_class=lambda x: (
                x.index.get_level_values("fusobacteria_binary_class") == "high"
            ).astype(int)
        )
        .reset_index(grouping_var)
    )

    # Association between gene and fusobacteria
    gene_model = statsmodels.formula.api.logit(
        formula="fusobacteria_binary_class ~ gene".format(grouping_var), data=stats_data
    ).fit(disp=0)
    baseline_model = statsmodels.formula.api.logit(
        formula="fusobacteria_binary_class ~ 1".format(grouping_var), data=stats_data
    ).fit(disp=0)
    gene_pvalue = py_utils.compare_lr_test.compare_lr_test(gene_model, baseline_model)[
        1
    ]

    # Association between gene/grouping_var interaction and fusobacteria
    interaction_model = statsmodels.formula.api.logit(
        formula="fusobacteria_binary_class ~ {} + gene:{}".format(
            grouping_var, grouping_var
        ),
        data=stats_data,
    ).fit(disp=0)
    no_interaction_model = statsmodels.formula.api.logit(
        formula="fusobacteria_binary_class ~ gene + {}".format(grouping_var),
        data=stats_data,
    ).fit(disp=0)
    interaction_pvalue = py_utils.compare_lr_test.compare_lr_test(
        interaction_model, no_interaction_model
    )[1]

    grouping_var_coefficients = dict(
        (re.match("gene:.*\\[(.*)]", k).groups()[0], v)
        for k, v in interaction_model.params.loc[
            interaction_model.params.index.str.startswith("gene:")
        ].items()
    )

    return pandas.concat(
        {
            "pvalues": pandas.Series(
                {"Overall": gene_pvalue, "interaction": interaction_pvalue}
            ),
            "coefficients": pandas.Series(
                {"Overall": gene_model.params.loc["gene"], **grouping_var_coefficients}
            ),
        },
        names=["stat", "level"],
    )


def plot_interaction_stats(pvalues: pandas.DataFrame, pvalue_cutoff: float) -> None:
    """
    Plot Fig. 6A (main manuscript).
    Scatter plot of P-values for gene/signature association with Fusobacteriales RA and P-values for interaction association
    between gene/signature and grouping_var with Fusobacteriales relative abundance (RA) in the TCGA-COAD-READ cohort.
    :param pvalues: dataframe,
        - one row per gene/signature;
        - indices include 'gene', 'gene_class', 'gene_or_signature';
        - columns include 'Overall' and 'interaction';
    :param pvalue_cutoff: p-value cut-off, used to annotate the plot.
    """
    plot_data = pvalues.reset_index(["gene_class", "gene_or_signature"])

    fig = matplotlib.pyplot.figure(figsize=(7, 4))
    ax = seaborn.scatterplot(
        x="Overall",
        y="interaction",
        hue="gene_class",
        style="gene_or_signature",
        markers="os",
        linewidth=1,
        edgecolor="k",
        palette=constants.GENES_OR_SIGNATURES,
        data=plot_data,
    )
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.invert_xaxis()
    ax.invert_yaxis()
    line_params = dict(linewidth=1, color="k", linestyle=":")
    ax.axvline(pvalue_cutoff, **line_params)
    ax.axhline(pvalue_cutoff, **line_params)

    ax.legend(loc="center left", bbox_to_anchor=(1, 0.5), fancybox=True, shadow=True)

    ax.set_xlabel("P-value for gene/signature association\nwith $Fusobacteriales$ RA")
    ax.set_ylabel(
        f"P-value for interaction association\nbetween gene/signature and mesenchymal status\nwith $Fusobacteriales$ RA"
    )

    matplotlib.pyplot.tight_layout()

    xlim = ax.get_xlim()
    ylim = ax.get_ylim()

    matplotlib.pyplot.annotate(
        "Interaction\n P<{} ".format(pvalue_cutoff),
        (1.1 * xlim[1], pvalue_cutoff),
        verticalalignment="top",
        horizontalalignment="right",
    )
    matplotlib.pyplot.annotate(
        " P<{}".format(pvalue_cutoff),
        (pvalue_cutoff, 1.1 * ylim[1]),
        verticalalignment="top",
        horizontalalignment="left",
    )

    annotations = [
        matplotlib.pyplot.annotate(
            row.Index,
            (row.Overall, row.interaction),
            fontsize="small",
            arrowprops={"arrowstyle": "->"},
        )
        for row in plot_data.query("interaction < @pvalue_cutoff").itertuples()
    ]
    adjustText.adjust_text(
        annotations,
        plot_data.Overall.values,
        plot_data.interaction.values,
        precision=0.0001,
    )

    # Add a patch highlighting significant quadrant
    rect = matplotlib.patches.Rectangle(
        (xlim[0], pvalue_cutoff),
        xlim[1] - xlim[0],
        ylim[1] - pvalue_cutoff,
        linewidth=1,
        edgecolor="#bdbdbd",
        facecolor="#bdbdbd",
        alpha=0.2,
    )
    ax.add_patch(rect)

    fig.savefig(FUSOBACTERIA_SUBTYPING_DST_DIR / "figure_6_panel_a.pdf")


def plot_gene_expression_or_signature_score_by_mesenchymal_status_and_cohort(
    significant_stats: pandas.DataFrame, n_counts: pandas.Series, pvalue_cutoff: float
) -> None:
    """
    Plot Fig. 6B (main manuscript).
    Breakdown of association including direction, effect size, in the unselected patients' population and within cases
    grouped by 'grouping_var'. Only gene/signatures with significant interaction identified in the TCGA-COAD-READ cohort
    between Fusobacteriales RA and the gene/signature interaction with 'grouping_var' are included. Associations for both
    the TCGA-COAD-READ (Fusobacteriales RA) and Taxonomy (Fn load) cohorts are included.
    :param significant_stats: dataframe,
        - one row per cohort and per gene/signature identified as statistically significant in the TCGA-COAD-READ cohort;
        - indices include 'cohort', 'gene', 'gene_class', 'gene_or_signature';
        - columns as returned by 'stats_helper';
    :param n_counts: series, with number of patients per 'cohort'/'level';
    :param pvalue_cutoff: -value cut-off, used to annotate the plot.
    """
    plot_data_1 = significant_stats.copy().reset_index("gene_or_signature", drop=True)
    plot_data_1[("significance", "Overall")] = plot_data_1.pvalues.Overall.lt(
        pvalue_cutoff
    )
    plot_data_2 = pandas.DataFrame(
        significant_stats.pvalues.interaction.lt(pvalue_cutoff)
        .values.reshape([-1, 1])
        .repeat(2, 1),
        columns=pandas.MultiIndex.from_product(
            [["significance"], significant_stats.coefficients.columns[1:]]
        ),
        index=significant_stats.index,
    )
    pretty_cohort = (
        n_counts.index.to_frame().cohort + " (n=" + n_counts.astype(str) + ")"
    )
    plot_data_3 = (
        plot_data_1.join(plot_data_2)[["significance", "coefficients"]]
        .stack("level")
        .join(pretty_cohort.rename("pretty_cohort"))
        .reset_index()
    )

    norm = matplotlib.colors.Normalize(vmin=-1.125, vmax=1.125, clip=True)
    # noinspection PyProtectedMember
    cmap = matplotlib.cm.RdBu_r._resample(9)
    height_ratios = plot_data_3.gene_class.value_counts(sort=False)

    with warnings.catch_warnings():
        # Fixed gridspec causes matplotlib tight_layout to issue warnings.
        warnings.simplefilter("ignore", UserWarning)
        g = seaborn.relplot(
            x="gene",
            y="pretty_cohort",
            hue="coefficients",
            palette=cmap,
            hue_norm=norm,
            legend=False,
            edgecolor="k",
            markers="os",
            style="significance",
            style_order=[True, False],
            row="level",
            row_order=["Overall", "no", "yes"],
            col="gene_class",
            data=plot_data_3,
            facet_kws=dict(
                sharex="col",
                sharey="row",
                gridspec_kws=dict(
                    top=0.85,
                    bottom=0.5,
                    left=0.20,
                    right=0.95,
                    hspace=0.4,
                    wspace=0.4,
                    width_ratios=height_ratios,
                ),
                despine=False,
                margin_titles=True,
            ),
            height=0.8,
            aspect=0.65,
        )

    g.set_titles(row_template="{row_name}", col_template="{col_name}")

    # Enable text wrapping for the titles in the first row:
    for ax in g.axes[0]:
        ax.title._get_wrap_line_width = lambda: ax.get_window_extent().width
        ax.title.set_wrap(True)

    g.set_xlabels("")
    g.set_ylabels("")

    g.fig.canvas.draw()  # Force rendering to get xticklabels placed
    g.set_xticklabels(rotation=90)

    # Adjust x and y limits
    for ax in g.axes.flat:
        ax.set_xlim(-0.5, max(ax.get_xticks()) + 0.5)
        ax.set_ylim(-0.5, max(ax.get_yticks()) + 0.5)
        ax.invert_yaxis()

    cbar_ax = g.fig.add_axes([0.2, 0.15, 0.95 - 0.2, 0.025])
    matplotlib.pyplot.colorbar(
        matplotlib.cm.ScalarMappable(norm=norm, cmap=cmap),
        orientation="horizontal",
        label="Association between gene expression and $Fusobacteriales$ RA\n"
        f"Gene expression coefficient (Overall) or gene:mesenchymal status interaction coefficient",
        cax=cbar_ax,
    )

    g.savefig(FUSOBACTERIA_SUBTYPING_DST_DIR / "figure_6_panel_b.pdf")


def export_supplementary_table_8(stats_w: pandas.DataFrame) -> None:
    """
    Export csv file for supplementary table 8.
    :param stats_w: dataframe with one row per cohort/gene and columns
                    - pvalues.Overall
                    - pvalues.interaction
                    - coefficients.Overall
                    - coefficients.no
                    - coefficients.yes
    """
    stats_w = stats_w.copy()

    # Replace values in index:
    gene_map = {
        "rna_cytolytic_score": "Cytolytic score",
        "rna_dna_damage_score": "DNA damage score",
        "rna_emt_score": "EMT score",
        "rna_hippo_score": "Hippo score",
        "rna_hypoxia_score": "Hypoxia score",
        "rna_ifng_score": "IFN-gamma score",
        "rna_metastasis_score": "Metastasis score",
        "rna_proliferation_score": "Proliferation score",
        "rna_stemness_score": "Stemness score",
        "rna_tis_score": "TIS score",
        "rna_wnt_score": "WNT score",
    }
    stats_w.index = pandas.MultiIndex.from_frame(
        stats_w.index.to_frame().assign(
            cohort=lambda x: x.cohort.replace({"taxonomy": "Taxonomy"}),
            gene=lambda x: x.gene.replace(gene_map),
        )
    )

    # Sort rows by TCGA-COAD-READ interaction p-value, and reorder indices:
    stats_w = (
        stats_w.unstack("cohort")
        .sort_values([("pvalues", "interaction", "TCGA-COAD-READ")])
        .stack("cohort")
        .reorder_levels(["cohort", "gene", "gene_or_signature", "gene_class"])[
            stats_w.columns
        ]
        .sort_index(level="cohort", sort_remaining=False)
    )

    stats_w.rename_axis(
        columns={"stat": "Statistical result", "level": "Level"}, inplace=True
    )
    stats_w.rename_axis(
        index={
            "cohort": "Cohort",
            "gene": "Gene or signature",
            "gene_class": "Gene or signature class",
            "gene_or_signature": "Gene or signature?",
        },
        inplace=True,
    )
    stats_w.rename(
        columns={
            "pvalues": "P-value",
            "interaction": "Interaction",
            "coefficients": "Coefficient",
            "no": "Non-mesenchymal tumours",
            "yes": "Mesenchymal tumours",
        },
        inplace=True,
    )

    stats_w.loc[:, ("P-value", "Overall")] = stats_w.loc[
        :, ("P-value", "Overall")
    ].apply(py_utils.format_pvalue.format_pvalue)
    stats_w.loc[:, ("P-value", "Interaction")] = stats_w.loc[
        :, ("P-value", "Interaction")
    ].apply(py_utils.format_pvalue.format_pvalue)
    stats_w.to_csv(FUSOBACTERIA_SUBTYPING_DST_DIR / "supplementary_table_8.csv")


def identify_and_plot_genes_or_signatures_associated_with_mesenchymal_status(
    data: pandas.DataFrame, pvalue_cutoff: float = 0.05
) -> typing.List[str]:
    """
    Identifies gene or signatures with a statistically significant interaction between Fusobacteriales RA and grouping_var
    in the TCGA-COAD-READ cohort and corresponding plots for the TCGA-COAD-READ and Taxonomy cohorts.
    :param data: dataframe, one row per patient, indices must include 'cohort' and the grouping_var and one column for
    each gene or signature;
    :param pvalue_cutoff: pvalue cut-off to consider results as statistically significant. Default to 0.05;
    :return:
    """
    stats = (
        data.rename_axis(columns="gene")
        .stack()
        .groupby(["cohort", "gene"])
        .apply(stats_helper, "is_mesenchymal")
    )
    gene_metadata = (
        pandas.Series(
            dict(
                sum(
                    [
                        [(gene, gene_class) for gene in genes]
                        for (gene_class, genes) in GENE_CLASSES.items()
                    ],
                    [],
                )
            )
        )
        .astype(pandas.CategoricalDtype(GENE_CLASSES.keys()))
        .to_frame("gene_class")
    )

    gene_metadata["gene_or_signature"] = (
        gene_metadata.index.to_series()
        .str.endswith("_score")
        .map({True: "signature", False: "gene"})
    )
    stats_w = (
        stats.to_frame("value")
        .join(gene_metadata, on="gene")
        .set_index(["gene_class", "gene_or_signature"], append=True)
        .value.unstack(["stat", "level"])
    )
    export_supplementary_table_8(stats_w)

    significant_genes = (
        stats_w.loc["TCGA-COAD-READ"]
        .pvalues.query("interaction < @pvalue_cutoff")
        .index.get_level_values("gene")
        .to_list()
    )
    significant_stats = stats_w.query("gene.isin(@significant_genes)")
    significant_stats.reset_index("gene_class", inplace=True)
    significant_stats.gene_class.cat.remove_unused_categories(inplace=True)
    significant_stats.set_index("gene_class", append=True, inplace=True)
    n_counts = (
        pandas.concat(
            [
                data.assign(level=data.index.get_level_values("is_mesenchymal")),
                data.assign(level="Overall"),
            ]
        )
        .groupby(["cohort", "level"])
        .size()
    )

    plot_interaction_stats(stats_w.loc["TCGA-COAD-READ"].pvalues, pvalue_cutoff)
    plot_gene_expression_or_signature_score_by_mesenchymal_status_and_cohort(
        significant_stats, n_counts, pvalue_cutoff
    )

    return significant_genes


# noinspection DuplicatedCode
def plot_association_between_genes_or_signatures_and_survival(
    data: pandas.DataFrame,
    gene_columns: typing.List[str],
    survival_type: str,
    figue_label: str,
) -> pandas.DataFrame:
    """
    Make figure that show coefficients from Cox interaction models in mesenchymal patients of the TCGA-COAD-READ cohort
    between Fusobacteriales RA and gene/signatures found to be statistically significant associated in analysis in Fig. 6A.
    :param data: dataframe with one row per patient and columns:
      'fusobacteria_binary_class', survival_time, survival_event and one column per gene/signature;
    :param gene_columns: names of columns in data that have gene/signature expressions;
    :param survival_type: prefix for survival_time, survival_event columns;
    :param figure_label: label for figure;
    :return: dataframe, one row per gene/signature and columns 'n', 'interaction_pvalue', 'lrt_pvalue', 'interaction_hr'.
    """
    survival_time = "{}_months".format(survival_type)
    survival_event = "{}_event".format(survival_type)
    data = (
        data[["fusobacteria_binary_class", survival_time, survival_event]]
        .join(data[gene_columns].apply(pandas.qcut, q=2, labels=["low", "high"]))
        .copy()
        .dropna()
    )

    cox_all = py_utils.cox.UnivariateCoxPerVariable()
    cox_all.fit(
        data[gene_columns + [survival_time, survival_event]].replace(
            {"low": 0, "high": 1}
        ),
        survival_time,
        survival_event,
    )

    interactions = cox_interaction.InteractionCoxModels(
        data, survival_time, survival_event, "fusobacteria_binary_class"
    )

    genes_sorted_order = (
        interactions.model_stats()
        .sort_values(by=["interaction_hr"], ascending=False)
        .index.to_list()
    )

    # noinspection PyTypeChecker
    fig, axes = matplotlib.pyplot.subplots(
        ncols=3, sharex=True, sharey=True, figsize=(7, 5)
    )
    cox_all.plot(ax=axes[0], columns=genes_sorted_order)
    interactions.plot("low", ax=axes[1], columns=genes_sorted_order)
    interactions.plot("high", ax=axes[2], columns=genes_sorted_order)

    axes[0].set_xlim(0.1, 10)
    axes[0].set_title(f"Unselected\npatients\n(n={data.shape[0]})")
    axes[1].set_title(
        f'Patients with\nlow $Fusobacteriales$\n(n={data.fusobacteria_binary_class.value_counts()["low"]})'
    )
    axes[2].set_title(
        f'Patients with\nhigh $Fusobacteriales$\n(n={data.fusobacteria_binary_class.value_counts()["high"]})'
    )

    axes[0].invert_yaxis()
    axes[0].set_ylabel("Gene or signatures")

    axes[0].set_xlabel("")
    axes[2].set_xlabel("")

    axes[0].yaxis.tick_right()
    axes[1].yaxis.tick_right()
    axes[2].yaxis.tick_right()
    axes[0].yaxis.set_tick_params(which="both", labelleft=False, labelright=False)
    axes[1].yaxis.set_tick_params(which="both", labelleft=False, labelright=False)
    axes[2].yaxis.set_tick_params(which="both", labelleft=False, labelright=True)

    matplotlib.pyplot.tight_layout()
    fig.savefig(FUSOBACTERIA_SUBTYPING_DST_DIR / f"{figue_label}_{survival_type}.pdf")

    return interactions.model_stats()


def plot_interaction_cox_stats_summary(plot_data: pandas.DataFrame) -> None:
    """
    Plot summary statistics for interaction models between Fusobacteriales and gene/signatures identified in Fig. 6A
    fitted on mesenchymal patients of the TCGA-COAD-READ cohort for clinical endpoints ('OS', 'DSS' and 'DFS').
    :param plot_data: dataframe as returned by plot_association_between_genes_or_signatures_and_survival.
    """
    plot_data = plot_data.copy()
    plot_data.reset_index("endpoint", inplace=True)
    plot_data["endpoint"] = pandas.Categorical(plot_data.endpoint, ["os", "dss", "dfs"])
    plot_data = plot_data.sort_values("endpoint")
    plot_data["Clinical endpoint"] = (
        plot_data.endpoint.str.upper() + " (n=" + plot_data.n.astype(str) + ")"
    ).astype("category")
    plot_data["is_significant"] = pandas.cut(
        plot_data.interaction_pvalue, [0, 0.05, 0.1, 1], right=False
    )
    plot_data = plot_data.sort_values("interaction_hr", ascending=False)

    hue_norm = matplotlib.colors.LogNorm(vmin=0.25, vmax=4)
    cmap = "RdYlBu_r"

    fig, ax = matplotlib.pyplot.subplots(
        figsize=(7, 1.5), gridspec_kw=dict(top=0.92, bottom=0.5, left=0.11, right=0.88)
    )
    seaborn.scatterplot(
        x="Gene or signature",
        y="Clinical endpoint",
        hue="interaction_hr",
        hue_norm=hue_norm,
        palette=cmap,
        style="is_significant",
        markers="os^",
        edgecolor="k",
        linewidth=0.5,
        data=plot_data,
        s=70,
        zorder=10,
        ax=ax,
    )
    ax.set_xlim(
        -0.5, plot_data.index.get_level_values("Gene or signature").nunique() - 0.5
    )
    ax.set_ylim(-0.5, plot_data["Clinical endpoint"].nunique() - 0.5)
    ax.invert_yaxis()
    matplotlib.pyplot.xticks(rotation=90)
    ax.grid(linestyle=":")
    cax = matplotlib.pyplot.axes([0.90, 0.04, 0.01, 0.46])
    cbar = fig.colorbar(
        matplotlib.cm.ScalarMappable(norm=hue_norm, cmap=cmap),
        cax=cax,
        label="Interaction model\nHazard Ratio",
    )
    cbar.set_ticks([0.25, 0.5, 1, 2, 4])
    cbar.set_ticklabels(["1/4X", "1/2X", "1X", "2X", "4X"])
    handles, labels = ax.get_legend_handles_labels()
    # Redraw legend without the HR
    ax.legend_.remove()
    ax.legend(
        handles[6:],
        labels[6:],
        title="Interaction model\nP-value",
        bbox_to_anchor=(1.02, 1),
        loc="upper left",
        borderaxespad=0,
    )
    fig.savefig(FUSOBACTERIA_SUBTYPING_DST_DIR / "figure_6_panel_c.pdf")


def main():
    crc = cohort.get_crc_cohorts()

    subtypes = crc.get_subtypes()
    rna = crc.get_rna()
    signatures = crc.get_rna_based_signatures()

    fusobacteria = fusobacteria_utils.get_crc_fusobacteria().query(
        'cohort.isin(["TCGA-COAD-READ", "taxonomy"])'
    )

    data = (
        subtypes.dropna()
        .join(
            rna[set(sum(GENE_CLASSES.values(), [])).intersection(rna.columns)],
            how="inner",
        )
        .join(signatures, how="inner")
        .join(fusobacteria, how="inner")
    )
    data.sort_values("fusobacteria", inplace=True)
    data.set_index(
        [
            "cms_class",
            "cris_class",
            "is_mesenchymal",
            "fusobacteria_binary_class",
            "fusobacteria",
        ],
        append=True,
        inplace=True,
    )

    significant_genes = identify_and_plot_genes_or_signatures_associated_with_mesenchymal_status(
        data
    )

    data = (
        data.query('cohort=="TCGA-COAD-READ" and is_mesenchymal=="yes"')
        .reset_index(
            ["technology", "cms_class", "cris_class", "fusobacteria"], drop=True
        )[significant_genes]
        .reset_index("fusobacteria_binary_class")
        .join(
            crc.get_clinical()[
                [
                    "os_months",
                    "os_event",
                    "dss_months",
                    "dss_event",
                    "dfs_months",
                    "dfs_event",
                ]
            ]
        )
    )

    cox_results = {}
    for survival_type, figure_label in zip(
        ["os", "dss", "dfs"],
        [
            "supplementary_figure_12",
            "supplementary_figure_13",
            "supplementary_figure_14",
        ],
    ):
        cox_results[
            survival_type
        ] = plot_association_between_genes_or_signatures_and_survival(
            data, significant_genes, survival_type, figure_label
        )
    cox_results = pandas.concat(cox_results, names=["endpoint"]).rename_axis(
        index={None: "Gene or signature"}
    )
    plot_interaction_cox_stats_summary(cox_results)


if __name__ == "__main__":
    matplotlib.rcParams["figure.dpi"] = 141
    matplotlib.rcParams["pdf.fonttype"] = 42
    matplotlib.rcParams["font.family"] = "Arial"
    matplotlib.rcParams["font.size"] = 6
    pathlib.Path(FUSOBACTERIA_SUBTYPING_DST_DIR).mkdir(parents=True, exist_ok=True)

    main()
