#!/usr/bin/env python3

"""Functions to read in and handle Fusobacterium nucleatum load for the Taxonomy cohort and/or relative abundance
of Fusobacteriales and higher resolution taxonomic ranks for the TCGA-COAD-READ cohort."""

import typing

import pandas

import pathseq_utils
import taxonomy_cohort

# Group genuses/species with relative abundance less than REL_ABUNDANCE_CUTOFF into 'Other'.
# Set high enough to cut out numerical sub-species, low enough to include all key species.
REL_ABUNDANCE_CUTOFF = 0.05


def add_column_index(
    df: pandas.DataFrame, name: str, value: typing.Any
) -> pandas.DataFrame:
    """
    Helper function to add an extra level to multi-level column index.
    :param df: dataframe to add new column level to;
    :param name: name of new column level;
    :param value: value to set in new column level;
    :return: dataframe with new level added to column headers.
    """
    return df.T.set_index(
        pandas.Series([value] * df.shape[1], name=name), append=True
    ).T


def get_relative_abundance_at_multiple_resolution_taxonomic_ranks() -> pandas.DataFrame:
    """
    Get Fusobacteriales relative abundance at multiple taxonomic ranks.
    :return: dataframe with index 'patient_id' columns with relative abundances for each 'order', 'family', 'genus' and
             'species' in the Fusobacteriales order. Multi-level column index with levels 'family', 'genus', 'species'
             and 'level'. Species or genuses with relative abundance lower than REL_ABUNDANCE_CUTOFF are grouped
             together as 'Other'.
    """
    species = pathseq_utils.get_fusobacteriales_species()
    species.reset_index(["technology", "cohort"], drop=True, inplace=True)

    # Get taxonomy data for species of the Fusobacteriales order
    taxonomy = pathseq_utils.get_taxonomy_metadata()
    taxonomy.query(
        'taxonomy.str.startswith("root|cellular_organisms|Bacteria|Fusobacteria") and type=="species"',
        inplace=True,
    )
    taxonomy = taxonomy.join(
        taxonomy.taxonomy.str.extract(
            "^root\|cellular_organisms\|Bacteria\|Fusobacteria\|(?P<class>[^|]+)\|"
            "(?P<order>[^|]+)\|(?P<family>[^|]+)\|(?P<genus>[^|]+)"
        )
    )

    # Combine relative abundance with taxonomy classification
    data = (
        species.rename_axis(columns="species")
        .T.join(taxonomy.set_index("name")[["family", "genus"]])
        .set_index(["family", "genus"], append=True)
        .reorder_levels(["family", "genus", "species"])
        .T.rename_axis(index="patient_id")
    )

    per_order = data.sum(axis=1)
    per_family = data.groupby(["family"], axis=1).sum()
    per_genus = data.groupby(["family", "genus"], axis=1).sum()
    # Combine genuses with low abundance as 'Other'
    genuses_to_show = per_genus.mean().to_frame("value").gt(REL_ABUNDANCE_CUTOFF)
    other_genuses = add_column_index(
        per_genus.loc[:, ~genuses_to_show.values].groupby("family", axis=1).sum(),
        "genus",
        "Other",
    )
    per_genus = pandas.concat(
        [per_genus.loc[:, genuses_to_show.values], other_genuses], axis=1
    )

    species_in_genuses_to_show = data.T.join(
        genuses_to_show.loc[genuses_to_show.values][[]], how="inner"
    ).T
    species_to_show = (
        species_in_genuses_to_show.mean().to_frame("value").gt(REL_ABUNDANCE_CUTOFF)
    )
    per_species = pandas.concat(
        [
            data.loc[
                :,
                species_in_genuses_to_show.loc[:, species_to_show.values]
                .mean()
                .sort_values(ascending=False)
                .index,
            ],
            add_column_index(
                species_in_genuses_to_show.loc[:, ~species_to_show.values]
                .groupby(["family", "genus"], axis=1)
                .sum(),
                "species",
                "Other",
            ),
            add_column_index(other_genuses, "species", "Other"),
        ],
        axis=1,
    )

    combined = pandas.concat(
        {
            "order": per_order.rename((None, None, None))
            .to_frame()
            .rename_axis(columns=["family", "genus", "species"]),
            "family": add_column_index(
                add_column_index(per_family, "genus", None), "species", None
            ),
            "genus": add_column_index(per_genus, "species", None),
            "species": per_species,
        },
        names=["level"],
        sort=False,
        axis=1,
    ).reorder_levels(["family", "genus", "species", "level"], axis=1)

    def label_column(c):
        return {
            "order": "Fusobacteriales",
            "family": f" {c.family}",
            "genus": f"  {c.genus}",
            "species": f"   {c.species}",
        }[c.level]

    combined.sort_index(axis=1, level=[0, 1], sort_remaining=False, inplace=True)

    return combined.T.set_index(
        combined.columns.to_frame().apply(label_column, axis=1).rename("label"),
        append=True,
    ).T


def bin_fusobacteria(
    data_series: pandas.Series, percentile_cutoff: float = 0.75
) -> pandas.Series:
    """
    Binarize data into 'low' and 'high' groups using a percentile cut-off.
    Data to binarize are either load of Fusobacterium nucleatum for the Taxonomy cohort or relative abundance of
    Fusobacteriales and higher resolution taxonomic ranks for the TCGA-COAD-READ cohort.
    :param: series with continuous data to binarize;
    :percentile_cutoff: percentile cut-off to use to binarize.
    """
    cutoff = data_series.quantile(percentile_cutoff)
    return pandas.cut(
        data_series, [0, cutoff, 100], include_lowest=True, labels=["low", "high"]
    )


def get_crc_fusobacteria() -> pandas.DataFrame:
    """
    Get Fusobacterium nucleatum load for the Taxonomy cohort and relative abundance of Fusobacteriales for the
    TCGA-COAD-READ cohort. Return the data under the umbrella term of 'fusobacteria' and 'fusobacteria_binary_class'
    for the continuous and binarized forms, respectively.
    :return: tall dataframe, one row per patient with indices: 'technology', 'cohort' and 'patient_id', and columns:
             'fusobacteria' and 'fusobacteria_binary_class'.
    """
    fusobacteria = pandas.concat(
        [
            pathseq_utils.get_fusobacteriales_order().rename(
                columns={"fusobacteriales": "fusobacteria"}
            ),
            taxonomy_cohort.get_data.fusobacterium_nucleatum().rename(
                columns={"fusobacterium_nucleatum": "fusobacteria"}
            ),
        ]
    )

    # Binarize each cohort independently using the 75th percentile as cut-off
    fusobacteria["fusobacteria_binary_class"] = pandas.Categorical(
        fusobacteria.groupby(["technology", "cohort"]).fusobacteria.apply(
            lambda x: bin_fusobacteria(x, 0.75)
        ),
        ["low", "high"],
    )

    return fusobacteria
