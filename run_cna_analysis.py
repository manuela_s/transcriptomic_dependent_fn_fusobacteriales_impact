#!/usr/bin/env python3

"""
Run GISTIC copy number aberrations (CNA) analysis on primary tumour samples of patients of the TCGA pancancer collection.
"""

import pathlib

import pandas

import cohort
import gistic
import pathseq_utils
import transcriptomics.get_samples_metadata

DST_DIR = pathlib.Path("processed_data", "cna")


def _get_pancancer_data() -> pandas.DataFrame:
    """
    Get copy number aberrations data from primary solid tumour samples of patients of the TCGA pancancer collection.
    Patients were restricted to those with good quality PathSeq outputs and, thus, relative abundance of Fusobacteriales
    and higher resolution taxonomic ranks used in downstream analysis.
    :return: tall dataframe, index: 'patient_id' and columns: 'chromosome', 'start', 'end', 'logr'.
    """
    pancancer = cohort.get_pancancer_cohorts()

    cna = pancancer.get_cna()

    samples_in_mapping = transcriptomics.get_samples_metadata.get_samples_metadata()
    samples_in_metrics = pathseq_utils.get_filter_metrics()

    samples_with_pathseq = samples_in_mapping.join(samples_in_metrics, how="inner")

    cli = pancancer.get_clinical()

    # Identify patients with non-redacted clinical data, good quality (i. e. primary_reads > 10e6) and PathSeq outputs
    # and, thus, relative abundance of Fusobacteriales and higher resolution taxonomic ranks used in downstream analysis
    has_pathseq_output = cli.join(
        samples_with_pathseq.query('tissue=="tumour" and primary_reads>10e6')
        .groupby("patient_id")
        .size()
        .to_frame("n"),
        how="inner",
    )

    return cna.query(
        'patient_id.isin(@has_pathseq_output.index.get_level_values("patient_id"))'
    ).reset_index("cohort", drop=True)


def main():
    pancancer_data = _get_pancancer_data()
    gistic.gistic(pancancer_data, DST_DIR.joinpath("pancancer_all_gistic"))


if __name__ == "__main__":
    DST_DIR.mkdir(parents=True, exist_ok=True)

    main()
