#!/usr/bin/env python3

""" Generate figure 3 panels a-b and supplementary figures 2, 3 and 4.
Association between Fusobacterium nucleatum load (Taxonomy cohort) and relative abundance of Fusobacteriales and
Fusobacterium nucleatum (TCGA-COAD-READ cohort) with clinico-pathological and mutational features."""

import os
import pathlib
import typing

import pandas

os.environ["OUTDATED_IGNORE"] = "1"

import cohort
import constants
import fusobacteria_utils
import tcga_pancancer.get_tcga_crc
import matplotlib.axes
import matplotlib.pyplot
import pingouin
import fusobacteria_plot_utils
import statsmodels.graphics.mosaicplot
import py_utils.format_pvalue


DST_DIR = pathlib.Path("figures_and_tables")


def _plot_mosaic_plot_by_cat_var(
    cat_data: pandas.DataFrame,
    cat_var: str,
    ax: matplotlib.axes.Axes,
    ylabel: str = "$Fusobacteriales$ (order) RA",
) -> None:
    """
    Helper function to plot a mosaic plot comparing patients with low vs. high either Fusobacterium nucleatum load
    (Taxonomy cohort) or Fusobacterium nucleatum / Fusobacteriales relative abundance (TCGA-COAD-READ cohort) and a
    categorical variable.
    :param cat_data: dataframe with one row per patient, indices 'cohort' and 'patient_id' and columns including
                     'fusobacteria_binary_class' and cat_var;
    :param cat_var: name of column in cat_data with the per-patient categorical data;
    :param ax: axes to plot in;
    :param ylabel: ylabel to use on the plot.
    """
    stats = pingouin.chi2_independence(
        data=cat_data, x="fusobacteria_binary_class", y=cat_var
    )[2].query('test == "mod-log-likelihood"')[["chi2", "pval"]]
    statsmodels.graphics.mosaicplot.mosaic(
        cat_data.sort_values(by=cat_var),
        [cat_var, "fusobacteria_binary_class"],
        properties=lambda x: {"color": constants.FUSOBACTERIA_BIN_COLORS[x[1]]},
        gap=0.01,
        statistic=True,
        ax=ax,
    )
    ax.set_xlabel(cat_var.capitalize().replace("_", " "))
    ax.set_ylabel(ylabel)
    ax.set_title(
        "chi2={:.2f}, P={:s}, n={}".format(
            stats.chi2.values[0],
            py_utils.format_pvalue.format_pvalue(stats.pval.values[0]),
            cat_data[cat_var].dropna().shape[0],
        )
    )
    matplotlib.pyplot.tight_layout()


def figure_3_panels_a_1_b_1_2_and_supplementary_figure_2_panels_a_b(
    tcga_fusobacteriales_num_data: pandas.DataFrame,
    tcga_fn_num_data: pandas.DataFrame,
    taxonomy_fn_num_data: pandas.DataFrame,
    num_cols: typing.List[str],
) -> None:
    """
    Generate figure 3 panels a 1, b 1-2 and supplementary figure 2 panels a and b.
    :param tcga_fusobacteriales_num_data: dataframe with one row per patient per feature, with indices 'cohort',
           'patient_id', 'fusobacteria_binary_class', and 'feature' (numerical) and a column 'expression' with the
           values for the feature. The 'fusobacteria_binary_class' is based on Fusobacteriales relative abundance;
    :param tcga_fn_num_data: dataframe with one row per patient per feature, with indices 'cohort', 'patient_id',
           'fusobacteria_binary_class', and 'feature' (numerical) and a column 'expression' with the values for the
           feature. The 'fusobacteria_binary_class' is based on Fusobacterium nucleatum relative abundance;
    :param taxonomy_fn_num_data: dataframe with one row per patient per feature, with indices 'cohort', 'patient_id',
           'fusobacteria_binary_class', and 'feature' (numerical) and a column 'expression' with the values for the
           feature. The 'fusobacteria_binary_class' is based on Fusobacterium nucleatum load;
    :param num_cols: names of numerical features to plot. Must match values in the 'features' columns.
    """
    fusobacteria_plot_utils.gene_group_vs_fusobacteria_figure_separate_panels(
        tcga_fusobacteriales_num_data.query('feature=="age"').expression,
        "tcga_crc",
        "figure_3_panel_a_1",
        num_cols,
        DST_DIR,
        "Age [years]",
    )

    fusobacteria_plot_utils.gene_group_vs_fusobacteria_figure_separate_panels(
        tcga_fusobacteriales_num_data.query(
            'feature=="intratumor_heterogeneity"'
        ).expression,
        "tcga_crc",
        "figure_3_panel_b_1",
        num_cols,
        DST_DIR,
        "Intratumour heterogeneity [a. u.]",
    )

    fusobacteria_plot_utils.gene_group_vs_fusobacteria_figure_separate_panels(
        tcga_fusobacteriales_num_data.query(
            'feature.isin(["silent_per_mb", "non_silent_per_mb"])'
        ).expression,
        "tcga_crc",
        "figure_3_panel_b_2",
        num_cols,
        DST_DIR,
        "# mutations [per Mb]",
    )

    fusobacteria_plot_utils.gene_group_vs_fusobacteria_figure_separate_panels(
        tcga_fn_num_data.query('feature=="age"').expression,
        "tcga_crc",
        "supplementary_figure_3_panel_a_tcga_coad_read_cohort",
        num_cols,
        DST_DIR,
        "Age [years]",
    )
    fusobacteria_plot_utils.gene_group_vs_fusobacteria_figure_separate_panels(
        taxonomy_fn_num_data.query('feature=="age"').expression,
        "taxonomy",
        "supplementary_figure_3_panel_a_taxonomy_cohort",
        num_cols,
        DST_DIR,
        "Age [years]",
    )


def figure_3_panels_a_2_3_b_3(cat_data: pandas.DataFrame) -> None:
    """
    Generate figure 3 panels a 2-3 and b 3.
    :param cat_data: dataframe with one row per patient, indices 'cohort' and 'patient_id' and columns including
                     'fusobacteria_binary_class', 'stage', 'tumour_site_1' and 'microsatellite_status'.
    """
    cat_data["tumour_site_1"] = pandas.Categorical(
        cat_data.tumour_site_1, ["proximal", "distal", "rectal"]
    )

    fig, ax = matplotlib.pyplot.subplots(1, 3, figsize=(6, 2))
    _plot_mosaic_plot_by_cat_var(cat_data, "stage", ax=ax[0])
    _plot_mosaic_plot_by_cat_var(cat_data, "tumour_site_1", ax=ax[1])
    _plot_mosaic_plot_by_cat_var(cat_data, "microsatellite_status", ax=ax[2])
    fig.savefig(DST_DIR / "figure_3_panels_a_2_3_b_3.pdf")


def supplementary_figure_2(cat_data: pandas.DataFrame) -> None:
    """
    Generate supplementary figure 2.
    :param cat_data: dataframe with one row per patient, indices 'cohort' and 'patient_id' and columns including
                     'fusobacteria_binary_class', 'sex', 'bmi_bin', 'has_lymphovascular_invasion' and
                     'has_perineural_invasion'.
    """
    cat_data["bmi_bin"] = pandas.Categorical(
        cat_data.bmi_bin,
        ["under-weight", "normal-weight", "over-weight", "obese", "severely-obese"],
    )

    fig, ax = matplotlib.pyplot.subplots(2, 2, figsize=(6, 5.5))
    ax = ax.flatten()
    _plot_mosaic_plot_by_cat_var(cat_data, "sex", ax=ax[0])
    _plot_mosaic_plot_by_cat_var(cat_data, "bmi_bin", ax=ax[1])
    _plot_mosaic_plot_by_cat_var(cat_data, "has_lymphovascular_invasion", ax=ax[2])
    _plot_mosaic_plot_by_cat_var(cat_data, "has_perineural_invasion", ax=ax[3])
    fig.savefig(DST_DIR / "supplementary_figure_2.pdf")


def supplementary_figure_3(
    tcga_data: pandas.DataFrame, taxonomy_data: pandas.DataFrame
) -> None:
    """
    Generate supplementary figure 3.
    :param tcga_data: dataframe with one row per patient, indices 'cohort' and 'patient_id' and columns including
                     'fusobacteria_binary_class', 'stage', 'sex', 'tumour_site_1', 'has_lymphovascular_invasion',
                     'has_perineural_invasion', 'kras_status', 'braf_status', 'tp53_status' and 'microsatellite_status';
    :param taxonomy_data: dataframe with one row per patient, indices 'cohort' and 'patient_id' and columns including
                     'fusobacteria_binary_class', 'stage', 'sex', 'tumour_site_1', 'has_lymphovascular_invasion',
                     'has_perineural_invasion', 'kras_status', 'braf_status' and 'tp53_status';
    """
    tcga_data["stage"] = pandas.Categorical(tcga_data.stage, [1, 2, 3, 4])
    tcga_data["sex"] = pandas.Categorical(tcga_data.sex, ["male", "female"])
    tcga_data["tumour_site_1"] = pandas.Categorical(
        tcga_data.tumour_site_1, ["proximal", "distal", "rectal"]
    )
    tcga_data["has_lymphovascular_invasion"] = pandas.Categorical(
        tcga_data.has_lymphovascular_invasion, ["yes", "no"]
    )
    tcga_data["has_perineural_invasion"] = pandas.Categorical(
        tcga_data.has_perineural_invasion, ["yes", "no"]
    )
    tcga_data["has_perineural_invasion"] = pandas.Categorical(
        tcga_data.has_perineural_invasion, ["yes", "no"]
    )
    tcga_data["kras_status"] = pandas.Categorical(
        tcga_data.kras_status, ["wild_type", "mutant"]
    )
    tcga_data["braf_status"] = pandas.Categorical(
        tcga_data.braf_status, ["wild_type", "mutant"]
    )
    tcga_data["tp53_status"] = pandas.Categorical(
        tcga_data.tp53_status, ["wild_type", "mutant"]
    )
    tcga_data["microsatellite_status"] = pandas.Categorical(
        tcga_data.microsatellite_status, ["MSI", "MSS"]
    )

    taxonomy_data["stage"] = pandas.Categorical(taxonomy_data.stage, [2, 3])
    taxonomy_data["sex"] = pandas.Categorical(taxonomy_data.sex, ["male", "female"])
    taxonomy_data["tumour_site_1"] = pandas.Categorical(
        taxonomy_data.tumour_site_1, ["proximal", "distal", "rectal"]
    )
    taxonomy_data["has_lymphovascular_invasion"] = pandas.Categorical(
        taxonomy_data.has_lymphovascular_invasion, ["yes", "no"]
    )
    taxonomy_data["has_perineural_invasion"] = pandas.Categorical(
        taxonomy_data.has_perineural_invasion, ["yes", "no"]
    )
    taxonomy_data["kras_status"] = pandas.Categorical(
        taxonomy_data.kras_status, ["wild_type", "mutant"]
    )
    taxonomy_data["braf_status"] = pandas.Categorical(
        taxonomy_data.braf_status, ["wild_type", "mutant"]
    )
    taxonomy_data["tp53_status"] = pandas.Categorical(
        taxonomy_data.tp53_status, ["wild_type", "mutant"]
    )

    fig, ax = matplotlib.pyplot.subplots(4, 5, figsize=(12, 9))
    ax = ax.flatten()

    _plot_mosaic_plot_by_cat_var(tcga_data, "stage", ax=ax[0], ylabel="$Fn$ RA")
    _plot_mosaic_plot_by_cat_var(tcga_data, "sex", ax=ax[1], ylabel="$Fn$ RA")
    _plot_mosaic_plot_by_cat_var(tcga_data, "tumour_site_1", ax=ax[2], ylabel="$Fn$ RA")
    _plot_mosaic_plot_by_cat_var(
        tcga_data, "has_lymphovascular_invasion", ax=ax[3], ylabel="$Fn$ RA"
    )
    _plot_mosaic_plot_by_cat_var(
        tcga_data, "has_perineural_invasion", ax=ax[4], ylabel="$Fn$ RA"
    )

    _plot_mosaic_plot_by_cat_var(taxonomy_data, "stage", ax=ax[5], ylabel="$Fn$ load")
    _plot_mosaic_plot_by_cat_var(taxonomy_data, "sex", ax=ax[6], ylabel="$Fn$ load")
    _plot_mosaic_plot_by_cat_var(
        taxonomy_data, "tumour_site_1", ax=ax[7], ylabel="$Fn$ load"
    )
    _plot_mosaic_plot_by_cat_var(
        taxonomy_data, "has_lymphovascular_invasion", ax=ax[8], ylabel="$Fn$ load"
    )
    _plot_mosaic_plot_by_cat_var(
        taxonomy_data, "has_perineural_invasion", ax=ax[9], ylabel="$Fn$ load"
    )

    _plot_mosaic_plot_by_cat_var(tcga_data, "kras_status", ax=ax[10], ylabel="$Fn$ RA")
    _plot_mosaic_plot_by_cat_var(tcga_data, "braf_status", ax=ax[11], ylabel="$Fn$ RA")
    _plot_mosaic_plot_by_cat_var(tcga_data, "tp53_status", ax=ax[12], ylabel="$Fn$ RA")
    _plot_mosaic_plot_by_cat_var(
        tcga_data, "microsatellite_status", ax=ax[13], ylabel="$Fn$ RA"
    )

    _plot_mosaic_plot_by_cat_var(
        taxonomy_data, "kras_status", ax=ax[15], ylabel="$Fn$ load"
    )
    _plot_mosaic_plot_by_cat_var(
        taxonomy_data, "braf_status", ax=ax[16], ylabel="$Fn$ load"
    )
    _plot_mosaic_plot_by_cat_var(
        taxonomy_data, "tp53_status", ax=ax[17], ylabel="$Fn$ load"
    )

    fig.savefig(DST_DIR / "supplementary_figure_3.pdf")


def supplementary_figure_4() -> None:
    """Generate supplementary figure 4."""
    mutational_features = pandas.read_csv(
        pathlib.Path(
            "processed_data",
            "tcga_coad_read_mutational_features",
            "tcga_coad_read_mutation_features.csv",
        ),
        index_col=[0, 1, 2, 3],
    )
    fusobacteria_plot_utils.gene_group_vs_fusobacteria_figure_separate_panels(
        mutational_features.expression,
        "tcga_crc",
        "supplementary_figure_4_panel_a",
        ["Ti", "Tv"],
        DST_DIR,
        "Mutations [%]",
    )

    fusobacteria_plot_utils.gene_group_vs_fusobacteria_figure_separate_panels(
        mutational_features.expression,
        "tcga_crc",
        "supplementary_figure_4_panel_b",
        ["C>A", "C>G", "C>T", "T>C", "T>A", "T>G"],
        DST_DIR,
        "Mutations [%]",
    )


def main():
    cli = (
        cohort.get_crc_cohorts()
        .get_clinical()
        .query('cohort.isin(["TCGA-COAD-READ", "taxonomy"])')
    )
    extra_cli_tcga = tcga_pancancer.get_tcga_crc.clinical()[
        ["silent_per_mb", "non_silent_per_mb", "intratumor_heterogeneity"]
    ].rename_axis(index={"bcr_patient_barcode": "patient_id"})
    microsatellite_and_mutational_status = (
        cohort.get_crc_cohorts()
        .get_microsatellite_and_mutational_status()[
            ["microsatellite_status", "kras_status", "braf_status", "tp53_status"]
        ]
        .query('cohort.isin(["TCGA-COAD-READ", "taxonomy"])')
    )
    metadata = cli.join(extra_cli_tcga).join(microsatellite_and_mutational_status)
    metadata["age"] = metadata.age.astype(float)
    metadata["tumour_site_1"] = pandas.Categorical(
        metadata.tumour_site_1, ["proximal", "distal", "rectal"]
    )

    tcga_fusobacteriales = (
        fusobacteria_utils.get_crc_fusobacteria()
        .loc["rna"][["fusobacteria_binary_class"]]
        .query('cohort=="TCGA-COAD-READ"')
    )

    num_cols = ["age", "silent_per_mb", "non_silent_per_mb", "intratumor_heterogeneity"]
    cat_cols = [
        "stage",
        "tumour_site_1",
        "microsatellite_status",
        "sex",
        "bmi_bin",
        "has_lymphovascular_invasion",
        "has_perineural_invasion",
        "kras_status",
        "braf_status",
        "tp53_status",
    ]

    tcga_fusobacteriales_with_cli = tcga_fusobacteriales.join(
        metadata[cat_cols], how="inner"
    )

    tcga_fuso_df = fusobacteria_utils.get_relative_abundance_at_multiple_resolution_taxonomic_ranks().query(
        'patient_id.isin(@tcga_fusobacteriales.index.get_level_values("patient_id"))'
    )
    tcga_fuso_df.columns = tcga_fuso_df.columns.droplevel([0, 1, 2, 3]).str.lstrip()
    tcga_fn = fusobacteria_utils.bin_fusobacteria(
        tcga_fuso_df.Fusobacterium_nucleatum
    ).to_frame("fusobacteria_binary_class")
    tcga_fn_with_cli = tcga_fn.join(metadata[cat_cols], how="inner")

    taxonomy_fn = fusobacteria_utils.get_crc_fusobacteria().query('cohort=="taxonomy"')[
        ["fusobacteria_binary_class"]
    ]
    taxonomy_fn_with_cli = metadata[cat_cols].join(taxonomy_fn, how="inner")

    tcga_fusobacteriales_num_data = (
        tcga_fusobacteriales.join(metadata[num_cols], how="inner")
        .set_index("fusobacteria_binary_class", append=True)
        .rename_axis(columns=["feature"])
        .stack()
        .to_frame("expression")
    )
    tcga_fn_num_data = (
        tcga_fn.join(metadata[num_cols], how="inner")
        .set_index("fusobacteria_binary_class", append=True)
        .rename_axis(columns=["feature"])
        .stack()
        .to_frame("expression")
    )
    taxonomy_fn_num_data = (
        taxonomy_fn.join(metadata[num_cols], how="inner")
        .set_index("fusobacteria_binary_class", append=True)
        .rename_axis(columns=["feature"])
        .stack()
        .to_frame("expression")
    )

    figure_3_panels_a_1_b_1_2_and_supplementary_figure_2_panels_a_b(
        tcga_fusobacteriales_num_data, tcga_fn_num_data, taxonomy_fn_num_data, num_cols
    )
    figure_3_panels_a_2_3_b_3(tcga_fusobacteriales_with_cli)
    supplementary_figure_2(tcga_fusobacteriales_with_cli)
    supplementary_figure_3(tcga_fn_with_cli, taxonomy_fn_with_cli)
    supplementary_figure_4()


if __name__ == "__main__":
    matplotlib.rcParams["pdf.fonttype"] = 42
    matplotlib.rcParams["font.size"] = 6
    matplotlib.rcParams["font.family"] = "Arial"

    pathlib.Path(DST_DIR).mkdir(parents=True, exist_ok=True)

    main()
