#!/usr/bin/env python3

""" Generate figure 3 panels c-d and supplementary table 3.
Association between Fusobacteriales relative abundance and mutational status in patients of the TCGA-COAD-READ cohort. """

import os
import pathlib
import typing

os.environ["OUTDATED_IGNORE"] = "1"

import matplotlib
import matplotlib.cm
import matplotlib.colors
import matplotlib.pyplot
import pandas
import pingouin
import seaborn

import cohort
import fusobacteria_utils
import py_utils.format_pvalue


DST_DIR = pathlib.Path("figures_and_tables")


def _identify_significant_categorical_features(
    layer_data: pandas.DataFrame, fusobacteria: pandas.DataFrame
) -> pandas.DataFrame:
    """
    Identify statistically significant features by Fusobacteriales relative abundance (low vs. high).
    :param layer_data: dataframe, one row per patient and features to test as columns;
    :param fusobacteria: dataframe, one row per patient, must include 'fusobacteria_binary_class' column;
    :return: dataframe, one row per feature tested:
            - indices: feature' and 'test';
            - columns: 'lambda', 'chi2', 'dof', 'pval', 'cramer', 'power', 'is_significant' and 'adjusted_p_value'.
    """
    features = layer_data.columns.to_list()
    layer_data_feature = layer_data.join(
        fusobacteria.fusobacteria_binary_class, how="inner"
    )
    stats = {}
    for feature in features:
        _, _, stats[feature] = pingouin.chi2_independence(
            layer_data_feature, x="fusobacteria_binary_class", y=feature
        )
    stats = (
        pandas.concat(stats, names=["feature"])
        .reset_index(1, drop=True)
        .set_index("test", append=True)
        .query('test=="mod-log-likelihood"')
    )
    stats["is_significant"], stats["adjusted_p_value"] = pingouin.multicomp(
        stats.pval.values, method="fdr_bh"
    )

    return stats


def supplementary_table_3(stats: pandas.DataFrame, n_patients: int) -> None:
    """
    Generate supplementary table 3.
    :param stats: dataframe, one row per significant feature and columns include 'type', 'test', 'chi2', 'pval',
                  'adjusted_p_value' and 'is_significant';
    :param n_patients: number of patients included in statistical analysis.
    """
    stats_for_export = stats.set_index("type", append=True)[
        ["test", "chi2", "pval", "adjusted_p_value", "is_significant"]
    ]
    stats_for_export.index = stats_for_export.index.swaplevel()
    stats_for_export.columns = stats_for_export.columns.str.capitalize().str.replace(
        "_", " "
    )
    stats_for_export.rename(
        columns={
            "Test": "Method",
            "Pval": "Unadjusted P-value",
            "Adjusted p value": "FDR-BH adjusted P-value",
        },
        inplace=True,
    )
    stats_for_export.rename_axis(
        index={"type": "Reason for inclusion", "feature": "Mutated gene"}, inplace=True
    )
    stats_for_export["Chi2"] = stats_for_export.Chi2.round(2)
    stats_for_export["Unadjusted P-value"] = stats_for_export[
        "Unadjusted P-value"
    ].apply(py_utils.format_pvalue.format_pvalue)
    stats_for_export["FDR-BH adjusted P-value"] = stats_for_export[
        "FDR-BH adjusted P-value"
    ].apply(py_utils.format_pvalue.format_pvalue)
    stats_for_export["N"] = n_patients

    # Re-order columns to match order used in supplementary tables 5 (gene expression) and 6 (protein expression)
    stats_for_export = stats_for_export[
        [
            "Is significant",
            "Method",
            "N",
            "Chi2",
            "Unadjusted P-value",
            "FDR-BH adjusted P-value",
        ]
    ]

    # Export
    stats_for_export.to_csv(DST_DIR / "supplementary_table_3.csv")


def figure_3_panels_c_d(
    fusobacteria: pandas.DataFrame,
    interesting_muts: typing.List[str],
    significant_muts: typing.List[str],
    stats: pandas.DataFrame,
    tcga_mut: pandas.DataFrame,
) -> None:
    """
    Generate figure 3 panels C-D.
    :param fusobacteria: dataframe, as returned by `fusobacteria_utils.get_crc_fusobacteria()`;
    :param interesting_muts: list of mutations selected a priori;
    :param significant_muts: list of mutations identified as statistically significant different by Fusobacteriales
                             relative abundance;
    :param stats: dataframe, one row per feature and must include the column 'pretty_feature';
    :param tcga_mut: dataframe, one row per patient and columns include boolean values for mutation features.
    """
    # Prepare data for inclusion in plot
    n_mutations = (
        fusobacteria.set_index("fusobacteria_binary_class", append=True)
        .drop(columns=["fusobacteria"])
        .join(tcga_mut[interesting_muts + significant_muts], how="inner")
        .astype(int)
        .rename(columns=stats.pretty_feature)
    )
    plot_data = (
        n_mutations.rename_axis(columns=["feature"]).stack().to_frame("n_mutations")
    )
    plot_data = (
        plot_data.groupby(
            ["feature", "fusobacteria_binary_class", "n_mutations"], observed=True
        )
        .size()
        .div(plot_data.groupby(["feature", "fusobacteria_binary_class"]).size())
        .mul(100)
        .to_frame("proportion")
    )
    plot_data.reset_index("fusobacteria_binary_class", inplace=True)
    plot_data["fusobacteria_binary_class"] = pandas.Categorical(
        plot_data.fusobacteria_binary_class, ["low", "high"]
    )
    plot_data.set_index("fusobacteria_binary_class", append=True, inplace=True)
    plot_data.query("n_mutations > 0", inplace=True)

    # Generate plot
    cmap = matplotlib.cm.Reds._resample(
        plot_data.index.get_level_values("n_mutations").max() + 1
    )
    norm = matplotlib.colors.Normalize(
        vmin=-0.5, vmax=plot_data.index.get_level_values("n_mutations").max() + 0.5
    )
    sm = matplotlib.cm.ScalarMappable(norm=norm, cmap=cmap)
    colors = {
        n: sm.to_rgba(n)
        for n in plot_data.index.get_level_values("n_mutations").unique()
    }

    def helper(data, color):
        ax = matplotlib.pyplot.gca()
        data.proportion.unstack("n_mutations").plot.bar(
            stacked=True, width=0.95, color=colors, edgecolor="k", linewidth=0.75, ax=ax
        )
        ax.set_xlim(-0.475, 1.475)
        ax.set_ylim(0)

    g = seaborn.FacetGrid(
        col="feature",
        col_order=stats.pretty_feature,
        data=plot_data.reset_index("feature"),
    )
    g.map_dataframe(helper)
    g.set_titles("{col_name}")
    g.set_xlabels("")
    g.set_ylabels("Mutation frequency [%]")
    g.fig.colorbar(
        sm,
        fraction=0.06,
        shrink=0.25,
        ax=g.axes,
        label="# aberrations",
        ticks=range(0, 12, 2),
    )
    matplotlib.pyplot.suptitle(
        f"TCGA-COAD-READ cohort (n={n_mutations.shape[0]})", fontsize=8
    )
    matplotlib.pyplot.subplots_adjust(
        top=0.732, bottom=0.216, left=0.058, right=0.92, hspace=0.2, wspace=0.602
    )
    g.fig.set_size_inches(7, 1.6)
    g.savefig(DST_DIR / "figure_3_panels_c_d.pdf")


def main():
    """
    Generate figure 3 panels C-D and export supplementary table 3.
    Figure 3 panels C-D include a comparison of frequency of occurrence of mutations selected *a priori* (C) or
    identified by an unbiased scan (D) in *Fusobacteriales-high vs -low patients of the TCGA-COAD-READ cohort.
    Supplementary table 3 include statistical results testing the association between mutational status and
    Fusobacteriales relative abundance in the TCGA-COAD-READ patients.
    """
    fusobacteria = fusobacteria_utils.get_crc_fusobacteria()

    tcga_mut = cohort.get_tcga_crc_cohort().get_mutations()
    # Select features with mutations in at least 5% of the patients and conver to boolean
    tcga_mut_reduced = tcga_mut.loc[:, tcga_mut.gt(0).mean().gt(0.05)].gt(0)

    stats = _identify_significant_categorical_features(
        tcga_mut_reduced, fusobacteria
    ).reset_index("test")
    interesting_muts = [
        "KRAS",
        "NRAS",
        "BRAF",
        "TP53",
        "PIK3CA",
        "SMAD4",
        "PTEN",
        "BRCA2",
        "APC",
    ]

    stats = stats.query("is_significant or feature.isin(@interesting_muts)").copy()
    significant_muts = (
        stats.query("is_significant").index.get_level_values("feature").to_list()
    )
    stats["type"] = pandas.Categorical(
        [
            "Unbiased screen" if x else "Selected a priori"
            for x in stats.index.isin(significant_muts)
        ],
        ["Selected a priori", "Unbiased screen"],
    )
    stats = stats.sort_values(by="type")
    stats["pretty_feature"] = stats.apply(
        lambda x: f"{x.name}\n(P={py_utils.format_pvalue.format_pvalue(x.adjusted_p_value)})",
        axis=1,
    )

    figure_3_panels_c_d(
        fusobacteria,
        interesting_muts,
        significant_muts,
        stats[["pretty_feature"]],
        tcga_mut,
    )
    supplementary_table_3(stats, n_patients=tcga_mut.shape[0])


if __name__ == "__main__":
    DST_DIR.mkdir(parents=True, exist_ok=True)
    matplotlib.rcParams["pdf.fonttype"] = 42
    matplotlib.rcParams["font.size"] = 6
    matplotlib.rcParams["font.family"] = "Arial"
    matplotlib.rcParams["figure.dpi"] = 141

    main()
