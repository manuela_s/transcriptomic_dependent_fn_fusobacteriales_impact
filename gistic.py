#!/usr/bin/env python3

"""
Module to run gistic (http://portals.broadinstitute.org/cgi-bin/cancer/publications/pub_paper.cgi?mode=view&paper_id=216&p=t)
"""

import pathlib
import subprocess
import tempfile

import pandas


def gistic(cna: pandas.DataFrame, base_dir: pathlib.Path) -> None:
    """
    Run GISTIC (Mermel, Genome Biol., 2011, PMID: 21527027) to process copy number alterations for the TCGA pancancer
    cohort and save results.
    :param cna: dataframe with columns ['chromosome', 'start', 'end', 'logr'] and index 'patient_id'
    :param base_dir: folder to store results in.
    """
    # Reformat segmentation data to match gistic expectations:
    segmentation_data = cna.rename(
        columns={
            "chromosome": "Chromosome",
            "start": "Start.bp",
            "end": "End.bp",
            "logr": "Seg.CN",
        }
    )
    segmentation_data["Chromosome"] = (
        segmentation_data.Chromosome.str.replace("chr", "")
        .str.replace("X", "23")
        .astype(int)
    )
    segmentation_data["Num.SNPs"] = (
        segmentation_data["End.bp"] - segmentation_data["Start.bp"]
    )
    segmentation_data = segmentation_data[
        ["Chromosome", "Start.bp", "End.bp", "Num.SNPs", "Seg.CN"]
    ]
    segmentation_data.rename_axis(index="Sample", inplace=True)

    segmentation_file = tempfile.NamedTemporaryFile(suffix="txt")
    segmentation_data.to_csv(segmentation_file.name, sep="\t")

    base_dir.mkdir(exist_ok=True)
    cmd = [
        "./gistic2",
        "-b",
        str(base_dir.absolute()),
        "-seg",
        segmentation_file.name,
        "-refgene",
        "refgenefiles/hg38.UCSC.add_miR.160920.refgene.mat",
        "-genegistic",
        "1",
        "-smallmem",
        "1",
        "-broad",
        "1",
        "-brlen",
        "0.5",
        "-conf",
        "0.90",
        "-armpeel",
        "1",
        "-savegene",
        "1",
        "-gcm",
        "extreme",
    ]
    subprocess.run(cmd, check=True, cwd="gistic")
