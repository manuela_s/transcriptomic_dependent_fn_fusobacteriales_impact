#!/usr/bin/env python3

""" Generate supplementary table 2.
Patient-level bacterium data for cases of the Taxonomy and TCGA-COAD-READ cohorts:
    - Fusobacterium nucleatum load for patients of the Taxonomy cohort;
    - Relative abundance of Fusobacteriales and higher resolution taxonomic ranks (family, genus and species) including
      the Fusobacterium nucleatum species for patients of the TCGA-COAD-READ cohort.
"""

import pathlib

import pandas

import fusobacteria_utils

DST_DIR = pathlib.Path("figures_and_tables")


def get_taxonomy_cohort_data() -> pandas.DataFrame:
    """
    Get per-patient 'Fusobacterium_nucleatum' (Fn) load values.
    :return: dataframe, one row per patient with index 'patient_id' and a column 'Fusobacterium_nucleatum' with load
             determined by qPCR.
    """
    return (
        fusobacteria_utils.get_crc_fusobacteria()
        .query('cohort=="taxonomy"')
        .reset_index(["technology", "cohort"], drop=True)
        .fusobacteria.to_frame("Fusobacterium_nucleatum")
    )


def get_tcga_coad_read_data() -> pandas.DataFrame:
    """
    Get per-patient 'Fusobacteriales' and higher resolution taxonomic rank relative abundance (RA) values.
    :return: dataframe, one row per patient with index 'patient_id' and multi-index columns including ['Family', 'Genus',
             'Species', 'Level', 'Label'] and relative abundance values, as returned by
             `fusobacteria_utils.get_relative_abundance_at_multiple_resolution_taxonomic_ranks`.
    """
    tcga_coad_read_cohort = (
        fusobacteria_utils.get_relative_abundance_at_multiple_resolution_taxonomic_ranks()
    )
    tcga_coad_read_cohort.columns = pandas.MultiIndex.from_frame(
        tcga_coad_read_cohort.columns.to_frame()
        .fillna("")
        .apply(lambda x: x.str.strip().str.capitalize())
    )
    tcga_coad_read_cohort.columns.names = [
        name.capitalize() for name in tcga_coad_read_cohort.columns.names
    ]

    return tcga_coad_read_cohort


def main():
    taxonomy_cohort = get_taxonomy_cohort_data()
    tcga_coad_read_cohort = get_tcga_coad_read_data()
    with pandas.ExcelWriter(f"{DST_DIR}/supplementary_table_2.xlsx") as writer:
        taxonomy_cohort.to_excel(
            writer, sheet_name=f"Taxonomy cohort (n={taxonomy_cohort.shape[0]})"
        )
        tcga_coad_read_cohort.to_excel(
            writer,
            sheet_name=f"TCGA-COAD-READ cohort (n={tcga_coad_read_cohort.shape[0]})",
        )


if __name__ == "__main__":
    DST_DIR.mkdir(parents=True, exist_ok=True)
    main()
