#!/usr/bin/env python3

"""Constants across multiple functions / figures."""

CNA_ALTERATION_COLORS = {"deletion": "#08619B", "amplification": "#F23E00"}

CNA_ALTERATION__DARKER_COLORS = {"deletion": "#064A77", "amplification": "#BA2F00"}

FUSOBACTERIA_BIN_COLORS = {"low": "#33a02c", "high": "#ff7f00"}

MESENCHYMAL_BIN_COLORS = {"low": "#4292c6", "high": "#08519c"}

CMS_COLORS = {
    "CMS1": "#E89E33",
    "CMS2": "#0073AC",
    "CMS3": "#D079A4",
    "CMS4": "#009E76",
    "NOLBL": "k",
}

CRIS_COLORS = {
    "CRIS-A": "#FF4500",
    "CRIS-B": "#B5022A",
    "CRIS-C": "#03285C",
    "CRIS-D": "#068142",
    "CRIS-E": "#02C6B0",
    "NOLBL": "k",
}


CMS_ORDER = ["CMS1", "CMS2", "CMS3", "CMS4", "NOLBL"]
CRIS_ORDER = ["CRIS-A", "CRIS-B", "CRIS-C", "CRIS-D", "CRIS-E", "NOLBL"]

GENES_OR_SIGNATURES = {
    "NOTCH": "#fdd0a2",
    "EMT": "#fdae6b",
    "WNT": "#fd8d3c",
    "Secretion, migration and metastasis": "#f16913",
    "Cell cycle": "#fcc5c0",
    "DNA damage": "#fa9fb5",
    "Cell types": "#c7e9c0",
    "Fibroblast": "#41ab5d",
    "Endothelial": "#006d2c",
    "Immuno": "#deebf7",
    "T cells chemotactism and activation": "#c6dbef",
    "Activated T cells": "#9ecae1",
    "Activated NK cells": "#6baed6",
    "B cells chemotactism and TLS": "#4292c6",
    "Inflammation signatures": "#fff5f0",
    "Inflammation": "#fee0d2",
    "Pro inflammation": "#fcbba1",
    "Anti inflammation": "#fc9272",
    "Metallo proteins": "#8c6bb1",
    "Immunosuppression": "#fb6a4a",
    "Inflammasome": "#a50f15",
    "Angiogenesis": "#ef3b2c",
    "Complement system": "#cb181d",
    "Macrophage chemotactism and activation": "#a50f15",
    "Antigen presentation": "#67000d",
    "Other pathways": "#969696",
}

QUANTISEQ = {
    "Tumour & Stromal cells": "#969696",
    "T cells CD4": "#b2df8a",
    "T cells CD8": "#33a02c",
    "T regs": "#006d2c",
    "Dendritic cells": "#ffff99",
    "B cells": "#fdbf6f",
    "Monocytes": "#ff7f00",
    "Macrophages M1": "#a6cee3",
    "Macrophages M2": "#1f78b4",
    "NK cells": "#fb9a99",
    "Neutrophils": "#e31a1c",
}
